package com.highlysucceed.impact_weather.util.view.activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.Data;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import icepick.State;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private Unbinder unbinder;
    @State String fragmentName;
    @State int fragmentFrame = R.id.content_frame;
    private Context context;
    @State boolean hasBackButton = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setContentView(onLayoutSet());
        bindView();
        context = this;
        Data.setSharedPreferences(context);
        initialFragment(getActivityTag(), getFragmentTag());
        onViewReady();
    }

    public void initialFragment(String activityName, String fragmentName){

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(hasBackButton){
            switch (item.getItemId()) {
                case android.R.id.home:
                    finish();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        if ( backStackCount <= 1 ) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        unbindView();
        super.onDestroy();
    }


    public int onLayoutSet(){
        return R.layout.activity_template;
    }

    public void onViewReady(){

    }

    public void showToolbarBackButton(boolean hasBackButton){
        getSupportActionBar().setDisplayHomeAsUpEnabled(hasBackButton);
        this.hasBackButton = hasBackButton;
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private void bindView(){
        unbinder = ButterKnife.bind(this);
    }

    private void unbindView(){
        unbinder.unbind();
    }

    public Context getContext(){
        return context;
    }

    public void setFragmentFrame(int fragmentFrame){
        this.fragmentFrame = fragmentFrame;
    }

    public void switchFragment(Fragment fragment){
        switchFragment(fragment, true, FragmentTransaction.TRANSIT_NONE);
    }

    public void switchFragment(Fragment fragment, int animation){
        switchFragment(fragment, true, animation);
    }

    public void switchFragment(Fragment fragment, boolean addBackStack){
        switchFragment(fragment, addBackStack, FragmentTransaction.TRANSIT_NONE);
    }

    public void switchFragment(Fragment fragment, boolean addBackStack, int animation) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(fragmentFrame, fragment);

        if(animation == FragmentTransaction.TRANSIT_NONE){
            transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        }else{
            transaction.setCustomAnimations(animation,0);
        }

        if(addBackStack){
            transaction.addToBackStack(null);
        }
        transaction.commit();

    }

    public String getActivityTag(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(RouteManager.ACTIVITY_TAG, "");
        }
        return "";
    }

    public String getFragmentTag(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(RouteManager.FRAGMENT_TAG, "");
        }
        return "";
    }

    public Bundle getActivityBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getBundle(RouteManager.ACTIVITY_BUNDLE);
        }
        return new Bundle();
    }

    public Bundle getFragmentBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getBundle(RouteManager.FRAGMENT_BUNDLE);
        }
        return new Bundle();
    }
    public boolean isAllPermissionResultGranted(int [] grantResults){
        boolean granted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                granted = false;
                break;
            }
        }
        return granted;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        List<Fragment> fragments = getSupportFragmentManager().getFragments();
//        if (fragments != null) {
//            for (Fragment fragment : fragments) {
//                fragment.onActivityResult(requestCode, resultCode, data);
//            }
//        }
//    }

}
