package com.highlysucceed.impact_weather.util.view.activity;

import android.os.Bundle;
import android.util.Log;

import com.highlysucceed.impact_weather.data.UserData;
import com.server.android.model.AdvisoryItem;
import com.server.android.util.InvalidToken;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.activity.DeviceActivity;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.activity.RecommendationActivity;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;
import com.highlysucceed.impact_weather.view.activity.SettingsActivity;
import com.highlysucceed.impact_weather.view.activity.WeatherActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Created by BCTI 3 on 2/6/2017.
 */

public class RouteActivity extends BaseActivity{

    public void startMainActivity(){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag("home")
                .startActivity();

                finish();
    }

    public void startMainActivityy(int farmId){
        Bundle bundle = new Bundle();
        bundle.putInt("farm_id", farmId);
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag("home")
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startRegistrationActivity(){
        RouteManager.Route.with(this)
                .addActivityClass(RegistrationActivity.class)
                .addActivityTag("registration")
                .addFragmentTag("splash")
                .startActivity();

        finish();
    }

    public void startSettingsActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(SettingsActivity.class)
                .addActivityTag("settings")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }
    public void startMyFarmActivity(int farmId, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putInt("farm_id", farmId);
        RouteManager.Route.with(this)
                .addActivityClass(FarmActivity.class)
                .addActivityTag("my_farm")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startJournalActivity(int farmId, String crop, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putInt("farm_id", farmId);
        bundle.putString("crop", crop);
        RouteManager.Route.with(this)
                .addActivityClass(FarmActivity.class)
                .addActivityTag("my_farm")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }
    public void startRecommendationActivity(int recommendationId, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putInt("recommendation_id", recommendationId);

        RouteManager.Route.with(this)
                .addActivityClass(RecommendationActivity.class)
                .addActivityTag("recommendation")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startWeatherActivity(int deviceId, String fragmentTAG){
        Bundle bundle = new Bundle();
        bundle.putInt("device_id", deviceId);

        RouteManager.Route.with(this)
                .addActivityClass(WeatherActivity.class)
                .addActivityTag("weather")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startMyDeviceActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(DeviceActivity.class)
                .addActivityTag("device")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void invalidToken(InvalidToken invalidToken){
        EventBus.getDefault().unregister(this);
        Log.e("Expired", "Session Expired");
        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        ToastMessage.show(this, "Session Expired", ToastMessage.Status.FAILED);
        startRegistrationActivity();
    }
}
