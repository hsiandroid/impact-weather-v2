package com.highlysucceed.impact_weather.util.lib;

import android.text.Editable;
import android.text.SpannableStringBuilder;

import java.util.List;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class StringFormatter {

    public static boolean isEmpty(String string) {
        if(string == null) return true;
        if(string.length() == 0) return true;
        if(string.equals("null")) return true;
        return false;
    }

    public static String clean(String string) {
        return clean(string, "");
    }

    public static String clean(String string, String defaultValue) {
        if(string == null) return defaultValue;
        if(string.length() == 0) return defaultValue;
        if(string.equals("null")) return defaultValue;
        return string;
    }

    public static String first(String string) {
        if(string == null) return "";

        if(string.length()<0) return "";

        if(string.length()>0){
            return string.substring(0,1).toUpperCase() + string.substring(1).toLowerCase();
        }
        return "";
    }

    public static String firstWord(String string) {
        if(string == null) return "";

        if(string.length()<0) return "";

        String [] words = string.split(" ");

        if(words.length < 0 ) return string;

        return words[0];
    }

    public static String titleCase(String string) {
        if(string == null) return "";

        if(string.length()<0) return "";

        StringBuilder sb = new StringBuilder(string.length());
        for(String word : string.split(" ")){
            sb.append(first(word));
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    public static String upperCase(String string) {
        if(string == null) return "";

        if(string.length()<0) return "";
        return string.toUpperCase();
    }

    public static String lowerCase(String string) {
        if(string == null) return "";

        if(string.length()<0) return "";
        return string.toLowerCase();
    }

    public static String implode(List<String> string, String separator) {
        if(string == null) return "";

        if(string.size()<0) return "";

        StringBuilder sb = new StringBuilder();
        for(String s : string){
            if (!isEmpty(s)) {
                sb.append(s);
                sb.append(separator);
            }
        }
        if(sb.length() <= 0){
            return "";
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public static void textWatcherToUpperCase(Editable editable){
        textWatcherToUpperCase(editable, "", "");
    }

    public static void textWatcherToUpperCase(Editable editable, String replaceTarget, String replacement){
        String raw = editable.toString();
        if (raw.contains(" ") || !raw.equals(raw.toUpperCase())) {
            Editable builder = new SpannableStringBuilder(raw.toUpperCase().replace(replaceTarget, replacement));
            editable.replace(0, editable.length(), builder);
        }
    }

    public static void textWatcherToLowerCase(Editable editable){
        textWatcherToLowerCase(editable, "", "");
    }

    public static void textWatcherToLowerCase(Editable editable, String replaceTarget, String replacement){
        String raw = editable.toString();
        if (raw.contains(" ") || !raw.equals(raw.toLowerCase())) {
            Editable builder = new SpannableStringBuilder(raw.toLowerCase().replace(replaceTarget, replacement));
            editable.replace(0, editable.length(), builder);
        }
    }

}
