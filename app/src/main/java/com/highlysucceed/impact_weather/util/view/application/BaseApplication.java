package com.highlysucceed.impact_weather.util.view.application;

import android.app.Application;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.Data;
import com.highlysucceed.impact_weather.data.UserData;
import com.server.android.util.App;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by BCTI 3 on 3/16/2017.
 */

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/aileron/aileron.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
