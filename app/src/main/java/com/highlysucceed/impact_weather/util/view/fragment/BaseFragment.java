package com.highlysucceed.impact_weather.util.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highlysucceed.impact_weather.R;

import java.lang.reflect.Method;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;

public class BaseFragment extends Fragment {

    private Unbinder unbinder;
    private Context context;
    private Class baseActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(onLayoutSet(), container, false);
        bindView(view);
        context = getActivity();
        onViewReady();
        onViewReady(savedInstanceState);
        return view;
    }

    public Context getContext(){
        return context;
    }

    public int onLayoutSet(){
        return R.layout.fragment_template;
    }

    public void setParentActivity(Class baseActivity){
        this.baseActivity = baseActivity;
    }

    public Method[] getParentActivity(){
        return baseActivity.getMethods();
    }

    public int onLayoutSet(@LayoutRes int layout){
        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        unbindView();
        super.onDestroyView();
    }

    public void onViewReady(){

    }

    public void onViewReady(Bundle savedInstanceState){

    }

    private void bindView(View view){
        unbinder = ButterKnife.bind(this, view);
    }

    private void unbindView(){
        unbinder.unbind();
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        List<Fragment> fragments = getChildFragmentManager().getFragments();
//        if (fragments != null) {
//            for (Fragment fragment : fragments) {
//                fragment.onActivityResult(requestCode, resultCode, data);
//            }
//        }
//    }
}
