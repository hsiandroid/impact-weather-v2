package com.highlysucceed.impact_weather.data;

import com.server.android.model.RecommendationItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 3/7/2017.
 */

public class SampleData {
    public static List<RecommendationItem> getRecommendationItems(int count){
        List<RecommendationItem> recommendationItems = new ArrayList<>();


        recommendationItems.clear();

//        recommendationItem.status = R.color.light_red;
        RecommendationItem recommendationItem = new RecommendationItem();
        recommendationItem.id = 1;
        recommendationItem.title = "Field Preparations";
        recommendationItem.content ="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        recommendationItem.type = "critical";
//        recommendationItem.temp_image = R.drawable.icon_preparation;
        recommendationItems.add(recommendationItem);

//        recommendationItem.bg = R.color.light_orange;
        recommendationItem = new RecommendationItem();
        recommendationItem.id = 1;
        recommendationItem.title = "Sowing/Planting";
        recommendationItem.content ="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        recommendationItem.type = "immediate";
//        recommendationItem.temp_image = R.drawable.icon_planting;
        recommendationItems.add(recommendationItem);

//        recommendationItem.bg = R.color.light_green;
        recommendationItem = new RecommendationItem();
        recommendationItem.id = 1;
        recommendationItem.title = "Harvesting";
        recommendationItem.content ="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        recommendationItem.type = "low";
//        recommendationItem.temp_image = R.drawable.icon_harvesting;
        recommendationItems.add(recommendationItem);


        List<RecommendationItem> val = new ArrayList<>();

        int counter = 0;

        do{
            for(RecommendationItem item : recommendationItems){
                if(counter < count){
                    val.add(item);
                    counter++;
                }
            }
        }while (counter < count);

        return val;
    }
}
