package com.highlysucceed.impact_weather.data;


import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.server.android.model.ActivityItem;
import com.server.android.model.AdvertisementItem;
import com.server.android.model.UserItem;
import com.server.android.transformer.AppSettingsTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class UserData extends Data{

    public static final String USER_ITEM = "user_item";
    public static final String AUTHORIZATION = "authorization";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String TOKEN_EXPIRED = "token_expired";
    public static final String ACTIVITY_ITEM = "activity_item";
    public static final String DEFAULT_FARM_ID = "default_farm_id";
    public static final String ADVERTISEMENT_ITEM = "advertisement_item";

    public static boolean isLogin(){
        return getUserItem().id != 0;
    }

    public static boolean isMe(int id){
        return getUserItem().id == id;
    }

    public static int getUserId(){
        return getUserItem().id;
    }

    public static void insert(String key, String value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void insert(String key, int value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void insert(String key, boolean value){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void insert(UserItem userItem){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(USER_ITEM, new Gson().toJson(userItem));
        editor.commit();
    }

    public static void insert(AppSettingsTransformer.Data activityTransformer){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(ACTIVITY_ITEM, new Gson().toJson(activityTransformer));
        editor.commit();
    }

    public static void insert(AdvertisementItem advertisementItem){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(ADVERTISEMENT_ITEM, new Gson().toJson(advertisementItem));
        editor.commit();
    }

    public static String getString(String key){
        return getSharedPreferences().getString(key, "");
    }

    public static int getInt(String key){
        return Data.getSharedPreferences().getInt(key, 0);
    }

    public static boolean getBoolean(String key){
        return getSharedPreferences().getBoolean(key, false);
    }

    public static UserItem getUserItem(){
        UserItem userItem = new Gson().fromJson(getSharedPreferences().getString(USER_ITEM, ""), UserItem.class);
        if(userItem == null){
            userItem = new UserItem();
        }
        return userItem;
    }

    public static AdvertisementItem getAdvertisementItem(){
        AdvertisementItem advertisementItem = new Gson().fromJson(getSharedPreferences().getString(ADVERTISEMENT_ITEM, ""), AdvertisementItem.class);
        if(advertisementItem == null){
            advertisementItem = new AdvertisementItem();
        }
        return advertisementItem;
    }

    public static List<ActivityItem> getActivityItems(){
        AppSettingsTransformer.Data activityTransformer = new Gson().fromJson(getSharedPreferences().getString(ACTIVITY_ITEM, ""), AppSettingsTransformer.Data.class);
        return activityTransformer.farm_activity;
    }

}
