package com.highlysucceed.impact_weather.view.dialog;

import android.content.DialogInterface;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.server.android.model.DeviceItem;
import com.server.android.request.station.StationAllRequest;
import com.server.android.transformer.station.StationCollectionTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.view.adapter.DeviceAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class SubscribeDeviceDialog extends BaseDialog implements DeviceAdapter.ClickListener,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        DeviceDetailsDialog.Callback{
	public static final String TAG = SubscribeDeviceDialog.class.getName().toString();

	private DeviceAdapter deviceAdapter;

    private StationAllRequest stationAllRequest;
    private DialogCallback dialogCallback;

	@BindView(R.id.deviceLV)		ListView deviceLV;
	@BindView(R.id.deviceSRL)       SwipeRefreshLayout deviceSRL;

	public static SubscribeDeviceDialog newInstance(DialogCallback dialogCallback) {
		SubscribeDeviceDialog subscribeDeviceDialog = new SubscribeDeviceDialog();
        subscribeDeviceDialog.dialogCallback = dialogCallback;
		return subscribeDeviceDialog;
	}

	public static SubscribeDeviceDialog newInstance() {
		SubscribeDeviceDialog fragment = new SubscribeDeviceDialog();
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_subscribe_device;
	}

	@Override
	public void onViewReady() {
		setUpMyDevicesListView();
        attemptFarmAll();
	}

	private void setUpMyDevicesListView() {
		deviceAdapter = new DeviceAdapter(getContext());
		deviceAdapter.setOnItemClickListener(this);
		deviceLV.setAdapter(deviceAdapter);
	}

	@Override
	public void onItemClick(DeviceItem deviceItem) {
		DeviceDetailsDialog.newInstance(deviceItem, this).show(getChildFragmentManager(), DeviceDetailsDialog.TAG);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
//			case R.id.doneBTN:
//				dismiss();
//				break;
		}
	}

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptFarmAll(){
        deviceSRL.setColorSchemeResources(R.color.colorPrimary);
        deviceSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        deviceSRL.setOnRefreshListener(this);
        stationAllRequest = new StationAllRequest(getContext());
        stationAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(deviceSRL)
                .addParameters(Variable.server.key.INCLUDE, "current_weather,location,daily_weather");
    }

    private void refreshList(){
        stationAllRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(StationAllRequest.ServerResponse responseData) {
        StationCollectionTransformer farmCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                deviceAdapter.addNewData(farmCollectionTransformer.data);

            }else{
                deviceAdapter.setNewData(farmCollectionTransformer.data);
            }
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(dialogCallback != null){
            dialogCallback.onDialogDismiss();
        }
        super.onDismiss(dialog);
    }

    @Override
    public void subscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.updateData(deviceItem);
        dialog.dismiss();
    }

    @Override
    public void unsubscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.updateData(deviceItem);
        dialog.dismiss();
    }

    public interface DialogCallback{
        void onDialogDismiss();
    }
}
