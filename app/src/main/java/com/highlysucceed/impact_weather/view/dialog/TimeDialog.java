package com.highlysucceed.impact_weather.view.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import butterknife.BindView;
import icepick.State;

public class TimeDialog extends BaseDialog implements
		View.OnClickListener  {
	public static final String TAG = TimeDialog.class.getName().toString();
	private static final String DEFAULT_FORMAT  = "hh:mm a";

	private final static int TIME_PICKER_INTERVAL = 5;
	private Activity mActivity;

	private TimePickerListener timePickerListener;
    private Calendar calendar;


	//private final String TAG = TimeDialog.this.getClass().getSimpleName();
//	private final static int TIME_PICKER_INTERVAL = 5;
//	private Dialog mDateDialog;
//	private TimePicker timePicker;


	@BindView(R.id.okBTN)	    	TextView okBTN;
	@BindView(R.id.closeBTN)	    View closeBTN;
	@BindView(R.id.timerDP)			TimePicker timerDP;

	@State String selectedTime;
	@State String format;

	public static TimeDialog newInstance(TimePickerListener timePickerListener, String selectedTime) {
		TimeDialog calendarDialog = new TimeDialog();
		calendarDialog.timePickerListener = timePickerListener;
		calendarDialog.selectedTime = selectedTime;
		calendarDialog.format = DEFAULT_FORMAT;
		return calendarDialog;
	}

	public static TimeDialog newInstance(TimePickerListener timePickerListener, String selectedTime, String format) {
		TimeDialog calendarDialog = new TimeDialog();
		calendarDialog.timePickerListener = timePickerListener;
		calendarDialog.selectedTime = selectedTime;
		calendarDialog.format = format;
		return calendarDialog;
	}



	@Override
	public int onLayoutSet() {
		return R.layout.dialog_time;

	}

	@Override
	public void onViewReady() {
		okBTN.setOnClickListener(this);
		closeBTN.setOnClickListener(this);

		SimpleDateFormat timeFormat = new SimpleDateFormat(format);
		calendar = Calendar.getInstance();
		try {
			calendar.setTime(timeFormat.parse(selectedTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		setHour(calendar.get(Calendar.HOUR_OF_DAY));
		setMinute(calendar.get(Calendar.MINUTE));

		timerDP.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);

			}
		});
	}

	public void setHour(int hour) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			timerDP.setHour(hour);
		} else {
			timerDP.setCurrentHour(hour);

		}

	}

	public void setMinute(int minute) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				timerDP.setMinute(minute);
		}
		else
			{
			timerDP.setCurrentMinute(minute);
		}

//		int hour = calendar.get(Calendar.HOUR_OF_DAY);
//		//int minute = calendar.get(Calendar.MINUTE);
//
//		if (((minute % TIME_PICKER_INTERVAL) != 0)) {
//			int minuteFloor = (minute + TIME_PICKER_INTERVAL) - (minute % TIME_PICKER_INTERVAL);
//			minute = minuteFloor + (minute == (minuteFloor + 1) ? TIME_PICKER_INTERVAL : 0);
//			if (minute >= 60) {
//				minute = minute % 60;
//				hour++;
//			}
//
//			timePicker.setCurrentHour(hour);
//			timePicker.setCurrentMinute(minute / TIME_PICKER_INTERVAL);
//		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.okBTN:
				okClicked();
				break;
			case R.id.closeBTN:
				dismiss();
				break;
		}
	}

	private void okClicked(){
		if(timePickerListener != null){
			if (calendar != null) {
				timePickerListener.forTimeDisplay(new SimpleDateFormat(format).format(calendar.getTime()));
			}
		}
		dismiss();
	}

	public interface TimePickerListener{
		void forTimeDisplay(String time);
	}

//	private void setTimePickerInterval(TimePicker timePicker) {
//		try {
//
//			NumberPicker minutePicker = (NumberPicker) timePicker.findViewById(Resources.getSystem().getIdentifier(
//					"minute", "timerDP", "android"));
//			minutePicker.setMinValue(0);
//			minutePicker.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
//			List<String> displayedValues = new ArrayList<String>();
//			for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
//				displayedValues.add(String.format("%02d", i));
//			}
//			minutePicker.setDisplayedValues(displayedValues.toArray(new String[0]));
//		} catch (Exception e) {
//			Log.e(TAG, "Exception: " + e);
//		}
//	}
}
