package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.RecommendationItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendationRecycleViewAdapter extends RecyclerView.Adapter<RecommendationRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<RecommendationItem> data;
    private LayoutInflater layoutInflater;

	public RecommendationRecycleViewAdapter(Context context, List<RecommendationItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}


    public void setNewData(List<RecommendationItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_recommendation, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.recommendationItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);


        holder.titleTXT.setText(holder.recommendationItem.title);
        holder.descriptionTXT.setText(holder.recommendationItem.content);
//        holder.imageCIV.setImageResource(holder.recommendationItem.temp_image);

        switch (holder.recommendationItem.type){
            case "critical":
                holder.view.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_red));
                break;
            case "immediate":
                holder.view.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_orange));
                break;
            case "low":
                holder.view.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_green));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RecommendationItem recommendationItem;

        @BindView(R.id.imageCIV)                ImageView imageCIV;
        @BindView(R.id.titleTXT)                TextView titleTXT;
        @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
