package com.highlysucceed.impact_weather.view.fragment.settings;

import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.SettingsActivity;
import com.highlysucceed.impact_weather.view.dialog.WebViewDialog;

import butterknife.BindView;

public class AboutFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = AboutFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    @BindView(R.id.versionTXT)          TextView versionTXT;
    @BindView(R.id.privacyPolicyTXT)    TextView privacyPolicyTXT;
    @BindView(R.id.termsTXT)            TextView termsTXT;

    public static AboutFragment newInstance() {
        AboutFragment aboutFragment = new AboutFragment();
        return aboutFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle(getString(R.string.about_settings));
        versionTXT.setText(getAppVersion());
        privacyPolicyTXT.setOnClickListener(this);
        termsTXT.setOnClickListener(this);
    }

    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version --";
        }

        return version;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.privacyPolicyTXT:
                WebViewDialog.newInstance("http://privacypolicy.technology/kpp.php").show(getFragmentManager(), TAG);
                break;
            case R.id.termsTXT:
                WebViewDialog.newInstance("http://privacypolicy.technology/matu.php").show(getFragmentManager(), TAG);
                break;
        }
    }
}
