package com.highlysucceed.impact_weather.view.fragment.farm;

import android.app.ProgressDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightGridView;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.util.widget.MultiSwipeRefreshLayout;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.adapter.JournalAdapter;
import com.highlysucceed.impact_weather.view.adapter.JournalCalendarDailyAdapter;
import com.highlysucceed.impact_weather.view.adapter.JournalCalendarMonthlyAdapter;
import com.highlysucceed.impact_weather.view.dialog.AddActivityDialog;
import com.highlysucceed.impact_weather.view.dialog.MonthYearDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;
import com.server.android.model.JournalItem;
import com.server.android.request.journals.JournalsCropRequest;
import com.server.android.request.journals.JournalsDeleteRequest;
import com.server.android.transformer.journals.JournalsCollectionTransformer;
import com.server.android.transformer.journals.JournalsSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class JournalCalendarFragment extends BaseFragment implements View.OnClickListener,
        JournalCalendarDailyAdapter.OnDayClickListener,
        JournalCalendarMonthlyAdapter.OnDayClickListener,
        JournalAdapter.ClickListener,
        MonthYearDialog.DialogCallback,
        SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = JournalCalendarFragment.class.getName().toString();

    private FarmActivity farmActivity;
    private JournalCalendarDailyAdapter journalCalendarDailyAdapter;
    private JournalCalendarMonthlyAdapter journalCalendarMonthlyAdapter;

    @BindView(R.id.journalGV)               ExpandableHeightGridView journalGV;
    @BindView(R.id.journalLV)               ListView journalLV;
    @BindView(R.id.prevBTN)                 View prevBTN;
    @BindView(R.id.nextBTN)		            View nextBTN;
    @BindView(R.id.monthBTN)	            View monthBTN;
    @BindView(R.id.monthViewCON)            View monthViewCON;
    @BindView(R.id.monthTXT)                TextView monthTXT;
    @BindView(R.id.yearTXT)                 TextView yearTXT;
    @BindView(R.id.swipeRefreshLayout)      MultiSwipeRefreshLayout swipeRefreshLayout;


    @BindView(R.id.dayNumTXT)           TextView dayNumTXT;
    @BindView(R.id.dayNameTXT)          TextView dayNameTXT;
    @BindView(R.id.messageTXT)          TextView messageTXT;
    @BindView(R.id.addBTN)              View addBTN;
    @BindView(R.id.farmPlaceHolderCON)  View farmPlaceHolderCON;
    @BindView(R.id.journalEHLV)         ExpandableHeightListView journalEHLV;

    @State String crop;
    @State int farmId;

    @State int month;
    @State int year;

    private String calendarView = "";

    public static JournalCalendarFragment newInstance(int farmId, String crop) {
        JournalCalendarFragment journalCalendarFragment = new JournalCalendarFragment();
        journalCalendarFragment.crop = crop;
        journalCalendarFragment.farmId = farmId;
        return journalCalendarFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_journal_calendar;
    }

    @Override
    public void onViewReady() {
        farmActivity = (FarmActivity) getActivity();
        farmActivity.setTitle("My Journal - " + crop);
        prevBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);
        monthBTN.setOnClickListener(this);
        setUpCalendar();
    }

    private void setUpCalendar(){

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setSwipeableChildren(R.id.journalLV, R.id.monthViewCON);

        journalCalendarDailyAdapter = new JournalCalendarDailyAdapter(getContext());
        journalCalendarDailyAdapter.setOnDayClickListener(this);
        journalLV.setAdapter(journalCalendarDailyAdapter);

        journalCalendarMonthlyAdapter = new JournalCalendarMonthlyAdapter(getContext());
        journalCalendarMonthlyAdapter.setOnDayClickListener(this);
        journalGV.setExpanded(true);
        journalGV.setAdapter(journalCalendarMonthlyAdapter);
        displayMonthYear();
    }

    public void displayMonth(){
        switch (calendarView){
            case "DAY":
                monthTXT.setText(journalCalendarDailyAdapter.getCurrentMonthName());
                break;
            case "MONTH":
                monthTXT.setText(journalCalendarMonthlyAdapter.getCurrentMonthName());
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prevBTN:
                prevMonth();
                break;
            case R.id.nextBTN:
                nextMonth();
                break;
            case R.id.monthBTN:
                openMonthSelector();
                break;
            case R.id.messageTXT:
            case R.id.addBTN:
                farmActivity.openJournalCreateFragment(farmId, crop, ((JournalCalendarMonthlyAdapter.CalendarData) v.getTag()).getFormattedDate());
//                AddActivityDialog.newInstance(farmId, crop, ((JournalCalendarMonthlyAdapter.CalendarData) v.getTag()).getFormattedDate(), null).show(getChildFragmentManager(), AddActivityDialog.TAG);
                break;
        }
    }

    private void openMonthSelector(){
        MonthYearDialog.newInstance(journalCalendarDailyAdapter.getCurrentMonthNum(), journalCalendarDailyAdapter.getCurrentYear(), this).show(getChildFragmentManager(), MonthYearDialog.TAG);
    }

    private void nextMonth(){
        journalCalendarDailyAdapter.nextMonth();
        journalCalendarMonthlyAdapter.nextMonth();
        displayMonthYear();
        displayMonth();
    }

    private void prevMonth(){
        journalCalendarDailyAdapter.prevMonth();
        journalCalendarMonthlyAdapter.prevMonth();
        displayMonthYear();
        displayMonth();
    }

    private void displayMonthYear(){
        monthTXT.setText(journalCalendarDailyAdapter.getCurrentMonthName());
        yearTXT.setText(String.valueOf(journalCalendarDailyAdapter.getCurrentYear()));

        if(journalCalendarDailyAdapter.isActualMonth()){
            journalLV.setSelection(journalCalendarDailyAdapter.getActualDayPosition());
        }else{
            journalLV.setSelection(0);
        }


        displayJournalData(journalCalendarMonthlyAdapter.getSelectedData());

        refreshJournalCrop();
    }

    private void deleteJournal(final int id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to delete?")
                .setIcon(R.drawable.icon_successful)
                .setPositiveButtonText("Delete")
                .setNegativeButtonText("Cancel")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptDelete(id);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }

    private void attemptDelete(int journalId) {
        new JournalsDeleteRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting Journal...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops")
                .addParameters(Variable.server.key.JOURNAL_ID, journalId)
                .execute();
    }



    private void refreshJournalCrop(){
        swipeRefreshLayout.setRefreshing(true);
        new JournalsCropRequest(getContext())
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addParameters(Variable.server.key.INCLUDE, "info, farm")
                .addParameters(Variable.server.key.FARM_ID, farmId)
                .addParameters(Variable.server.key.CROP, crop)
                .addParameters(Variable.server.key.MONTH_YEAR, journalCalendarDailyAdapter.getCurrentMonthName() + " " + journalCalendarDailyAdapter.getCurrentYear())
                .execute();
    }

    @Subscribe
    public void onResponse(JournalsDeleteRequest.ServerResponse responseData) {
        JournalsSingleTransformer journalsSingleTransformer = responseData.getData(JournalsSingleTransformer.class);
        if(journalsSingleTransformer.status){
            journalCalendarDailyAdapter.removeJournalItem(journalsSingleTransformer.data);
            journalCalendarMonthlyAdapter.removeJournalItem(journalsSingleTransformer.data);
            displayJournalData(journalCalendarMonthlyAdapter.getSelectedData());
        }
    }

    @Subscribe
    public void onResponse(JournalsCropRequest.ServerResponse responseData) {
        JournalsCollectionTransformer journalsCollectionTransformer = responseData.getData(JournalsCollectionTransformer.class);
        if(journalsCollectionTransformer.status){
            List<Transform.Holder> holders = new Transform(journalsCollectionTransformer.data).getHolder();
            for(Transform.Holder holder : holders){
                Log.e("response", ">>>" + holder.reference);
                journalCalendarDailyAdapter.addJournalsInDay(holder.reference, holder.items);
                journalCalendarMonthlyAdapter.addJournalsInDay(holder.reference, holder.items);
            }
            journalCalendarDailyAdapter.updateCalendarData();
            journalCalendarMonthlyAdapter.updateCalendarData();
            displayJournalData(journalCalendarMonthlyAdapter.getSelectedData());
        }

    }


//    @Subscribe
//    public void onResponse(JournalsCreateRequest.ServerResponse responseData) {
//        Jou journalsCollectionTransformer = responseData.getData(Jou.class);
//        if(journalsCollectionTransformer.status){
//            List<Transform.Holder> holders = new Transform(journalsCollectionTransformer.data).getHolder();
//            for(Transform.Holder holder : holders){
//                Log.e("response", ">>>" + holder.reference);
//                journalCalendarDailyAdapter.addJournalsInDay(holder.reference, holder.items);
//                journalCalendarMonthlyAdapter.addJournalsInDay(holder.reference, holder.items);
//            }
//            journalCalendarDailyAdapter.updateCalendarData();
//            journalCalendarMonthlyAdapter.updateCalendarData();
//            displayJournalData(journalCalendarMonthlyAdapter.getSelectedData());
//        }
//    }

    @Override
    public void onItemClick(JournalCalendarMonthlyAdapter.CalendarData calendarData) {
        if(calendarData.isOffsetFirst){
            prevMonth();
            journalCalendarMonthlyAdapter.setSelectedDay(calendarData);
        }else if(calendarData.isOffsetLast){
            nextMonth();
            journalCalendarMonthlyAdapter.setSelectedDay(calendarData);
        }else{
            displayJournalData(calendarData);
        }
    }

    private void displayJournalData(JournalCalendarMonthlyAdapter.CalendarData calendarData){
        messageTXT.setTag(calendarData);
        messageTXT.setOnClickListener(this);
        addBTN.setTag(calendarData);
        addBTN.setOnClickListener(this);

        dayNumTXT.setText(String.valueOf(calendarData.getDayNum()));
        dayNameTXT.setText(calendarData.getDayName());

        if(journalCalendarMonthlyAdapter.isActualMonth() && calendarData.getDayNum() == journalCalendarMonthlyAdapter.getActualDay()){
            dayNumTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
            dayNameTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary));
        }else{
            dayNumTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.text_gray));
            dayNameTXT.setTextColor(ActivityCompat.getColor(getContext(), R.color.text_gray));
        }

        if(calendarData.getJournalAdapter() != null && calendarData.getJournalAdapter().getCount() > 0){
            journalEHLV.setVisibility(View.VISIBLE);
            farmPlaceHolderCON.setVisibility(View.GONE);
            journalEHLV.setExpanded(true);
            journalEHLV.setAdapter(calendarData.getJournalAdapter(this));
            messageTXT.setVisibility(View.INVISIBLE);
            messageTXT.setOnClickListener(null);
            addBTN.setOnClickListener(this);
        }else{
            journalEHLV.setVisibility(View.GONE);
            farmPlaceHolderCON.setVisibility(View.VISIBLE);
            messageTXT.setVisibility(View.VISIBLE);
            messageTXT.setOnClickListener(this);
            addBTN.setOnClickListener(this);
        }
    }

    public class Transform{

        List<Holder> holder;

        public Transform(List<JournalItem> raw){
            holder = new ArrayList<>();
            Holder ho = new Holder();
            if (raw.size() > 0){
                ho.reference = raw.get(0).entry_date_db;
            }else {
                ho.reference = "";
            }
            holder.add(ho);
            raw(raw);
        }

        private void raw(List<JournalItem> raw){
            for(JournalItem journalItem : raw){
                holder(journalItem);
            }
        }

        private void holder(JournalItem journalItem){

            boolean found = false;
            for(Holder h : holder){
                if(journalItem.entry_date_db.equals(h.reference)){
                    h.items.add(journalItem);
                    found = true;
                    break;
                }
            }

            if(found){
                return;
            }

            Holder ho = new Holder();
            ho.reference = journalItem.entry_date_db;
            ho.items.add(journalItem);
            holder.add(ho);
        }

        public List<Holder> getHolder(){
            return holder;
        }

        public class Holder{
            private String reference;
            List<JournalItem> items;

            public Holder(){
                reference = "";
                items = new ArrayList<>();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Override
    public void onItemClick(JournalCalendarDailyAdapter.CalendarData calendarData) {

    }

    @Override
    public void onAddClick(JournalCalendarDailyAdapter.CalendarData calendarData) {
        AddActivityDialog.newInstance(farmId, crop, calendarData.getFormattedDate(), null).show(getChildFragmentManager(), AddActivityDialog.TAG);
    }

    @Override
    public void onJournalClickListener(JournalItem journalItem) {
        farmActivity.openJournalCreateFragment(journalItem);
    }

    @Override
    public void onJournalDeleteClickListener(JournalItem journalItem) {
        deleteJournal(journalItem.id);
    }

    @Override
    public void dialogCallback(int month, int year) {
        journalCalendarDailyAdapter.setCurrentMonthNum(month);
        journalCalendarDailyAdapter.setCurrentYear(year);
        journalCalendarDailyAdapter.refresh();

        journalCalendarMonthlyAdapter.setCurrentMonthNum(month);
        journalCalendarMonthlyAdapter.setCurrentYear(year);
        journalCalendarMonthlyAdapter.refresh();
        displayMonthYear();
    }

    @Override
    public void onRefresh() {
        journalCalendarDailyAdapter.refresh();
        journalCalendarMonthlyAdapter.refresh();
        refreshJournalCrop();
    }
}
