package com.highlysucceed.impact_weather.view.dialog;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.view.adapter.CropsSelectorRecyclerViewAdapter;
import com.server.android.model.CropsItem;

import java.util.List;

import butterknife.BindView;

public class CropDialog extends BaseDialog implements
        CropsSelectorRecyclerViewAdapter.ClickListener,
        View.OnClickListener{
	public static final String TAG = CropDialog.class.getName().toString();

	private CropsCallback cropsCallback;
	private List<CropsItem> cropsItems;
	private Object target;
	private LinearLayoutManager linearLayoutManager;
	private CropsSelectorRecyclerViewAdapter cropsSelectorRecyclerViewAdapter;

	@BindView(R.id.mainIconIV)      View mainIconIV;
	@BindView(R.id.cropsRV)         RecyclerView cropsRV;

	public static CropDialog newInstance(List<CropsItem> cropsItems, Object target, CropsCallback cropsCallback) {
		CropDialog cropDialog = new CropDialog();
		cropDialog.cropsItems = cropsItems;
		cropDialog.target = target;
		cropDialog.cropsCallback = cropsCallback;
		return cropDialog;
	}

	public List<CropsItem> getCropsItems() {
		return cropsItems;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_crops;
	}

	@Override
	public void onViewReady() {
        mainIconIV.setOnClickListener(this);
        setupListView();
	}

	private void setupListView(){
        cropsSelectorRecyclerViewAdapter = new CropsSelectorRecyclerViewAdapter(getContext());
        cropsSelectorRecyclerViewAdapter.setOnItemClickListener(this);
        cropsSelectorRecyclerViewAdapter.setNewData(getCropsItems());
        linearLayoutManager = new LinearLayoutManager(getContext());
        cropsRV.setAdapter(cropsSelectorRecyclerViewAdapter);
        cropsRV.setLayoutManager(linearLayoutManager);
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onItemClick(CropsItem cropsItem) {
        if(cropsCallback != null){
            cropsCallback.cropsCallback(cropsItem, target);
        }
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                dismiss();
                break;
        }
    }

    public interface CropsCallback{
		void cropsCallback(CropsItem cropsItem, Object target);
	}
}
