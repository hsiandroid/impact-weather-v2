package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.WeatherItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyForcastAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;

	public DailyForcastAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<WeatherItem> data){
        for(WeatherItem item : data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void addNewData(WeatherItem data){
        this.data.add(0, data);
        notifyDataSetChanged();
    }

    public void updateOrNewData(WeatherItem daily_forecast){

        for(WeatherItem item : data){
            if(daily_forecast.id == item.id){
                data.set(data.indexOf(item), daily_forecast);
                notifyDataSetChanged();
                return;
            }
        }

        this.data.add(0, daily_forecast);
        notifyDataSetChanged();
    }

    public void removeData(WeatherItem daily_forecast){
        for(WeatherItem item : data){
            if(daily_forecast.id == item.id){
                data.remove(data.indexOf(item));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void updateData(WeatherItem daily_forecast){
        for(WeatherItem item : data){
            if(daily_forecast.id == item.id){
                data.set(data.indexOf(item), daily_forecast);
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_dailyforcast, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.daily_forecast = data.get(position);
        holder.view.setOnClickListener(this);


        holder.placeTXT.setText(holder.daily_forecast.read_hr);
        holder.highestTempTXT.setText(holder.daily_forecast.max_temp);
        holder.statusTXT.setText(holder.daily_forecast.status);
        holder.lowestTempTXT.setText(holder.daily_forecast.min_temp);
        holder.rainAmountTXT.setText(holder.daily_forecast.probability_of_precip);
        holder.speedTXT.setText(holder.daily_forecast.wind_speed);
        holder.degreesTXT.setText(holder.daily_forecast.temperature);

        Picasso.with(context)
                .load(holder.daily_forecast.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.weatherIV);

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(viewHolder.daily_forecast);
        }
    }

    public class ViewHolder{

        WeatherItem daily_forecast;

        @BindView(R.id.weatherIV)           ImageView weatherIV;
        @BindView(R.id.placeTXT)            TextView placeTXT;
        @BindView(R.id.degreesTXT)          TextView degreesTXT;
        @BindView(R.id.highestTempTXT)      TextView highestTempTXT;
        @BindView(R.id.lowestTempTXT)       TextView lowestTempTXT;
        @BindView(R.id.rainAmountTXT)       TextView rainAmountTXT;
        @BindView(R.id.speedTXT)            TextView speedTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.windTXT)             TextView windTXT;
        @BindView(R.id.humidityTXT)         TextView humidityTXT;
        @BindView(R.id.backgroundCON)       View backgroundCON;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(WeatherItem daily_forecast);
    }
} 
