package com.highlysucceed.impact_weather.view.dialog;

import android.app.ProgressDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.server.android.model.SettingsItem;
import com.server.android.request.profile.ProfileEditFieldRequest;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class EditProfileDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = EditProfileDialog.class.getName().toString();

	private SettingsItem settingsItem;
	private static final String CONTACT_FORMAT = "(+63) [000] [000] [0000]";

	@BindView(R.id.titleTXT) 	TextView titleTXT;
	@BindView(R.id.contentTXT) 	EditText contentTXT;
	@BindView(R.id.content1TXT) EditText content1TXT;
	@BindView(R.id.content2TXT) EditText content2TXT;
	@BindView(R.id.saveBTN) 	View saveBTN;
	@BindView(R.id.cancelBTN) 	View cancelBTN;
	@BindView(R.id.mainTitleTXT) TextView mainTitleTXT;

	public static EditProfileDialog newInstance(SettingsItem settingsItem) {
		EditProfileDialog editProfileDialog = new EditProfileDialog();
		editProfileDialog.settingsItem = settingsItem;
		return editProfileDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_edit_profile;
	}

	@Override
	public void onViewReady() {
		titleTXT.setText(settingsItem.title);
		contentTXT.setHint("Enter " + settingsItem.title);
		content1TXT.setVisibility(View.GONE);
		content2TXT.setVisibility(View.GONE);

		switch (settingsItem.key){
			case "name":
				mainTitleTXT.setText("Update Name");
				contentTXT.setText(settingsItem.value);
				contentTXT.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
				break;
			case "email":
                mainTitleTXT.setText("Update Email");
				contentTXT.setText(settingsItem.value);
				contentTXT.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				break;
			case "contact":
                mainTitleTXT.setText("Update Mobile Number");
				contentTXT.setInputType(InputType.TYPE_CLASS_PHONE);
				onMaskedTextChangedListener(contentTXT);
				break;
		}
		saveBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
	}

	@Override
		public void onClick(View view) {
			switch (view.getId()){
				case R.id.saveBTN:
					switch (settingsItem.key){
						case "name":
						case "email":
							attemptEdit();
								break;
						case "contact":
							attemptEditContact();
							break;
					}
					break;
                case  R.id.cancelBTN:
                    dismiss();
                    break;
		}
	}

	public void onMaskedTextChangedListener(EditText editText){

		MaskedTextChangedListener maskedTextChangedListener = new MaskedTextChangedListener(
				CONTACT_FORMAT,
				true,
				editText,
				null,
				null);

		editText.addTextChangedListener(maskedTextChangedListener);
		editText.setOnFocusChangeListener(maskedTextChangedListener);

		editText.setText(settingsItem.value);
	}

	private void attemptEdit(){
		ProfileEditFieldRequest profileEditFieldRequest = new ProfileEditFieldRequest(getContext());
		profileEditFieldRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating profile details...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Variable.server.key.INCLUDE, "info,image,owner,delivery_info")
				.addParameters(Variable.server.key.KEY, settingsItem.key)
				.addParameters(Variable.server.key.VALUE, contentTXT.getText().toString())
				.execute();
	}

	private void attemptEditContact(){
		String content = contentTXT.getText().toString();
		Mask.Result result = new Mask(CONTACT_FORMAT).apply(new CaretString(content,content.length()),true);
		ProfileEditFieldRequest profileEditFieldRequest = new ProfileEditFieldRequest(getContext());
		profileEditFieldRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating profile details...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Variable.server.key.INCLUDE, "info,image,owner,delivery_info")
				.addParameters(Variable.server.key.KEY, settingsItem.key)
				.addParameters(Variable.server.key.VALUE, "+63" + result.getExtractedValue())
				.execute();
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(ProfileEditFieldRequest.ServerResponse responseData) {
		UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
		if(userSingleTransformer.status){
			UserData.insert(userSingleTransformer.data);
			dismiss();
			ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
			ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
			if(userSingleTransformer.hasRequirements()){
//				ErrorResponseManger.first(contentTXT, userSingleTransformer.requires.value);
				Log.e("field", ">>>" + ErrorResponseManger.first(userSingleTransformer.requires.key));
				Log.e("value", ">>>" + ErrorResponseManger.first(userSingleTransformer.requires.value));
			}else{
				Log.e("Error", ">>>Error");
			}
		}
	}
}
