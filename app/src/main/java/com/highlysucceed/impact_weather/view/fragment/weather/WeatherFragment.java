package com.highlysucceed.impact_weather.view.fragment.weather;

import android.app.ProgressDialog;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.WeatherActivity;
import com.highlysucceed.impact_weather.view.adapter.DailyWeatherRecycleViewAdapter;
import com.highlysucceed.impact_weather.view.adapter.HourlyWeatherRecycleViewAdapter;
import com.highlysucceed.impact_weather.view.dialog.MeteogramDialog;
import com.highlysucceed.impact_weather.view.dialog.WeatherDetailDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;
import com.server.android.model.DeviceItem;
import com.server.android.model.WeatherItem;
import com.server.android.request.station.StationInfoRequest;
import com.server.android.request.station.StationSubscribeRequest;
import com.server.android.request.station.StationUnSubscribeRequest;
import com.server.android.transformer.station.StationSingleTransformer;
import com.server.android.transformer.station.StationSubscriptionSingleTransformer;
import com.server.android.util.Variable;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class WeatherFragment extends BaseFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        DailyWeatherRecycleViewAdapter.ClickListener,
        HourlyWeatherRecycleViewAdapter.ClickListener{

    public static final String TAG = WeatherFragment.class.getName().toString();

    private WeatherActivity weatherActivity;
    private StationInfoRequest stationInfoRequest;

    private HourlyWeatherRecycleViewAdapter hourlyWeatherRecycleViewAdapter;
    private DailyWeatherRecycleViewAdapter dailyWeatherRecycleViewAdapter;

    @BindView(R.id.weatherSRL)                      SwipeRefreshLayout weatherSRL;
    @BindView(R.id.weatherCON)                      View weatherCON;
    @BindView(R.id.subscribeBTN)                    TextView subscribeBTN;

    @BindView(R.id.hourlyPlaceholderCON)            View hourlyPlaceholderCON;
    @BindView(R.id.dailyPlaceholderCON)             View dailyPlaceholderCON;

    @BindView(R.id.weatherIV)                       ImageView weatherIV;
    @BindView(R.id.meteogramIV)                     ImageView meteogramIV;
    @BindView(R.id.hintTXT)                         TextView hintTXT;
    @BindView(R.id.placeTXT)                        TextView placeTXT;
    @BindView(R.id.statusTXT)                       TextView statusTXT;
    @BindView(R.id.degreesTXT)                      TextView degreesTXT;
    @BindView(R.id.highestTempTXT)                  TextView highestTempTXT;
    @BindView(R.id.lowestTempTXT)                   TextView lowestTempTXT;
    @BindView(R.id.rainAmountTXT)                   TextView rainAmountTXT;
    @BindView(R.id.speedTXT)                        TextView speedTXT;

    @BindView(R.id.weatherSV)                       NestedScrollView weatherSV;
    @BindView(R.id.hourlyRV)                        RecyclerView hourlyRV;
    @BindView(R.id.dailyRV)                         RecyclerView dailyRV;

    @State int deviceId;

    public static WeatherFragment newInstance(int deviceId) {
        WeatherFragment weatherFragment = new WeatherFragment();
        weatherFragment.deviceId = deviceId;
        return weatherFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_weather;
    }

    @Override
    public void onViewReady() {
        weatherActivity = (WeatherActivity) getActivity();

        subscribeBTN.setOnClickListener(this);
        meteogramIV.setOnClickListener(this);
        hintTXT.setOnClickListener(this);

        setUpHourlyWeatherListView();
        setUpDailyWeatherListView();

        attemptShow();
        refreshList();

    }

    private void displayData(DeviceItem deviceItem){

        weatherActivity.setTitle(deviceItem.name + " (" + deviceItem.code + ")");

        Picasso.with(getContext())
                .load(deviceItem.current_weather.data.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(weatherIV);

        Picasso.with(getContext())
                .load(deviceItem.getMeteogram())
                .into(meteogramIV);

        meteogramIV.setTag(deviceItem.getMeteogram());
        statusTXT.setTag(deviceItem.name + " (" + deviceItem.code + ")");

        statusTXT.setText(deviceItem.current_weather.data.status);

        placeTXT.setText(deviceItem.name);
        placeTXT.setSelected(true);
        highestTempTXT.setText(deviceItem.current_weather.data.max_temp);
        lowestTempTXT.setText(deviceItem.current_weather.data.min_temp);
        rainAmountTXT.setText(deviceItem.current_weather.data.probability_of_precip);
        speedTXT.setText(deviceItem.current_weather.data.wind_speed);
        degreesTXT.setText(deviceItem.current_weather.data.temperature);


        hourlyWeatherRecycleViewAdapter.setNewData(deviceItem.daily_weather.data);
        dailyWeatherRecycleViewAdapter.setNewData(weatherActivity.transformDailyWeather(deviceItem.weekly_weather.data));

        if(deviceItem.is_subscribed){
            subscribeBTN.setText("UNSUBSCRIBE");
            subscribeBTN.setTag(deviceItem.subscription_id);
        }else{
            subscribeBTN.setText("SUBSCRIBE");
            subscribeBTN.setTag(deviceItem.id);
        }
    }

    private void setUpHourlyWeatherListView() {
        hourlyWeatherRecycleViewAdapter = new HourlyWeatherRecycleViewAdapter(getContext());
        hourlyRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        hourlyRV.setAdapter(hourlyWeatherRecycleViewAdapter);
    }

    private void setUpDailyWeatherListView() {
        dailyWeatherRecycleViewAdapter = new DailyWeatherRecycleViewAdapter(getContext());
        dailyWeatherRecycleViewAdapter.setOnItemClickListener(this);
        dailyRV.setLayoutManager(new LinearLayoutManager(getContext()));
        dailyRV.setAdapter(dailyWeatherRecycleViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.subscribeBTN:
                if(subscribeBTN.getText().toString().equals("SUBSCRIBE")){
                    attemptSubscribe((int) v.getTag());
                }else{
                    unsubscribe((int) v.getTag());
                }
                break;
            case R.id.hintTXT:
            case R.id.meteogramIV:
                MeteogramDialog.newInstance(statusTXT.getTag().toString(), meteogramIV.getTag().toString()).show(getChildFragmentManager(), MeteogramDialog.TAG);
                break;
        }
    }

    private void unsubscribe(final int id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog.setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptUnSubscribe(id);
                    }
                })
                .setDescription("Are you sure you want to Unsubscribe this device?")
                .setIcon(R.drawable.icon_information)
                .show(getChildFragmentManager(),confirmationDialog.TAG);
    }

    private void attemptSubscribe(final int id){
        StationSubscribeRequest stationSubscribeRequest = new StationSubscribeRequest(getContext());
        stationSubscribeRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Subscribing to device...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Variable.server.key.INCLUDE, "info, station.current_weather, station.location, station.daily_weather")
                .addParameters(Variable.server.key.STATION_ID, id)
                .execute();
    }

    private void attemptUnSubscribe(int subscriptionId){
        StationUnSubscribeRequest unSubscribeRequest = new StationUnSubscribeRequest(getContext());
        unSubscribeRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Processing request...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Variable.server.key.INCLUDE, "info,station.current_weather,station.location,station.recommendations")
                .addParameters(Variable.server.key.SUBSCRIPTION_ID, subscriptionId)
                .execute();
    }

    private void attemptShow(){

        weatherSRL.setColorSchemeResources(R.color.colorPrimary);
        weatherSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        weatherSRL.setOnRefreshListener(this);

        stationInfoRequest = new StationInfoRequest(getContext());
        stationInfoRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(weatherSRL)
//                .addParameters(Variable.server.key.INCLUDE, "info,daily_weather,current_weather,weekly_weather,location,recommendations")
                .addParameters(Variable.server.key.INCLUDE, "info,daily_weather,current_weather,weekly_weather")
                .addParameters(Variable.server.key.STATION_ID, deviceId);
    }

    private void refreshList(){
        weatherCON.setVisibility(View.GONE);
        subscribeBTN.setVisibility(View.GONE);
        stationInfoRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(StationInfoRequest.ServerResponse responseData) {
        StationSingleTransformer stationSingleTransformer = responseData.getData(StationSingleTransformer.class);
        if(stationSingleTransformer.status){
            weatherCON.setVisibility(View.VISIBLE);
            subscribeBTN.setVisibility(View.VISIBLE);
            displayData(stationSingleTransformer.data);
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
            weatherActivity.onBackPressed();
        }
        if (hourlyWeatherRecycleViewAdapter.getItemCount() == 0){
            hourlyPlaceholderCON.setVisibility(View.VISIBLE);
            hourlyRV.setVisibility(View.GONE);
        }else {
            hourlyPlaceholderCON.setVisibility(View.GONE);
            hourlyRV.setVisibility(View.VISIBLE);
        }
        if (dailyWeatherRecycleViewAdapter.getItemCount() == 0){
            dailyPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            dailyPlaceholderCON.setVisibility(View.GONE);
        }
    }


    @Subscribe
    public void onResponse(StationSubscribeRequest.ServerResponse responseData) {
        StationSubscriptionSingleTransformer stationSingleTransformer = responseData.getData(StationSubscriptionSingleTransformer.class);
        if(stationSingleTransformer.status){
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            subscribeBTN.setText("UNSUBSCRIBE");
            subscribeBTN.setTag(stationSingleTransformer.data.id);
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(StationUnSubscribeRequest.ServerResponse responseData) {
        StationSubscriptionSingleTransformer stationSingleTransformer = responseData.getData(StationSubscriptionSingleTransformer.class);
        if(stationSingleTransformer.status){
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            weatherActivity.onBackPressed();
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onHourItemClick(WeatherItem weatherItem) {
        weatherActivity.startRecommendationActivity(0, "view");
    }

    @Override
    public void onDailyItemClick(WeatherItem weatherItem) {
        WeatherDetailDialog.newInstance(weatherItem).show(getChildFragmentManager(), WeatherDetailDialog.TAG);
    }
}
