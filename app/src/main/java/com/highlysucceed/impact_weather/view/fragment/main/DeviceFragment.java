package com.highlysucceed.impact_weather.view.fragment.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.highlysucceed.impact_weather.util.widget.MultiSwipeRefreshLayout;
import com.server.android.model.DeviceItem;
import com.server.android.request.station.StationNearbyRequest;
import com.server.android.request.station.StationOwnedRequest;
import com.server.android.transformer.station.StationCollectionTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.PermissionChecker;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.DeviceAdapter;
import com.highlysucceed.impact_weather.view.dialog.DeviceDetailsDialog;
import com.highlysucceed.impact_weather.view.dialog.SubscribeDeviceDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;

public class DeviceFragment extends BaseFragment implements DeviceAdapter.ClickListener,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SubscribeDeviceDialog.DialogCallback,
        DeviceDetailsDialog.Callback {

    public static final String TAG = DeviceFragment.class.getName().toString();

    private MainActivity mainActivity;
    private StationOwnedRequest stationOwnedRequest;
    private StationNearbyRequest stationNearbyRequest;

    private static final int REQUEST_GPS = 8445;
    private static final int PERMISSION_LOCATION = 8445;

    private DeviceAdapter subscribedDeviceAdapter;
    private DeviceAdapter nearByDeviceAdapter;

    private GoogleApiClient googleApiClient;


    @BindView(R.id.myDevicesEHLV)                   ExpandableHeightListView myDevicesEHLV;
    @BindView(R.id.nearByEHLV)                      ExpandableHeightListView nearByEHLV;
    @BindView(R.id.nearbyCON)                       View nearbyCON;
    @BindView(R.id.subscribeCON)                    View subscribeCON;
    @BindView(R.id.subscribeBTN)                    View subscribeBTN;
    @BindView(R.id.devicePlaceholderIV)             View devicePlaceholderIV;
    @BindView(R.id.nearbydevicePlaceholderIV)       View nearbydevicePlaceholderIV;
    @BindView(R.id.actionBTN)                       View actionBTN;
    @BindView(R.id.searchingCON)                    View searchingCON;
    @BindView(R.id.placeholderTXT)                  TextView placeholderTXT;
    @BindView(R.id.placeholderIV)                   ImageView placeholderIV;
    @BindView(R.id.deviceSRL)                       MultiSwipeRefreshLayout deviceSRL;
    @BindView(R.id.loadMoreBTN)                     View loadMoreBTN;

    public static DeviceFragment newInstance() {
        DeviceFragment deviceFragment = new DeviceFragment();
        return deviceFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_device;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.my_devices_title));
        mainActivity.hideFarmBTN();
        subscribeBTN.setOnClickListener(this);

        deviceSRL.setColorSchemeResources(R.color.colorPrimary);
        deviceSRL.setOnRefreshListener(this);
        deviceSRL.setSwipeableChildren(R.id.deviceSV);

        actionBTN.setOnClickListener(this);
        loadMoreBTN.setOnClickListener(this);

        setUpMyDevicesListView();
        attemptStationAll();
        setUpNearByDevicesListView();
    }

    public void setGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

    private boolean isGPSOn() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled;
        boolean networkEnabled;
        try {
            gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (gpsEnabled) {
                return true;
            }
        } catch (Exception ex) {
        }

        try {
            networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (networkEnabled) {
                return true;
            }
        } catch (Exception ex) {

        }

        return false;
    }

    public void openGPS(){
        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_LOCATION)){
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(myIntent, REQUEST_GPS);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void setUpMyDevicesListView() {
        subscribedDeviceAdapter = new DeviceAdapter(getContext());
        myDevicesEHLV.setAdapter(subscribedDeviceAdapter);
        myDevicesEHLV.setExpanded(true);
        subscribedDeviceAdapter.setOnItemClickListener(this);

    }

    private void setUpNearByDevicesListView() {
        nearByDeviceAdapter = new DeviceAdapter(getContext());
        nearByEHLV.setAdapter(nearByDeviceAdapter);
        nearByEHLV.setExpanded(true);
        nearByDeviceAdapter.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(DeviceItem deviceItem) {
//        if(deviceItem.is_subscribed){
            mainActivity.startWeatherActivity(deviceItem.id, "weather");
//        }else{
//            DeviceDetailsDialog.newInstance(deviceItem, this).show(getChildFragmentManager(), DeviceDetailsDialog.TAG);
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.subscribeBTN:
//                SubscribeDeviceDialog.newInstance(this).show(getChildFragmentManager(), SubscribeDeviceDialog.TAG);
                mainActivity.startMyDeviceActivity("all");
                break;
            case R.id.actionBTN:
            case R.id.placeholderTXT:
                openGPS();
                break;
            case R.id.loadMoreBTN:
                deviceSRL.setRefreshing(true);
                stationOwnedRequest.nextPage();
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
            if(requestCode== REQUEST_GPS){

            }
    }

    private void attemptSearchNearby(Location location){
        searchingCON.setVisibility(View.VISIBLE);

        stationNearbyRequest = new StationNearbyRequest(getContext());
        stationNearbyRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Variable.server.key.INCLUDE, "current_weather,location,daily_weather")
                .addParameters(Variable.server.key.INCLUDE, "current_weather,location")
                .addParameters(Variable.server.key.USER_LAT, location.getLatitude())
                .addParameters(Variable.server.key.USER_LONG, location.getLongitude())
                .execute();
    }

    private void attemptStationAll(){
        deviceSRL.setColorSchemeResources(R.color.colorPrimary);
        deviceSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        deviceSRL.setOnRefreshListener(this);
        stationOwnedRequest = new StationOwnedRequest(getContext());
        stationOwnedRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(deviceSRL)
//                .addParameters(Variable.server.key.INCLUDE, "current_weather,location,daily_weather");
                .addParameters(Variable.server.key.INCLUDE, "current_weather, location");
    }

    private void refreshList(){
        if(isGPSOn()){
               actionBTN.setVisibility(View.GONE);
               placeholderTXT.setOnClickListener(null);
            if (nearByDeviceAdapter.getCount() == 0){
                nearbydevicePlaceholderIV.setVisibility(View.VISIBLE);
                placeholderTXT.setText(getString(R.string.placeholder_nearby_device));
                placeholderIV.setImageResource(R.drawable.placeholder_my_device);
                placeholderTXT.setOnClickListener(null);
            }else {
                nearbydevicePlaceholderIV.setVisibility(View.GONE);
            }
        }else{
               nearbydevicePlaceholderIV.setVisibility(View.VISIBLE);
               actionBTN.setVisibility(View.GONE);
               placeholderTXT.setText("GPS is required or tap to activate GPS setting.");
               placeholderIV.setImageResource(R.drawable.placeholder_gps);
               placeholderTXT.setOnClickListener(this);

            nearByDeviceAdapter.setNewData(new ArrayList<DeviceItem>());
        }

        setGoogleApiClient();
        stationOwnedRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(StationOwnedRequest.ServerResponse responseData) {
        StationCollectionTransformer farmCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                subscribedDeviceAdapter.addNewData(farmCollectionTransformer.data);
            }else{
                subscribedDeviceAdapter.setNewData(farmCollectionTransformer.data);
            }
        }
        deviceSRL.setRefreshing(false);
        loadMoreBTN.setVisibility(farmCollectionTransformer.has_morepages ? View.VISIBLE : View.GONE);
        if (subscribedDeviceAdapter.getCount() == 0){
            devicePlaceholderIV.setVisibility(View.VISIBLE);
        }else {
            devicePlaceholderIV.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(StationNearbyRequest.ServerResponse responseData) {
        StationCollectionTransformer farmCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                nearByDeviceAdapter.addNewData(farmCollectionTransformer.data);
            }else{
                nearByDeviceAdapter.setNewData(farmCollectionTransformer.data);
            }
        }

        if(nearByDeviceAdapter.getCount() == 0){
            nearbydevicePlaceholderIV.setVisibility(View.VISIBLE);
            placeholderIV.setImageResource(R.drawable.placeholder_nearby_device);
            placeholderTXT.setText(getString(R.string.placeholder_nearby_device));
            placeholderTXT.setOnClickListener(null);
        }else {
            nearbydevicePlaceholderIV.setVisibility(View.GONE);
        }
        searchingCON.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("onConnected", ">>>Success");
        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_LOCATION)){
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
//                location.setLatitude(23.633020);
//                location.setLongitude(102.4431109);
                attemptSearchNearby(location);
                Log.e("Lat", ">>>" + location.getLatitude());
                Log.e("Long", ">>>" + location.getLongitude());
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("onConnected", ">>>Failed");
    }


    @Override
    public void onDialogDismiss() {
        refreshList();
    }

    @Override
    public void subscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        nearByDeviceAdapter.updateData(deviceItem);
        subscribedDeviceAdapter.addNewData(deviceItem);
        dialog.dismiss();
    }

    @Override
    public void unsubscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        nearByDeviceAdapter.updateData(deviceItem);
        subscribedDeviceAdapter.removeData(deviceItem);
        dialog.dismiss();
    }
}
