package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.server.android.model.WeatherItem;
import com.highlysucceed.impact_weather.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HourlyWeatherAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;

	public HourlyWeatherAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_hourly_weather, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.weatherItem = data.get(position);
        holder.view.setOnClickListener(this);

        holder.timeTXT.setText(holder.weatherItem.read_hr);
        holder.temperatureTXT.setText(holder.weatherItem.temperature);
        holder.rainAmountTXT.setText(holder.weatherItem.probability_of_precip);
        holder.speedTXT.setText(holder.weatherItem.wind_speed);
        holder.statusTXT.setText("(" + holder.weatherItem.status + ")");

        Picasso.with(context)
                .load(holder.weatherItem.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.weatherIV);

		return convertView;
	}

    public class ViewHolder{
        WeatherItem weatherItem;
        @BindView(R.id.timeTXT)         TextView timeTXT;
        @BindView(R.id.weatherIV)       ImageView weatherIV;
        @BindView(R.id.temperatureTXT)  TextView temperatureTXT;
        @BindView(R.id.rainAmountTXT)   TextView rainAmountTXT;
        @BindView(R.id.speedTXT)        TextView speedTXT;
        @BindView(R.id.statusTXT)       TextView statusTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onHourItemClick(viewHolder.weatherItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onHourItemClick(WeatherItem weatherItem);
    }
} 
