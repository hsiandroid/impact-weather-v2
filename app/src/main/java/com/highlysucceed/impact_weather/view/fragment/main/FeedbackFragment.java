package com.highlysucceed.impact_weather.view.fragment.main;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.FarmSpinnerAdapter;
import com.highlysucceed.impact_weather.view.dialog.ImagePickerDialog;
import com.server.android.request.farm.FarmAllRequest;
import com.server.android.request.inquiry.InquiryAdvisoryRequest;
import com.server.android.transformer.farm.FarmCollectionTransformer;
import com.server.android.util.BaseTransformer;
import com.server.android.util.Variable;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import icepick.State;

public class FeedbackFragment extends BaseFragment implements
        View.OnClickListener,
        ImagePickerDialog.ImageCallback  {

    public static final String TAG = FeedbackFragment.class.getName().toString();

    private MainActivity mainActivity;
    private FarmAllRequest farmAllRequest;
    private File file;

    @BindView(R.id.imageIV)         ImageView imageIV;
    @BindView(R.id.uploadBTN)       View uploadBTN;
    @BindView(R.id.messageET)       TextView messageET;
    @BindView(R.id.submitBTN)       View submitBTN;
    @BindView(R.id.farmSPR)         Spinner farmSPR;

    @State int farm_id;

    private FarmSpinnerAdapter activitySpinnerAdapter;

    public static FeedbackFragment newInstance() {
        FeedbackFragment settingsFragment = new FeedbackFragment();
        return settingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_feedback;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle("FEEDBACK ADVISORY");
        mainActivity.hideFarmBTN();
        imageIV.setOnClickListener(this);
        uploadBTN.setOnClickListener(this);
        submitBTN.setOnClickListener(this);
        attemptFarmAll();
        setUpSPR();
    }

    private void attemptFarmAll(){
        farmAllRequest = new FarmAllRequest(getContext());
        farmAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info, crops");
        farmAllRequest.first();
    }

    private void setUpSPR(){
        activitySpinnerAdapter = new FarmSpinnerAdapter(getContext());
        farmSPR.setAdapter(activitySpinnerAdapter);
        farmSPR.getBackground().setColorFilter(ActivityCompat.getColor(getContext(),R.color.white), PorterDuff.Mode.SRC_ATOP);

        farmSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               farm_id = activitySpinnerAdapter.getFarmId(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    @Subscribe
    public void onResponse(FarmAllRequest.ServerResponse responseData) {
        FarmCollectionTransformer farmCollectionTransformer = responseData.getData(FarmCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                activitySpinnerAdapter.addNewData(farmCollectionTransformer.data);

            }else{
                activitySpinnerAdapter.setNewData(farmCollectionTransformer.data);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageIV:
            case R.id.uploadBTN:
                pickImage();
                break;
            case R.id.submitBTN:
                sendInquiry();
                break;
        }
    }

    private void pickImage(){
        ImagePickerDialog.newInstance("Add Photo", false, this).show(getChildFragmentManager(),ImagePickerDialog.TAG);
    }

    private void sendInquiry(){
        InquiryAdvisoryRequest inquiryAdvisoryRequest = new InquiryAdvisoryRequest(getContext());
        inquiryAdvisoryRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Submitting feedback...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addRequestBodyParameters(Variable.server.key.CONTENT, messageET.getText().toString())
                .addRequestBodyParameters(Variable.server.key.FARM_ID, farm_id);

        if(file != null){
            inquiryAdvisoryRequest.addPart(Variable.server.key.FILE, file);
        }

        inquiryAdvisoryRequest.execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(InquiryAdvisoryRequest.ServerResponse responseData) {
        BaseTransformer userSingleTransformer = responseData.getData(BaseTransformer.class);
        if(userSingleTransformer.status){
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            file = null;
            Picasso.with(getContext())
                    .load(R.drawable.icon_feedback)
                    .into(imageIV);
            mainActivity.openNewHomeFragment(UserData.getInt(UserData.DEFAULT_FARM_ID));
        }else{
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext())
                .load(file)
                .into(imageIV);
    }
}
