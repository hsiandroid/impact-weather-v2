package com.highlysucceed.impact_weather.view.activity;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.registration.ForgotPasswordFragment;
import com.highlysucceed.impact_weather.view.fragment.registration.LoginFragment;
import com.highlysucceed.impact_weather.view.fragment.registration.MobileConfirmationFragment;
import com.highlysucceed.impact_weather.view.fragment.registration.SignUpFragment;
import com.highlysucceed.impact_weather.view.fragment.registration.SplashFragment;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RegistrationActivity extends RouteActivity implements View.OnClickListener{
    public static final String TAG = RegistrationActivity.class.getName().toString();

    @BindView(R.id.titleBarCON)         View titleBarCON;
    @BindView(R.id.mainIconIV)          View mainIconIV;
    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;

    public static final boolean activeSMS = true;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_registration;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openSplashFragment();
    }

    public void openSplashFragment(){
        switchFragment(SplashFragment.newInstance(), false, R.anim.slide_in_right);
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance(), R.anim.slide_in_right);
    }

    public void openSignUpFragment(){
        switchFragment(SignUpFragment.newInstance(), R.anim.slide_in_right);
    }

    public void openForgotPasswordFragment(){
        switchFragment(ForgotPasswordFragment.newInstance(), R.anim.slide_in_right);
    }

    public void openMobileConfirmationFragment(String contact){
        switchFragment(MobileConfirmationFragment.newInstance(contact));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void showTitleBar(boolean b){
        titleBarCON.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }
}
