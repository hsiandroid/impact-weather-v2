package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.RecommendationItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendationAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<RecommendationItem> data;
    private LayoutInflater layoutInflater;

	public RecommendationAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

	public RecommendationAdapter(Context context, List<RecommendationItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<RecommendationItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_recommendation, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.recommendationItem = data.get(position);
        holder.view.setOnClickListener(this);

        holder.titleTXT.setText(holder.recommendationItem.title);
        switch (holder.recommendationItem.type){
            case "warning":
                holder.statusTXT.setTextColor(ActivityCompat.getColor(context, R.color.light_orange));
                holder.statusTXT.setText("Warning");
                break;
            default:
                holder.statusTXT.setTextColor(ActivityCompat.getColor(context, R.color.light_green));
                holder.statusTXT.setText("Recommended");
                break;
        }

		return convertView;
	}



    public class ViewHolder{
        RecommendationItem recommendationItem;

        @BindView(R.id.titleTXT)                TextView titleTXT;
        @BindView(R.id.statusTXT)               TextView statusTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onRecommendationClick(viewHolder.recommendationItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onRecommendationClick(RecommendationItem recommendationItem);
    }
} 
