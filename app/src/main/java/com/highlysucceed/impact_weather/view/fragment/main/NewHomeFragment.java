package com.highlysucceed.impact_weather.view.fragment.main;


import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.AdvisoryAdapter;
import com.highlysucceed.impact_weather.view.adapter.DailyForcastAdapter;
import com.highlysucceed.impact_weather.view.adapter.HourlyWeatherRecycleViewAdapter;
import com.highlysucceed.impact_weather.view.adapter.WeeklyWeatherRecycleViewAdapter;
import com.highlysucceed.impact_weather.view.dialog.MeteogramDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ComingSoonDialog;
import com.server.android.model.AdvisoryItem;
import com.server.android.model.FarmItem;
import com.server.android.model.WeatherItem;
import com.server.android.newAPI.config.Keys;
import com.server.android.newAPI.request.Farm;
import com.server.android.newAPI.server.request.APIRequest;
import com.server.android.newAPI.server.transformer.CollectionTransformer;
import com.server.android.newAPI.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import icepick.State;

public class NewHomeFragment extends BaseFragment implements
        View.OnClickListener,
        DailyForcastAdapter.ClickListener,
        HourlyWeatherRecycleViewAdapter.ClickListener{

    public static final String TAG = NewHomeFragment.class.getName().toString();
    private MainActivity mainActivity;
    private APIRequest apiRequest;
    private List<AdvisoryItem> advisoryItems;
    private DailyForcastAdapter deviceAdapter;
    private HourlyWeatherRecycleViewAdapter hourlyWeatherRecycleViewAdapter;
    private WeeklyWeatherRecycleViewAdapter weeklyWeatherRecycleViewAdapter;

    @BindView(R.id.nameTXT)                 TextView nameTXT;
    @BindView(R.id.farmNameTXT)             TextView farmNameTXT;
    @BindView(R.id.farmSizeTXT)             TextView farmSizeTXT;
    @BindView(R.id.farmVegiTXT)             TextView farmVegiTXT;
    @BindView(R.id.deviceLV)                ExpandableHeightListView deviceLV;
    @BindView(R.id.farmCON)                 View farmCON;
    @BindView(R.id.farmInfoCON)             View farmInfoCON;
    @BindView(R.id.addFarmCON)              View addFarmCON;
    @BindView(R.id.weatherCON)              View weatherCON;
    @BindView(R.id.weatherIV)               ImageView weatherIV;
    @BindView(R.id.placeTXT)                TextView placeTXT;
    @BindView(R.id.degreesTXT)              TextView degreesTXT;
    @BindView(R.id.highestTempTXT)          TextView highestTempTXT;
    @BindView(R.id.lowestTempTXT)           TextView lowestTempTXT;
    @BindView(R.id.rainAmountTXT)           TextView rainAmountTXT;
    @BindView(R.id.speedTXT)                TextView speedTXT;
    @BindView(R.id.statusTXT)               TextView statusTXT;
    @BindView(R.id.windTXT)                 TextView windTXT;
    @BindView(R.id.humidityTXT)             TextView humidityTXT;
    @BindView(R.id.feelsLikeTXT)            TextView feelsLikeTXT;
    @BindView(R.id.advisoryTXT)            TextView advisoryTXT;
    @BindView(R.id.advisoryCON)            View advisoryCON;

    @BindView(R.id.chanceOfTXT)             TextView chanceOfTXT;
    @BindView(R.id.hourlyRV)                RecyclerView hourlyRV;
    @BindView(R.id.weeklyRV)                RecyclerView weeklyRV;
    @BindView(R.id.homeSV)                  NestedScrollView homeSV;
    @BindView(R.id.addBTN)                  FloatingActionMenu addBTN;
    @BindView(R.id.addActBTN)               FloatingActionMenu addActBTN;
    @BindView(R.id.addActivityBTN)          FloatingActionButton addActivityBTN;
    @BindView(R.id.diseaseBTN)              FloatingActionButton diseaseBTN;
    @BindView(R.id.meteogramIV)             ImageView meteogramIV;
    @BindView(R.id.farmEditBTN)             ImageView farmEditBTN;
    @BindView(R.id.farmInfoBTN)             ImageView farmInfoBTN;

    @State int farm_id;
    @State String crop;
    @State String meteoGraph;

    public static NewHomeFragment newInstance(int farm_id) {
        NewHomeFragment homeFragment = new NewHomeFragment();
        homeFragment.farm_id = farm_id;
        return homeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_new_home;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_home));
        mainActivity.showFarmBTN();
        Log.e("EEEEEEEEE", "ID:  " + String.valueOf(UserData.getInt(UserData.DEFAULT_FARM_ID)));
        addFarmCON.setOnClickListener(this);
        farmInfoCON.setOnClickListener(this);
        if (farm_id != 0){
            farmCON.setVisibility(View.VISIBLE);
            weatherCON.setVisibility(View.VISIBLE);
            addFarmCON.setVisibility(View.GONE);
        }else{
            farmCON.setVisibility(View.GONE);
            addFarmCON.setVisibility(View.VISIBLE);
            weatherCON.setVisibility(View.GONE);
        }
        nameTXT.setOnClickListener(this);
        setUpMyDevicesListView();
        setUpHourlyWeatherListView();
        setUpWeeklyWeatherListView();
        addActivityBTN.setOnClickListener(this);
        addActBTN.setOnClickListener(this);
        diseaseBTN.setOnClickListener(this);
        setUPFarm(farm_id);
        farmEditBTN.setOnClickListener(this);
        farmInfoBTN.setOnClickListener(this);
        meteogramIV.setOnClickListener(this);
        advisoryTXT.setOnClickListener(this);
    }

    private void setUpMyDevicesListView() {
        deviceAdapter = new DailyForcastAdapter(getContext());
        deviceAdapter.setOnItemClickListener(this);
        deviceLV.setAdapter(deviceAdapter);
    }

    private void setUpHourlyWeatherListView() {
        hourlyWeatherRecycleViewAdapter = new HourlyWeatherRecycleViewAdapter(getContext());
        hourlyRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        hourlyRV.setAdapter(hourlyWeatherRecycleViewAdapter);
    }

    private void setUpWeeklyWeatherListView() {
        weeklyWeatherRecycleViewAdapter = new WeeklyWeatherRecycleViewAdapter(getContext());
        weeklyRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        weeklyRV.setAdapter(weeklyWeatherRecycleViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.farmInfoBTN:
                mainActivity.startMyFarmActivity(farm_id, "details");
                break;
            case R.id.farmEditBTN:
                mainActivity.startMyFarmActivity(farm_id, "edit");
                break;
            case R.id.addActivityBTN:
                mainActivity.startJournalActivity(farm_id, crop, "journal");
                break;
            case R.id.addFarmCON:
                mainActivity.startMyFarmActivity(0,"create");
                break;
            case R.id.diseaseBTN:
                ComingSoonDialog.Builder().show(getFragmentManager(), TAG);
                break;
            case R.id.addActBTN:
                mainActivity.startJournalActivity(farm_id, crop, "journal");
                break;
                case R.id.meteogramIV:
                    MeteogramDialog.newInstance(statusTXT.getText().toString(), meteoGraph).show(getChildFragmentManager(), MeteogramDialog.TAG);
                break;
            case R.id.advisoryTXT:
                mainActivity.openAdvisoryFragment(advisoryItems);
                break;
        }
    }

    public void setUPFarm(int id){
       apiRequest = Farm.getDefault().show(getContext(), id).addAuthorization(UserData.getString(UserData.AUTHORIZATION));
       apiRequest.first();
    }



    @Subscribe
    public void onResponse(Farm.AdvisoryResponse responseData) {
        CollectionTransformer<AdvisoryItem> singleTransformer = responseData.getData(CollectionTransformer.class);
        if(singleTransformer.status){
//            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            if (singleTransformer.num_advisory.equalsIgnoreCase("0")){
                advisoryCON.setVisibility(View.GONE);
            }else { advisoryCON.setVisibility(View.VISIBLE);}
            advisoryTXT.setText(singleTransformer.advisory_display);
            advisoryItems = singleTransformer.data;
        }else{
//            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
            advisoryCON.setVisibility(View.GONE);
        }
    }


    @Subscribe
    public void onResponse(Farm.EditResponse responseData) {
        SingleTransformer<FarmItem> singleTransformer = responseData.getData(SingleTransformer.class);
        if(singleTransformer.status){
            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            nameTXT.setText(singleTransformer.data.name);
            farmNameTXT.setText(singleTransformer.data.name);
            farmSizeTXT.setText(String.valueOf(singleTransformer.data.size) + " sqm");
            farmVegiTXT.setText(singleTransformer.data.crops.data.get(0).name + " - " + singleTransformer.data.crops.data.get(0).variety );
            deviceAdapter.setNewData(singleTransformer.data.daily_forecast.data);
            hourlyWeatherRecycleViewAdapter.setNewData(singleTransformer.data.daily_forecast.data);
            weeklyWeatherRecycleViewAdapter.setNewData(singleTransformer.data.weekly_forecast.data);

            placeTXT.setText(singleTransformer.data.current_forecast.data.date);
            highestTempTXT.setText("Day " + singleTransformer.data.current_forecast.data.max_temp);
            statusTXT.setText(singleTransformer.data.current_forecast.data.status);
            lowestTempTXT.setText("Night " + singleTransformer.data.current_forecast.data.min_temp);
            rainAmountTXT.setText(singleTransformer.data.current_forecast.data.probability_of_precip);
            speedTXT.setText(singleTransformer.data.current_forecast.data.wind_speed);
            degreesTXT.setText(singleTransformer.data.current_forecast.data.temperature + "C");
            chanceOfTXT.setText(singleTransformer.data.current_forecast.data.probability_of_precip + " chance of raining");
            feelsLikeTXT.setText("Feels like " + singleTransformer.data.current_forecast.data.feeled_temperature);
            farmInfoCON.setVisibility(View.VISIBLE);
            crop = singleTransformer.data.crops.data.get(0).name;
            meteoGraph = singleTransformer.data.station.data.meteogramImage;
            Glide.with(getContext()).load(singleTransformer.data.station.data.meteogramImage).into(meteogramIV);

            Picasso.with(getContext())
                    .load(singleTransformer.data.current_forecast.data.image)
                    .placeholder(R.drawable.icon_weather)
                    .error(R.drawable.icon_weather)
                    .into(weatherIV);

            int temp = singleTransformer.data.current_forecast.data.precip_percentage;
            if(temp < 50 ){
                chanceOfTXT.setBackgroundResource(R.color.tranparent);
            }else if(temp >= 50){
                chanceOfTXT.setBackgroundResource(R.color.deep_orange);
            }else if (temp >= 75){
                chanceOfTXT.setBackgroundResource(R.color.red_orange);
            }else{
                chanceOfTXT.setBackgroundResource(R.color.red);
            }

//           if (singleTransformer.data.crops.data.get(0).name.equalsIgnoreCase("potato")){
//                addBTN.setVisibility(View.VISIBLE);
//                addActBTN.setVisibility(View.GONE);
//           }else{
//               addBTN.setVisibility(View.GONE);
//               addActBTN.setVisibility(View.VISIBLE);
//           }

        }else{
//            ToastMessage.show(getActivity(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    private void refreshList(){
        Farm.getDefault().advisory(getContext()).addAuthorization(UserData.getString(UserData.AUTHORIZATION)).addParameter(Keys.FARM_ID, farm_id).execute();
    }

    @Override
    public void onItemClick(WeatherItem daily_forecast) {

    }

    @Override
    public void onHourItemClick(WeatherItem weatherItem) {
        mainActivity.startRecommendationActivity(0, "view");
    }
}
