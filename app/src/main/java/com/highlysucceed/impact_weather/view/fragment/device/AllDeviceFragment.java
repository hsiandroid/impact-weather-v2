package com.highlysucceed.impact_weather.view.fragment.device;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.DeviceActivity;
import com.highlysucceed.impact_weather.view.adapter.DeviceAdapter;
import com.highlysucceed.impact_weather.view.dialog.DeviceDetailsDialog;
import com.server.android.model.DeviceItem;
import com.server.android.request.station.StationAllRequest;
import com.server.android.transformer.station.StationCollectionTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class AllDeviceFragment extends BaseFragment implements DeviceAdapter.ClickListener,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        DeviceDetailsDialog.Callback{

	public static final String TAG = AllDeviceFragment.class.getName().toString();

	private DeviceAdapter deviceAdapter;
    private DeviceActivity deviceActivity;
    private StationAllRequest stationAllRequest;

	@BindView(R.id.deviceLV)		ListView deviceLV;
	@BindView(R.id.deviceSRL)       SwipeRefreshLayout deviceSRL;
	@BindView(R.id.loadMoreBTN)     View loadMoreBTN;

	public static AllDeviceFragment newInstance() {
		AllDeviceFragment allDeviceFragment = new AllDeviceFragment();
		return allDeviceFragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_subscribe_device;
	}

	@Override
	public void onViewReady() {
        deviceActivity = (DeviceActivity) getActivity();
        deviceActivity.setTitle("Weather Stations");
		setUpMyDevicesListView();
        attemptFarmAll();
        loadMoreBTN.setOnClickListener(this);
	}

	private void setUpMyDevicesListView() {
		deviceAdapter = new DeviceAdapter(getContext());
		deviceAdapter.setOnItemClickListener(this);
		deviceLV.setAdapter(deviceAdapter);
	}

	@Override
	public void onItemClick(DeviceItem deviceItem) {
        deviceActivity.startWeatherActivity(deviceItem.id, "weather");
//		DeviceDetailsDialog.newInstance(deviceItem, this).show(getChildFragmentManager(), DeviceDetailsDialog.TAG);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
            case R.id.loadMoreBTN:
                deviceSRL.setRefreshing(true);
                stationAllRequest.nextPage();
                break;
		}
	}

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptFarmAll(){
        deviceSRL.setColorSchemeResources(R.color.colorPrimary);
        deviceSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        deviceSRL.setOnRefreshListener(this);
        stationAllRequest = new StationAllRequest(getContext());
        stationAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(deviceSRL)
                .setPerPage(10)
//                .addParameters(Variable.server.key.INCLUDE, "current_weather,location,daily_weather");
                .addParameters(Variable.server.key.INCLUDE, "current_weather");
    }

    private void refreshList(){
        stationAllRequest
                .showSwipeRefreshLayout(true)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(StationAllRequest.ServerResponse responseData) {
        StationCollectionTransformer farmCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                deviceAdapter.addNewData(farmCollectionTransformer.data);

            }else{
                deviceAdapter.setNewData(farmCollectionTransformer.data);
            }

            loadMoreBTN.setVisibility(farmCollectionTransformer.has_morepages ? View.VISIBLE : View.GONE);
        }
        deviceSRL.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void subscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.updateData(deviceItem);
        dialog.dismiss();
    }

    @Override
    public void unsubscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.updateData(deviceItem);
        dialog.dismiss();
    }
}
