package com.highlysucceed.impact_weather.view.fragment.recommendation;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.server.android.model.RecommendationItem;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.SampleData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.RecommendationActivity;
import com.highlysucceed.impact_weather.view.adapter.RecommendationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class RecommendationFragment extends BaseFragment implements RecommendationAdapter.ClickListener,SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = RecommendationFragment.class.getName().toString();

    private RecommendationActivity recommendationActivity;

    private List<RecommendationItem> recommendationItems;
    private RecommendationAdapter recommendationAdapter;

    @BindView(R.id.recommendationEHLV)  ExpandableHeightListView recommendationEHLV;
    @BindView(R.id.recommendationCON)   View recommendationCON;
    @BindView(R.id.recommendationSRL)   SwipeRefreshLayout recommendationSRL;

    public static RecommendationFragment newInstance() {
        RecommendationFragment recommendationFragment = new RecommendationFragment();
        return recommendationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_recommendation;
    }

    @Override
    public void onViewReady() {
        recommendationActivity = (RecommendationActivity) getActivity();
        recommendationActivity.setTitle(getString(R.string.title_recommendation));

        setUpRecommendationListView();
        sampleData();

        recommendationSRL.setColorSchemeResources(R.color.colorPrimary);
        recommendationSRL.setOnRefreshListener(this);
    }

    private void setUpRecommendationListView() {

        recommendationItems = new ArrayList<>();
        recommendationAdapter = new RecommendationAdapter(getContext());
        recommendationEHLV.setExpanded(true);
        recommendationEHLV.setAdapter(recommendationAdapter);

        recommendationAdapter.setOnItemClickListener(this);
    }

    private void sampleData(){
        recommendationAdapter.setNewData(SampleData.getRecommendationItems(3));
    }

    @Override
    public void onRecommendationClick(RecommendationItem recommendationItem) {
        recommendationActivity.openRecommendationDetailFragment(recommendationItem);
    }

    @Override
    public void onRefresh() {

        recommendationCON.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recommendationSRL.setRefreshing(false);
                recommendationCON.setVisibility(View.VISIBLE);
            }
        }, 2000);
    }
}
