package com.highlysucceed.impact_weather.view.activity;

import android.graphics.drawable.AnimationDrawable;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.adapter.WidgetAdapter;
import com.highlysucceed.impact_weather.view.dialog.AllFarmDialog;
import com.highlysucceed.impact_weather.view.dialog.LogoutDialog;
import com.highlysucceed.impact_weather.view.fragment.main.AdvisoryFragment;
import com.highlysucceed.impact_weather.view.fragment.main.DeviceFragment;
import com.highlysucceed.impact_weather.view.fragment.main.FarmFragment;
import com.highlysucceed.impact_weather.view.fragment.main.FeedbackFragment;
import com.highlysucceed.impact_weather.view.fragment.main.HomeFragment;
import com.highlysucceed.impact_weather.view.fragment.main.NewHomeFragment;
import com.highlysucceed.impact_weather.view.fragment.main.ProfileFragment;
import com.highlysucceed.impact_weather.view.fragment.main.SettingsFragment;
import com.highlysucceed.impact_weather.view.fragment.settings.ChangePasswordFragment;
import com.server.android.model.AdvisoryItem;
import com.server.android.model.UserItem;
import com.server.android.model.WidgetItem;
import com.server.android.request.profile.ProfileAvatarRequest;
import com.server.android.request.profile.ProfileEditFieldRequest;
import com.server.android.transformer.user.UserSingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class MainActivity extends RouteActivity
        implements View.OnClickListener,WidgetAdapter.ClickListener, AllFarmDialog.Callback{

    private List<WidgetItem> widgetItem;
    private WidgetAdapter widgetAdapter;

    @BindView(R.id.drawerLayout)    DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)  NavigationView navigationView;
    @BindView(R.id.navMenuGV)       GridView navMenuGV;

    @BindView(R.id.mainIconIV)      View mainIconIV;
    @BindView(R.id.mainTitleTXT)    TextView mainTitleTXT;
    @BindView(R.id.navNameTXT)      TextView navNameTXT;
    @BindView(R.id.navContactTXT)   TextView navContactTXT;
    @BindView(R.id.navImageCIV)     ImageView navImageCIV;
    @BindView(R.id.farmBTN)         ImageView farmBTN;
    @BindView(R.id.refreshBTN)      ImageView refreshBTN;
    @BindView(R.id.hideNavIV)       View hideNavIV;
    @BindView(R.id.editProfileBTN)  View editProfileBTN;


    @State int farmID = UserData.getInt(UserData.DEFAULT_FARM_ID)    ;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE);
        disableNavigationViewScrollbars();
        navigationView.getBackground().setAlpha(180);
        mainIconIV.setOnClickListener(this);
        hideNavIV.setOnClickListener(this);
        navImageCIV.setOnClickListener(this);
        editProfileBTN.setOnClickListener(this);
        hideNavigationDrawer();

        setUpWidgetGridView();

        widgetData();

        updateProfileDetail(UserData.getUserItem());

        farmBTN.setOnClickListener(this);

        refreshBTN.setOnClickListener(this);

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openNewHomeFragment(UserData.getInt(UserData.DEFAULT_FARM_ID));
    }

    public void openHomeFragment(){
        switchFragment(HomeFragment.newInstance());
    }

    public void openNewHomeFragment(int id){ switchFragment(NewHomeFragment.newInstance(id)); }

    public void openMyProfileFragment(){
      switchFragment(ProfileFragment.newInstance());
    }

    public void openSettingsFragment(){
      switchFragment(SettingsFragment.newInstance());
    }

    public void openMyFarmFragment(){
      switchFragment(FarmFragment.newInstance());
    }

    public void openDeviceFragment(){
      switchFragment(DeviceFragment.newInstance());
    }

    public void openFeedbackFragment(){
      switchFragment(FeedbackFragment.newInstance());
    }

    public void openChangePass(){
        switchFragment(ChangePasswordFragment.newInstance());
    }

    public void openAdvisoryFragment(List<AdvisoryItem> advisoryItems){
        switchFragment(AdvisoryFragment.newInstance(advisoryItems));
    }


    public void openLogoutDialogFragment(){
        LogoutDialog logoutDialog = LogoutDialog.newInstance();
        logoutDialog.show(getSupportFragmentManager(), logoutDialog.TAG);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart(){
        drawerLayout.closeDrawer(GravityCompat.START);
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showNavigationDrawer(){
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    public void showFarmBTN(){ farmBTN.setVisibility(View.VISIBLE);
        refreshBTN.setVisibility(View.VISIBLE);}


    public void hideNavigationDrawer(){
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void hideFarmBTN(){
        farmBTN.setVisibility(View.GONE);
        refreshBTN.setVisibility(View.GONE);
    }



    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    public void setSelectedItem(int itemId) {

        for (int pos = 0; pos < navigationView.getMenu().size(); pos++) {
            navigationView.getMenu().getItem(pos).setChecked(false);
        }
    }

    private void setUpWidgetGridView() {

        widgetItem = new ArrayList<>();
        widgetAdapter = new WidgetAdapter(getBaseContext(), widgetItem);
        navMenuGV.setAdapter(widgetAdapter);
        widgetAdapter.setOnItemClickListener(this);
    }

    private void widgetData(){
        widgetItem.clear();
        WidgetItem widgetItems = new WidgetItem();
        widgetItems.id = 1;
        widgetItems.name ="HOME";
        widgetItems.image = R.drawable.home;
        widgetItem.add(widgetItems);

        widgetItems = new WidgetItem();
        widgetItems.id = 2;
        widgetItems.name ="PROFILE";
        widgetItems.image = R.drawable.profile;
        widgetItem.add(widgetItems);

        widgetItems = new WidgetItem();
        widgetItems.id = 3;
        widgetItems.name ="FARM";
        widgetItems.image = R.drawable.icon_map_farm;
        widgetItem.add(widgetItems);

//        if(UserData.getUserItem().allow_weather_station.equalsIgnoreCase("yes")){
//            widgetItems = new WidgetItem();
//            widgetItems.id = 4;
//            widgetItems.name ="DEVICES";
//            widgetItems.image = R.drawable.devices;
//            widgetItem.add(widgetItems);
//        }

        widgetItems = new WidgetItem();
        widgetItems.id = 5;
        widgetItems.name ="FEEDBACK ADVISORY";
        widgetItems.image = R.drawable.advisory;
        widgetItem.add(widgetItems);

        widgetItems = new WidgetItem();
        widgetItems.id = 6;
        widgetItems.name ="LOGOUT";
        widgetItems.image = R.drawable.logout;
        widgetItem.add(widgetItems);

//        widgetItems = new WidgetItem();
//        widgetItems.id = 5;
//        widgetItems.name ="Settings";
//        widgetItems.image = R.drawable.icon_settings;
//        widgetItem.add(widgetItems);
//
//        widgetItems = new WidgetItem();
//        widgetItems.id = 6;
//        widgetItems.name ="Sign Out";
//        widgetItems.image = R.drawable.icon_sign_out;
//        widgetItem.add(widgetItems);
    }

    private void updateProfileDetail(UserItem userItem){
        navNameTXT.setText(userItem.name);
        navContactTXT.setText(userItem.contact);

        Picasso.with(getContext())
                .load(userItem.image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(navImageCIV);
    }

    @Subscribe
    public void onResponse(ProfileEditFieldRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            updateProfileDetail(userSingleTransformer.data);
        }
    }

    @Subscribe
    public void onResponse(ProfileAvatarRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            updateProfileDetail(userSingleTransformer.data);
        }
    }

    public void setUpFarmID(int id){
        farmID = id;
    }


    @Override
    public void onItemClick(int position, View v) {
        WidgetItem widgetItems = widgetItem.get(position);

        switch (widgetItems.id){
            case 1:
                openNewHomeFragment(UserData.getInt(UserData.DEFAULT_FARM_ID) );
                break;
            case 2:
//                openMyProfileFragment();
                openSettingsFragment();
                break;
            case 3:
//                openMyFarmFragment();
                AllFarmDialog.Builder(this).show(getSupportFragmentManager(), AllFarmDialog.TAG);
                break;
            case 4:
                openDeviceFragment();
                break;
            case 5:
                openFeedbackFragment();
                break;
            case 6:
                openLogoutDialogFragment();
                break;
        }
        hideNavigationDrawer();
    }

    @Override
    public void onItemLongClick(int position, View v) {

    }

    private void disableNavigationViewScrollbars() {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                showNavigationDrawer();
                break;
            case R.id.hideNavIV:
                hideNavigationDrawer();
                break;
            case R.id.farmBTN:
                AllFarmDialog.Builder(this).show(getSupportFragmentManager(), AllFarmDialog.TAG);
                break;

            case R.id.refreshBTN:
                openNewHomeFragment(farmID);
                break;

            case R.id.editProfileBTN:
            case R.id.navImageCIV:
//                openMyProfileFragment();
                openSettingsFragment();
                hideNavigationDrawer();
                break;
        }
    }

    @Override
    public void onSuccess(int id) {
        openNewHomeFragment(id);
        farmID = id;
    }
}
