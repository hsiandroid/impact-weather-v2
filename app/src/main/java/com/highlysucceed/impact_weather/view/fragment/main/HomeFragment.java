package com.highlysucceed.impact_weather.view.fragment.main;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.impact_weather.util.lib.StringFormatter;
import com.highlysucceed.impact_weather.util.widget.MultiSwipeRefreshLayout;
import com.server.android.model.AdvertisementItem;
import com.server.android.model.DeviceItem;
import com.server.android.model.FarmItem;
import com.server.android.request.farm.FarmAllRequest;
import com.server.android.request.station.StationOwnedRequest;
import com.server.android.transformer.farm.FarmCollectionTransformer;
import com.server.android.transformer.station.StationCollectionTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.DeviceAdapter;
import com.highlysucceed.impact_weather.view.adapter.FarmAdapter;
import com.highlysucceed.impact_weather.view.dialog.DeviceDetailsDialog;
import com.highlysucceed.impact_weather.view.dialog.SubscribeDeviceDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class HomeFragment extends BaseFragment implements FarmAdapter.ClickListener,
        DeviceAdapter.ClickListener,
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        SubscribeDeviceDialog.DialogCallback,
        DeviceDetailsDialog.Callback{

    public static final String TAG = HomeFragment.class.getName().toString();
    private MainActivity mainActivity;

    private FarmAllRequest farmAllRequest;
    private StationOwnedRequest stationOwnedRequest;

    private FarmAdapter farmAdapter;
    private DeviceAdapter deviceAdapter;

    @BindView(R.id.homeEHLV)                ExpandableHeightListView homeEHLV;
    @BindView(R.id.myDevicesEHLV)           ExpandableHeightListView myDevicesEHLV;
    @BindView(R.id.myFarmBTN)               View myFarmBTN;
    @BindView(R.id.myDevicesBTN)            View myDevicesBTN;
    @BindView(R.id.addFarmBTN)              View addFarmBTN;
    @BindView(R.id.weatherCON)              View weatherCON;
    @BindView(R.id.subscribeBTN)            View subscribeBTN;
    @BindView(R.id.farmPB)                  View farmPB;
    @BindView(R.id.devicePB)                View devicePB;
    @BindView(R.id.farmPlaceHolderCON)      View farmPlaceHolderCON;
    @BindView(R.id.devicePlaceholderCON)    View devicePlaceholderCON;
    @BindView(R.id.devicesCON)              View devicesCON;
    @BindView(R.id.loadMoreBTN)             View loadMoreBTN;
    @BindView(R.id.adsIV)                   ImageView adsIV;
    @BindView(R.id.homeSRL)                 MultiSwipeRefreshLayout homeSRL;

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_home));
        mainActivity.hideFarmBTN();
        myFarmBTN.setOnClickListener(this);
        myDevicesBTN.setOnClickListener(this);
        addFarmBTN.setOnClickListener(this);
        subscribeBTN.setOnClickListener(this);
        weatherCON.setOnClickListener(this);
        adsIV.setOnClickListener(this);
        loadMoreBTN.setOnClickListener(this);

        homeSRL.setColorSchemeResources(R.color.colorPrimary);
        homeSRL.setOnRefreshListener(this);
        homeSRL.setSwipeableChildren(R.id.homeSV);

        setUpHomeListView();
        setUpMyDevicesListView();

        attemptFarmAll();

        if (UserData.getUserItem().allow_weather_station.equalsIgnoreCase("yes")){
            attemptStationAll();
        }else{
            devicesCON.setVisibility(View.GONE);
        }

        setUpAds();
    }

    private void setUpAds(){
        AdvertisementItem advertisementItem = UserData.getAdvertisementItem();
        if(advertisementItem.id > 0){
            adsIV.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(advertisementItem.getImage())
                    .into(adsIV);
        }else{
            adsIV.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptFarmAll(){
        farmAllRequest = new FarmAllRequest(getContext());
        farmAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info,crops");
//                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops");
    }

    private void attemptStationAll(){
        stationOwnedRequest = new StationOwnedRequest(getContext());
        stationOwnedRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "current_weather, location");
//                .addParameters(Variable.server.key.INCLUDE, "current_weather, location, daily_weather");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FarmAllRequest.ServerResponse responseData) {
        farmPB.setVisibility(View.GONE);
        FarmCollectionTransformer farmCollectionTransformer = responseData.getData(FarmCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                farmAdapter.addNewData(farmCollectionTransformer.data);

            }else{
                farmAdapter.setNewData(farmCollectionTransformer.data);
            }

        }
        if (farmCollectionTransformer.data.size() == 0){
            farmPlaceHolderCON.setVisibility(View.VISIBLE);
        }else {
            farmPlaceHolderCON.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(StationOwnedRequest.ServerResponse responseData) {

        devicePB.setVisibility(View.GONE);

        StationCollectionTransformer stationCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(stationCollectionTransformer.status){
            if(responseData.isNext()){
                deviceAdapter.addNewData(stationCollectionTransformer.data);
            }else{
                deviceAdapter.setNewData(stationCollectionTransformer.data);
            }

            loadMoreBTN.setVisibility(stationCollectionTransformer.has_morepages ? View.VISIBLE : View.GONE);
        }

        homeSRL.setRefreshing(false);
        if (deviceAdapter.getCount() == 0){
            devicePlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            devicePlaceholderCON.setVisibility(View.GONE);
        }
    }

    private void setUpHomeListView() {
        farmAdapter = new FarmAdapter(getContext());
        farmAdapter.setOnItemClickListener(this);
        homeEHLV.setAdapter(farmAdapter);
    }

    private void setUpMyDevicesListView() {
        deviceAdapter = new DeviceAdapter(getContext());
        deviceAdapter.setOnItemClickListener(this);
        myDevicesEHLV.setAdapter(deviceAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myFarmBTN:
                mainActivity.openMyFarmFragment();
                break;
            case R.id.addFarmBTN:
                mainActivity.startMyFarmActivity(0, "create");
                break;
            case R.id.weatherCON:
                mainActivity.startWeatherActivity(0, "weather");
                break;
            case R.id.subscribeBTN:
//                SubscribeDeviceDialog.newInstance(this).show(getChildFragmentManager(), SubscribeDeviceDialog.TAG);
                mainActivity.startMyDeviceActivity("all");
                break;
            case R.id.adsIV:
                openAds();
                break;
            case R.id.loadMoreBTN:
                homeSRL.setRefreshing(true);
                stationOwnedRequest.nextPage();
                break;
        }
    }

    private void openAds(){
        AdvertisementItem advertisementItem = UserData.getAdvertisementItem();
        if(!StringFormatter.isEmpty(advertisementItem.web_link)){
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(advertisementItem.web_link));
            try {
                startActivity(webIntent);
            } catch (ActivityNotFoundException ex) {

            }
        }

    }

    @Override
    public void onItemClick(FarmItem farmItem) {
        mainActivity.startMyFarmActivity(farmItem.id, "details");
    }

    @Override
    public void onItemClick(DeviceItem deviceItem) {
        if(deviceItem.is_subscribed){
            mainActivity.startWeatherActivity(deviceItem.id, "weather");
        }else{
            DeviceDetailsDialog.newInstance(deviceItem, this).show(getChildFragmentManager(), DeviceDetailsDialog.TAG);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList(){
        homeSRL.setRefreshing(false);
        refreshFarm();
        if (UserData.getUserItem().allow_weather_station.equalsIgnoreCase("yes")){
            refreshDevice();
        }else{
            devicesCON.setVisibility(View.GONE);
        }

    }

    private void refreshFarm(){
        farmPB.setVisibility(View.VISIBLE);
        farmAllRequest.first();
    }


    private void refreshDevice(){
        devicePB.setVisibility(View.VISIBLE);
        stationOwnedRequest.first();
    }

    @Override
    public void onDialogDismiss() {
        if (UserData.getUserItem().allow_weather_station.equalsIgnoreCase("yes")){
            refreshDevice();
        }else{
            devicesCON.setVisibility(View.GONE);
        }
    }

    @Override
    public void subscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.updateOrNewData(deviceItem);
        dialog.dismiss();
    }

    @Override
    public void unsubscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog) {
        deviceAdapter.removeData(deviceItem);
        dialog.dismiss();
    }
}
