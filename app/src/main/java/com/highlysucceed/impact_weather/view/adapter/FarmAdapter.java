package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.FarmItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FarmAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<FarmItem> data;
    private LayoutInflater layoutInflater;

	public FarmAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<FarmItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<FarmItem> data){
        for(FarmItem farmItem : data){
            this.data.add(farmItem);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_home, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);

        holder.farmItem = data.get(position);
        holder.placeTXT.setText(holder.farmItem.name);
        holder.sizeTXT.setText("Farm area: " + holder.farmItem.size + " sqm");
//        holder.criticalTXT.setText(holder.farmItem.status.data.critical);
//        holder.moderateTXT.setText(holder.farmItem.status.data.moderate);
//        holder.lowTXT.setText(holder.farmItem.status.data.low);

        if(holder.farmItem.crops.data.size() == 1){
            holder.productTXT.setText(holder.farmItem.crops.data.get(0).name);
        }else{
            holder.productTXT.setText(holder.farmItem.crops.data.size() + " crops");
        }
		return convertView;
	}

	public class ViewHolder{
        FarmItem farmItem;

        @BindView(R.id.placeTXT)               TextView placeTXT;
        @BindView(R.id.sizeTXT)                 TextView sizeTXT;
        @BindView(R.id.productTXT)             TextView productTXT;
        @BindView(R.id.criticalTXT)            TextView criticalTXT;
        @BindView(R.id.moderateTXT)            TextView moderateTXT;
        @BindView(R.id.lowTXT)                 TextView lowTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(viewHolder.farmItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(FarmItem farmItem);
    }
} 
