package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.lib.StringFormatter;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.server.android.model.JournalItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JournalAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<JournalItem> data;
    private LayoutInflater layoutInflater;

	public JournalAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

	public JournalAdapter(Context context, List<JournalItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<JournalItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<JournalItem> data){
        for(JournalItem item : data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void addItem(JournalItem item){
        this.data.add(item);
        notifyDataSetChanged();
    }

    public List<JournalItem> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_journal, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.setJournalItem(data.get(position));

        holder.view.setOnClickListener(this);
        holder.deleteBTN.setTag(holder.journalItem);
        holder.deleteBTN.setOnClickListener(this);
        holder.titleTXT.setText(holder.journalItem.title);
        if(holder.journalItem.start_time.equals("12:00 AM") && holder.journalItem.end_time.equals("12:00 AM")){
            holder.timeTXT.setText("All day");
        }else{
            holder.timeTXT.setText(holder.journalItem.start_time + " - " + holder.journalItem.end_time);
        }
        String brand = StringFormatter.isEmpty(holder.journalItem.brand) ? "" : "( " + holder.journalItem.brand + " )";
        holder.quantityTXT.setText(holder.journalItem.qty + " " + brand);

        if(holder.journalItem.recommendationItems.size() == 0){
            holder.recommendationEHLV.setVisibility(View.GONE);
        }else{
            holder.recommendationEHLV.setVisibility(View.VISIBLE);
            holder.recommendationEHLV.setExpanded(true);
            holder.recommendationEHLV.setAdapter(holder.getRecommendationAdapter());
        }
		return convertView;
	}

    public void removeItem(JournalItem journalItem){
        for(JournalItem item : data){
            if(item.id == journalItem.id){
                data.remove(data.indexOf(item));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.deleteBTN:
                if(clickListener != null){
                    clickListener.onJournalDeleteClickListener((JournalItem) v.getTag());
                }
                break;
            default:
                ViewHolder viewHolder = (ViewHolder) v.getTag();
                if(clickListener != null){
                    clickListener.onJournalClickListener(viewHolder.journalItem);
                }
        }
    }

    public class ViewHolder{
        JournalItem journalItem;
        RecommendationAdapter recommendationAdapter;

        @BindView(R.id.titleTXT)                TextView titleTXT;
        @BindView(R.id.timeTXT)                 TextView timeTXT;
        @BindView(R.id.quantityTXT)             TextView quantityTXT;
        @BindView(R.id.deleteBTN)               View deleteBTN;
        @BindView(R.id.recommendationEHLV)      ExpandableHeightListView recommendationEHLV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public void setJournalItem(JournalItem journalItem){
            this.journalItem = journalItem;

            if(journalItem.recommendationItems == null){
                journalItem.recommendationItems = new ArrayList<>();
            }
            recommendationAdapter = new RecommendationAdapter(context, journalItem.recommendationItems);
        }

        public RecommendationAdapter getRecommendationAdapter(){
            return recommendationAdapter;
        }
	}

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onJournalClickListener(JournalItem journalItem);
        void onJournalDeleteClickListener(JournalItem journalItem);
    }
} 
