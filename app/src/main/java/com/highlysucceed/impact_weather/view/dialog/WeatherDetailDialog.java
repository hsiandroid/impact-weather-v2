package com.highlysucceed.impact_weather.view.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.util.widget.HorizontalListView;
import com.highlysucceed.impact_weather.view.adapter.HourlyWeatherAdapter;
import com.server.android.model.WeatherItem;

import butterknife.BindView;

public class WeatherDetailDialog extends BaseDialog implements View.OnClickListener,
		HourlyWeatherAdapter.ClickListener{
	public static final String TAG = WeatherDetailDialog.class.getName().toString();

	private HourlyWeatherAdapter hourlyWeatherAdapter;

	@BindView(R.id.dayTitleTXT)				    TextView dayTitleTXT;
	@BindView(R.id.closeBTN)				    TextView closeBTN;
	@BindView(R.id.hourlyEHGV)					HorizontalListView hourlyEHGV;

	private WeatherItem weatherItem;
	public static WeatherDetailDialog newInstance(WeatherItem weatherItem) {
		WeatherDetailDialog weatherDetailDialog = new WeatherDetailDialog();
		weatherDetailDialog.weatherItem = weatherItem;
		return weatherDetailDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_weather_detail;
	}

	@Override
	public void onViewReady() {
		dayTitleTXT.setText(weatherItem.day);
		closeBTN.setOnClickListener(this);
		setUpHourlyWeatherListView();
	}

	private void setUpHourlyWeatherListView() {
		hourlyWeatherAdapter = new HourlyWeatherAdapter(getContext());
		hourlyWeatherAdapter.setNewData(weatherItem.hourly_weather);
		hourlyWeatherAdapter.setOnItemClickListener(this);
		hourlyEHGV.setAdapter(hourlyWeatherAdapter);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogCustomSize(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.closeBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onHourItemClick(WeatherItem weatherItem) {
		((RouteActivity) getContext()).startRecommendationActivity(0, "view");
	}
}
