package com.highlysucceed.impact_weather.view.activity;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.TemplateFragment;

import icepick.State;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class TemplateActivity extends RouteActivity {
    public static final String TAG = TemplateActivity.class.getName().toString();

    @State public String sampleStr;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }
    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openTemplateFragment();
    }

    public void openTemplateFragment(){
        switchFragment(TemplateFragment.newInstance());
    }

}
