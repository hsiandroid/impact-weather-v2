package com.highlysucceed.impact_weather.view.dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.PermissionChecker;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.util.widget.MultiSwipeRefreshLayout;
import com.server.android.model.DeviceItem;
import com.server.android.model.MapItem;
import com.server.android.request.station.StationOwnedRequest;
import com.server.android.transformer.station.StationCollectionTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class FarmMapDialog extends BaseDialog implements View.OnClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLoadedCallback,
        LocationListener {

    public static final String TAG = FarmMapDialog.class.getName().toString();
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int PERMISSION_LOCATION = 8445;

    private GoogleMap googleMap;
    private List<Marker> markers;
    private List<Polyline> polyLines;
    private List<MapItem> mapItems;
    private Polygon polygon;
    private SupportMapFragment supportMapFragment;
    private Callback callback;
    private StationOwnedRequest stationOwnedRequest;
    private Marker marker;
    private boolean isInfoWindowShown = false;

    private LatLng initialPosition;

    private boolean polygonCreated = false;

    @BindView(R.id.continueBTN)         TextView continueBTN;
    @BindView(R.id.reDrawBTN)           View reDrawBTN;
    @BindView(R.id.searchBTN)           View searchBTN;
    @BindView(R.id.mainIconIV)          View mainIconIV;
    @BindView(R.id.howIV)               ImageView howIV;
    @BindView(R.id.hintTXT)             View hintTXT;
    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;

    public static FarmMapDialog newInstance(List<MapItem> mapItems, Callback callback) {
        FarmMapDialog farmMapDialog = new FarmMapDialog();
        farmMapDialog.callback = callback;
        farmMapDialog.mapItems = mapItems;
        return farmMapDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_add_farm;
    }

    @Override
    public void onViewReady() {
        continueBTN.setOnClickListener(this);
        reDrawBTN.setOnClickListener(this);
        searchBTN.setOnClickListener(this);
        mainIconIV.setOnClickListener(this);
        hintTXT.setOnClickListener(this);
        howIV.setOnClickListener(this);

        mainTitleTXT.setText("Farm Map");

        Glide.with(getContext())
                .load(R.raw.map_how)
                .into(howIV);

        initialPosition = new LatLng(14.59951, 120.984222);
        setupStations();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (supportMapFragment == null) {
            supportMapFragment = new SupportMapFragment();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.farmMapFL, supportMapFragment).commit();
            supportMapFragment.getMapAsync(this);
        }
        setupStations();
    }

    @Override
    public void onPause() {
        super.onPause();
        getChildFragmentManager().beginTransaction()
                .remove(supportMapFragment)
                .commit();
        supportMapFragment = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        setDialogMatchParent();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBTN:
                continueClicked();
                break;
            case R.id.mainIconIV:
                dismiss();
                break;
            case R.id.searchBTN:
                searchAddress();
                break;
            case R.id.reDrawBTN:
                redraw();
                break;
            case R.id.howIV:
            case R.id.hintTXT:
                showHint();
                break;
        }
    }

    private void showHint(){
        howIV.setVisibility(howIV.isShown() ? View.GONE : View.VISIBLE);
    }

    private void redraw(){
        googleMap.clear();
        markers.clear();
        mapItems.clear();
        polygonCreated = false;
        setupStationsCont();
    }

    private void continueClicked(){
        mapItems = new ArrayList<>();
        LatLngBounds.Builder latLngBounds = new  LatLngBounds.Builder();

        for(Marker marker : markers){
            MapItem mapItem = new MapItem();
            mapItem.geo_lat = marker.getPosition().latitude;
            mapItem.geo_long = marker.getPosition().longitude;
            mapItems.add(mapItem);
            latLngBounds.include(marker.getPosition());
        }

        if(polygonCreated && callback != null){
            LatLngBounds bounds = latLngBounds.build();
            LatLng center = bounds.getCenter();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 2));
            callback.onSelected(mapItems, center.latitude + "," + center.longitude);
            dismiss();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (polygonCreated == false) {
            addMarker(latLng);
            drawLine();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        if(!marker.getTitle().equalsIgnoreCase("-1")){
            if (markers.get(0).getPosition().equals(marker.getPosition())) {
                createPolygon();
                enableContinueButton(true);
            }
//        }
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        if(polygonCreated){
            createPolygon();
            enableContinueButton(true);
        }else{
            drawLine();
        }
    }

    private void enableContinueButton(boolean enable){
        continueBTN.setVisibility(enable ? View.VISIBLE : View.GONE);
//        continueBTN.setEnabled(enable);
//        continueBTN.setTextColor(ActivityCompat.getColor(getContext(), enable ? R.color.text_gray : R.color.white_dark_gray));
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        setupStations();
        markers = new ArrayList<>();
        polyLines = new ArrayList<>();

        this.googleMap = googleMap;
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(initialPosition, 16));
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        this.googleMap.setOnMapClickListener(this);
        this.googleMap.setOnMarkerClickListener(this);
        this.googleMap.setOnMarkerDragListener(this);

        if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_LOCATION)){
            this.googleMap.setMyLocationEnabled(true);
        }

        this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return new View(getContext());
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                initialPosition = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            }
        }
    }

    private void addMarker(LatLng latLng){
        BitmapDrawable bitmapDrawable = (BitmapDrawable) ActivityCompat.getDrawable(getContext(), R.drawable.white_dot);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 50, 50, false);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .anchor(0.5f, 0.5f);

        Marker marker = googleMap.addMarker(markerOptions);
        markers.add(marker);
    }

    public void addMarker(DeviceItem deviceItem) {
        if(googleMap != null){
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(deviceItem.location.data.getLatLng());
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon));
            markerOptions.title("-1");
            googleMap.addMarker(markerOptions);
        }
    }

    private void drawLine() {
        if (markers.size() > 1) {
            clearPolylines();
            for (int i = 1; i < markers.size(); i++) {
                Polyline line = googleMap.addPolyline(new PolylineOptions()
                        .add(markers.get(i - 1).getPosition(), markers.get(i).getPosition())
                        .width(5)
                        .color(Color.WHITE));
                polyLines.add(line);
            }

        }
    }

    private void clearPolylines() {
        for (Polyline polyline : polyLines) {
            polyline.remove();
        }
        polyLines.clear();
    }

    public void createPolygon() {

        clearPolylines();

        if(polygon != null){
            polygon.remove();
        }

        if (markers.size() >= 3) {
            polygonCreated = true;
            PolygonOptions polygonOptions = new PolygonOptions();

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            for(Marker marker: markers){
                polygonOptions.add(marker.getPosition());
                builder.include(marker.getPosition());
            }

            LatLngBounds bounds = builder.build();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));

            polygonOptions.strokeColor(Color.WHITE);
            polygonOptions.strokeWidth(7);
            polygonOptions.fillColor(ActivityCompat.getColor(getContext(), R.color.primaryTransparent));
            polygon = googleMap.addPolygon(polygonOptions);
            enableContinueButton(true);
        }
    }

    private void setUpFarmMapPoints(){
        for(int i = 0 ; i < mapItems.size(); i++){
            addMarker(new LatLng(mapItems.get(i).geo_lat, mapItems.get(i).geo_long));
            if(i == mapItems.size() - 1){
                createPolygon();
            }
        }
    }

    private void searchAddress(){
        try {
            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(Place.TYPE_COUNTRY)
                    .setCountry("PH")
                    .build();

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(autocompleteFilter)
                            .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }



    private void setupStations(){
        stationOwnedRequest = new StationOwnedRequest(getContext());
        stationOwnedRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "current_weather,location")
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading Stations...", false, false))
                .execute();
    }

    private void setupStationsCont(){
        stationOwnedRequest = new StationOwnedRequest(getContext());
        stationOwnedRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "current_weather,location")
                .execute();
    }

    @Subscribe
    public void onResponse(StationOwnedRequest.ServerResponse responseData) {
        StationCollectionTransformer farmCollectionTransformer = responseData.getData(StationCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for(DeviceItem deviceItem : farmCollectionTransformer.data){
                addMarker(deviceItem);
                builder.include(deviceItem.location.data.getLatLng());
            }
            int width = getActivity().getResources().getDisplayMetrics().widthPixels;
            int height = getActivity().getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10);
//            builder.build();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapLoaded() {
        setUpFarmMapPoints();
        setupStations();
    }

    public interface Callback{
        void onSelected(List<MapItem> mapItems, String center);
    }
}
