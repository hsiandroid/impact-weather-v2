package com.highlysucceed.impact_weather.view.dialog;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import icepick.State;

public class DatePickerDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = DatePickerDialog.class.getName().toString();

	@BindView(R.id.simpleDatePicker) 		DatePicker simpleDatePicker;
	@BindView(R.id.submitBTN)				View submitBTN;

	@State String startDate;

	public static DatePickerDialog newInstance(String startDate) {
		DatePickerDialog datePickerDialog = new DatePickerDialog();
		datePickerDialog.startDate = startDate;
		return datePickerDialog;
	}

	public static DatePickerDialog newInstance() {
		DatePickerDialog fragment = new DatePickerDialog();
		fragment.startDate = "";
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_date_picker;
	}

	@Override
	public void onViewReady() {
		submitBTN.setOnClickListener(this);
		try {
			simpleDatePicker.setMinDate(new SimpleDateFormat("MMMM dd, yyyy").parse(startDate).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.submitBTN:
				String day = "" + simpleDatePicker.getDayOfMonth();
				String month =  "" + String.valueOf(simpleDatePicker.getMonth() + 1);
				String year = "" + simpleDatePicker.getYear();

				String dateString = month + "/" + day + "/" + year;
				Log.e("Date String >>", dateString);

				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = null;
				try {
					date = dateFormat.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				SimpleDateFormat dateFormatYouWant = new SimpleDateFormat("MMMM dd, yyyy");
				String finalDate = dateFormatYouWant.format(date);
				Log.e("Date >>",finalDate) ;

				if (clickListener != null){
					clickListener.onclick(finalDate);
				}
				dismiss();
			break;
		}
	}

	public ClickListener clickListener;

	public DatePickerDialog setClickListener(ClickListener clickListener){
		this.clickListener = clickListener;
		return this;
	}

	public interface ClickListener{
		void onclick(String date);
	}

}
