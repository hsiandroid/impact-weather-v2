package com.highlysucceed.impact_weather.view.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.server.android.model.FarmItem;
import com.server.android.request.farm.FarmAllRequest;
import com.server.android.transformer.farm.FarmCollectionTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.FarmAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class FarmFragment extends BaseFragment implements FarmAdapter.ClickListener,View.OnClickListener,SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = FarmFragment.class.getName().toString();

    private MainActivity       mainActivity;

    private FarmAllRequest farmAllRequest;
    private FarmAdapter farmAdapter;

    @BindView(R.id.myFarmLV)            ExpandableHeightListView myFarmLV;
    @BindView(R.id.addFarmBTN)          View addFarmBTN;
    @BindView(R.id.farmSRL)             SwipeRefreshLayout farmSRL;
    @BindView(R.id.farmPlaceholderIV)   View farmPlaceholderIV;

    public static FarmFragment newInstance() {
        FarmFragment farmFragment = new FarmFragment();
        return farmFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_farm;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_my_farm));
        addFarmBTN.setOnClickListener(this);
        mainActivity.hideFarmBTN();
        setUpFarmListView();
        attemptAll();

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptAll(){
        farmSRL.setColorSchemeResources(R.color.colorPrimary);
        farmSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        farmSRL.setOnRefreshListener(this);
        farmAllRequest = new FarmAllRequest(getContext());
        farmAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setPerPage(10000)
                .setSwipeRefreshLayout(farmSRL)
//                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops");
                .addParameters(Variable.server.key.INCLUDE, "info, crops");
    }

    private void refreshList(){
        myFarmLV.setVisibility(View.GONE);
        farmPlaceholderIV.setVisibility(View.GONE);
        addFarmBTN.setVisibility(View.GONE);
        farmAllRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FarmAllRequest.ServerResponse responseData) {
        addFarmBTN.setVisibility(View.VISIBLE);
        myFarmLV.setVisibility(View.VISIBLE);
        FarmCollectionTransformer farmCollectionTransformer = responseData.getData(FarmCollectionTransformer.class);
        if(farmCollectionTransformer.status){
            if(responseData.isNext()){
                farmAdapter.addNewData(farmCollectionTransformer.data);
            }else{
                farmAdapter.setNewData(farmCollectionTransformer.data);

            }
        }
        if (farmAdapter.getCount() == 0){
            farmPlaceholderIV.setVisibility(View.VISIBLE);
        }else {
            farmPlaceholderIV.setVisibility(View.GONE);
        }

    }

    private void setUpFarmListView() {
        farmAdapter = new FarmAdapter(getContext());
        myFarmLV.setAdapter(farmAdapter);
        myFarmLV.setExpanded(true);
        farmAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addFarmBTN:
                mainActivity.startMyFarmActivity(0, "create");
                break;
        }
    }

    @Override
    public void onItemClick(FarmItem farmItem) {
        mainActivity.startMyFarmActivity(farmItem.id, "details");
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
