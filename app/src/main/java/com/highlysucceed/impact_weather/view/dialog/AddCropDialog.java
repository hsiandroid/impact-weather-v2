package com.highlysucceed.impact_weather.view.dialog;

import android.app.ProgressDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.adapter.CropsAdapter;
import com.server.android.model.CropItem;
import com.server.android.model.CropSelectedItem;
import com.server.android.request.crops.CropsAllRequest;
import com.server.android.request.farm.FarmCropsEditRequest;
import com.server.android.transformer.crop.CropsCollectionTransformer;
import com.server.android.transformer.farm.FarmSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class AddCropDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = AddCropDialog.class.getName().toString();

	private CropsAdapter 	cropsAdapter;
	private List<CropItem> 	selectedCropsItem;
	private Callback callback;

	private CropsAllRequest cropsAllRequest;

	@State int farmId;

	@BindView(R.id.saveBTN)			TextView saveBTN;
	@BindView(R.id.cancelBTN)		TextView cancelBTN;
	@BindView(R.id.addBTN)			TextView addBTN;
	@BindView(R.id.cropsEHLV)       ExpandableHeightListView cropsEHLV;
	@BindView(R.id.scrollView)      ScrollView scrollView;

	public static AddCropDialog newInstance(int farmId, List<CropItem> selectedCropsItem, Callback callback) {
		AddCropDialog addCropDialog = new AddCropDialog();
		addCropDialog.farmId = farmId;
		addCropDialog.selectedCropsItem = selectedCropsItem;
		addCropDialog.callback = callback;
		return addCropDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_add_crop;
	}

	@Override
	public void onViewReady() {
		saveBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		addBTN.setOnClickListener(this);

		setUpSPR();
		attemptCropsAll();
	}

	public List<CropItem> getSelectedCropsItem() {
		if(selectedCropsItem == null){
			selectedCropsItem = new ArrayList<>();
		}
		return selectedCropsItem;
	}

	private void setUpSPR(){
		cropsAdapter =  new CropsAdapter(getContext());
		CropSelectedItem cropSelectedItem;
		for(CropItem cropItem : getSelectedCropsItem()){
			cropSelectedItem = new CropSelectedItem();
			cropSelectedItem.name = cropItem.name;
			cropSelectedItem.variety = cropItem.variety;
			cropsAdapter.addCrop(cropSelectedItem);
		}
		cropsEHLV.setItemsCanFocus(true);
		cropsEHLV.setAdapter(cropsAdapter);
	}

	private void attemptCropsAll(){
		cropsAllRequest = new CropsAllRequest(getContext());
		cropsAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Loading...", false, false))
				.execute();
	}

	private void attemptEdit(){
		FarmCropsEditRequest farmCropsEditRequest = new FarmCropsEditRequest(getContext());
		farmCropsEditRequest
				.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating farm details...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Variable.server.key.FARM_ID, farmId);

		if(cropsAdapter.getCount() != 0){
            List<String> selected = cropsAdapter.getSelected();
			for(String string : selected){
				farmCropsEditRequest.addParameters("crops[" + selected.indexOf(string) + "]", string);
			}
		}
		farmCropsEditRequest.execute();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(FarmCropsEditRequest.ServerResponse responseData) {
		FarmSingleTransformer farmSingleTransformer = responseData.getData(FarmSingleTransformer.class);
		if(farmSingleTransformer.status){
			ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.SUCCESS);
			if(callback != null){
				callback.onDone();
			}
			dismiss();
		}else{
			ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.FAILED);
			if(farmSingleTransformer.hasRequirements()){
			}
		}
	}

	@Subscribe
	public void onResponse(CropsAllRequest.ServerResponse responseData) {
		CropsCollectionTransformer cropsCollectionTransformer = responseData.getData(FarmSingleTransformer.class);
		if(cropsCollectionTransformer.status){
			cropsAdapter.setCropsItems(cropsCollectionTransformer.data);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.saveBTN:
				attemptEdit();
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
			case R.id.addBTN:
                addCrop();
				break;
		}
	}

	private void addCrop(){
        cropsAdapter.addCrop();
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

	private void refreshList(){
		cropsAllRequest.execute();
	}

	public interface Callback {
		void onDone();
	}
}
