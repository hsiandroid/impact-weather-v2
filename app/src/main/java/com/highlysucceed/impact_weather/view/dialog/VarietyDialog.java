package com.highlysucceed.impact_weather.view.dialog;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.view.adapter.VarietySelectorRecyclerViewAdapter;

import java.util.List;

import butterknife.BindView;

public class VarietyDialog extends BaseDialog implements
        VarietySelectorRecyclerViewAdapter.ClickListener,
        View.OnClickListener{
	public static final String TAG = VarietyDialog.class.getName().toString();

	private VarietyCallback varietyCallback;
	private List<String> varietyItems;
	private Object target;
	private LinearLayoutManager linearLayoutManager;
	private VarietySelectorRecyclerViewAdapter cropsSelectorRecyclerViewAdapter;

	@BindView(R.id.mainIconIV)          View mainIconIV;
	@BindView(R.id.varietyRV)           RecyclerView varietyRV;

	public static VarietyDialog newInstance(List<String> varietyItems, Object target, VarietyCallback varietyCallback) {
		VarietyDialog cropDialog = new VarietyDialog();
		cropDialog.varietyItems = varietyItems;
		cropDialog.target = target;
		cropDialog.varietyCallback = varietyCallback;
		return cropDialog;
	}

	public List<String> getVarietyItems() {
		return varietyItems;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_variety;
	}

	@Override
	public void onViewReady() {
        mainIconIV.setOnClickListener(this);
        setupListView();
	}

	private void setupListView(){
        cropsSelectorRecyclerViewAdapter = new VarietySelectorRecyclerViewAdapter(getContext());
        cropsSelectorRecyclerViewAdapter.setOnItemClickListener(this);
        cropsSelectorRecyclerViewAdapter.setNewData(getVarietyItems());
        linearLayoutManager = new LinearLayoutManager(getContext());
        varietyRV.setAdapter(cropsSelectorRecyclerViewAdapter);
        varietyRV.setLayoutManager(linearLayoutManager);
    }

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                dismiss();
                break;
        }
    }

	@Override
	public void onItemClick(String item) {
		if(varietyCallback != null){
			varietyCallback.varietyCallback(item, target);
		}
		dismiss();
	}

	public interface VarietyCallback{
		void varietyCallback(String variety, Object target);
	}
}
