package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import com.highlysucceed.impact_weather.R;

import java.util.List;

import butterknife.ButterKnife;

public class TemplateAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<String> data;
    private LayoutInflater layoutInflater;

	public TemplateAdapter(Context context, List<String> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_template, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.string = data.get(position);
        holder.view.setOnClickListener(this);

		return convertView;
	}




    public class ViewHolder{
        String string;
        int s;

//        @BindView(R.id.emotionTXT) TextView emotionTXT;
//        @BindView(R.id.genderTXT) TextView genderTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        TemplateAdapter.ViewHolder viewHolder = (TemplateAdapter.ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(data.indexOf(viewHolder.s), v);
        }
    }


    private TemplateAdapter.ClickListener clickListener;
    public void setOnItemClickListener(TemplateAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
