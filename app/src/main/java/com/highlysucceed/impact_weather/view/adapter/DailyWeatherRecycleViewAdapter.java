package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.WeatherItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyWeatherRecycleViewAdapter extends RecyclerView.Adapter<DailyWeatherRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener {

	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;

	public DailyWeatherRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_daily_weather, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.weatherItem = data.get(position);

        holder.view.setOnClickListener(this);
        holder.view.setTag(position);

        int index = 0;
        int select = 2;

        if(select > holder.weatherItem.hourly_weather.size() - 1){
            index = holder.weatherItem.hourly_weather.size() - 1;
        }else{
            index = select;
        }

        holder.dayTXT.setText(holder.weatherItem.hourly_weather.get(index).day);
        holder.highestTempTXT.setText(holder.weatherItem.hourly_weather.get(index).max_temp);
        holder.lowestTempTXT.setText(holder.weatherItem.hourly_weather.get(index).min_temp);
        holder.statusTXT.setText("(" + holder.weatherItem.hourly_weather.get(index).status + ")");
        holder.windSpeedTXT.setText(holder.weatherItem.hourly_weather.get(index).wind_speed);
        holder.dailyTemperatureTXT.setText(holder.weatherItem.hourly_weather.get(index).temperature);

        Picasso.with(context)
                .load(holder.weatherItem.hourly_weather.get(index).image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.dailyWeatherIV);

        if(holder.weatherItem.isVisible){
            holder.hourlyWeatherRecycleViewAdapter = new HourlyWeatherRecycleViewAdapter(context);
            holder.hourlyWeatherRecycleViewAdapter.setNewData(holder.weatherItem.hourly_weather);
            holder.hourlyRV.setLayoutManager(holder.getLinearLayoutManager());
            holder.hourlyRV.setAdapter(holder.hourlyWeatherRecycleViewAdapter);
            holder.hourlyRV.setVisibility(View.VISIBLE);
        }else{
            holder.hourlyRV.setVisibility(View.GONE);
        }

        holder.weeklyCON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherItem weatherItem = data.get(position);
                weatherItem.isVisible = !weatherItem.isVisible;
                data.set(position, weatherItem);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        WeatherItem weatherItem;
        HourlyWeatherRecycleViewAdapter hourlyWeatherRecycleViewAdapter;

        @BindView(R.id.dayTXT)                  TextView dayTXT;
        @BindView(R.id.dailyWeatherIV)          ImageView dailyWeatherIV;
        @BindView(R.id.dailyTemperatureTXT)     TextView dailyTemperatureTXT;
        @BindView(R.id.statusTXT)               TextView statusTXT;
        @BindView(R.id.highestTempTXT)          TextView highestTempTXT;
        @BindView(R.id.lowestTempTXT)           TextView lowestTempTXT;
        @BindView(R.id.windSpeedTXT)            TextView windSpeedTXT;
        @BindView(R.id.weeklyCON)               View weeklyCON;
        @BindView(R.id.hourlyRV)                RecyclerView hourlyRV;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public LinearLayoutManager getLinearLayoutManager(){
            return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onDailyItemClick(data.get(((int) v.getTag())));
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onDailyItemClick(WeatherItem weatherItem);
    }
} 
