package com.highlysucceed.impact_weather.view.dialog;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;

public class TemplateDialog extends BaseDialog {
	public static final String TAG = TemplateDialog.class.getName().toString();

	public static TemplateDialog newInstance() {
		TemplateDialog templateDialog = new TemplateDialog();
		return templateDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_template;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
