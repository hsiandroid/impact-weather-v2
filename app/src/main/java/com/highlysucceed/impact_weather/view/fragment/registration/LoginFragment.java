package com.highlysucceed.impact_weather.view.fragment.registration;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.CountryData;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;
import com.highlysucceed.impact_weather.view.dialog.CountryDialog;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;
import com.server.android.request.GetCountryRequest;
import com.server.android.request.auth.LoginRequest;
import com.server.android.request.auth.ResendCodeRequest;
import com.server.android.transformer.GetCountryTransformer;
import com.server.android.transformer.user.LoginTransformer;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class LoginFragment extends BaseFragment implements View.OnClickListener,  CountryDialog.CountryPickerListener{

    public static final String TAG = LoginFragment.class.getName().toString();
    public RegistrationActivity registrationActivity;

    private static final String CONTACT_FORMAT = "[000] [000] [0000]";
    @BindView(R.id.contactET)                     EditText contactET;
    @BindView(R.id.passwordET)                     EditText passwordET;
    @BindView(R.id.signUpTV)                      TextView signUpTV;
    @BindView(R.id.loginTV)                       TextView loginTV;
    @BindView(R.id.forgotPasswordBTN)             TextView forgotPassBTN;
    @BindView(R.id.countryFlagIV)                 ImageView countryFlagIV;
    @BindView(R.id.contactCodeTXT)                TextView contactCodeTXT;
    @BindView(R.id.contactCON)                    View contactCON;

    @State  String contact;
    @State String country;

    private CountryData.Country countryData;


    public static LoginFragment newInstance(){
        LoginFragment loginFragment = new LoginFragment();
        return loginFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.showTitleBar(false);
        signUpTV.setOnClickListener(this);
        loginTV.setOnClickListener(this);
        forgotPassBTN.setOnClickListener(this);
        onMaskedTextChangedListener(contactET);
        contactCON.setOnClickListener(this);
        getCountry();
    }

    public void onMaskedTextChangedListener(EditText editText){

        MaskedTextChangedListener maskedTextChangedListener = new MaskedTextChangedListener(
                CONTACT_FORMAT,
                true,
                editText,
                null,
                null);

        editText.addTextChangedListener(maskedTextChangedListener);
        editText.setOnFocusChangeListener(maskedTextChangedListener);
        editText.setText(" ");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpTV:
                registrationActivity.openSignUpFragment();
                break;
            case R.id.forgotPasswordBTN:
                registrationActivity.openForgotPasswordFragment();
                break;
            case R.id.loginTV:
                attemptLogin();
                break;
            case R.id.contactCON:
                showCountryPicker();
                break;

        }
    }

    private void attemptLogin(){
        contact = contactET.getText().toString();
        Mask.Result result = new Mask(CONTACT_FORMAT).apply(new CaretString(contact,contact.length()),true);
        LoginRequest loginRequest = new LoginRequest(getContext());
        loginRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging in...", false, false))
                .addParameters(Variable.server.key.CONTACT, contactCodeTXT.getText().toString() + result.getExtractedValue())
                .addParameters(Variable.server.key.CLIENT_ID, Variable.server.client.ID)
                .addParameters(Variable.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Variable.server.key.CLIENT_SECRET, Variable.server.client.SECRET)
                .execute();
    }

    @Subscribe
    public void onResponse(GetCountryRequest.ServerResponse responseData) {
        GetCountryTransformer getCountryTransformer = responseData.getData(GetCountryTransformer.class);
        if(getCountryTransformer.status){
            countryData = CountryData.getCountryDataByCode1(getCountryTransformer.data.country);
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);
        }else{
            ToastMessage.show(getContext(), getCountryTransformer.msg, ToastMessage.Status.FAILED);

        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LoginRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            UserData.insert(UserData.AUTHORIZATION, userSingleTransformer.token.access_token);
            UserData.insert(UserData.REFRESH_TOKEN, userSingleTransformer.token.refresh_token);
            UserData.insert(UserData.DEFAULT_FARM_ID, userSingleTransformer.data.defaultFarmId);
            UserData.insert(UserData.TOKEN_EXPIRED, false);
            UserData.insert(userSingleTransformer.data);
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.startMainActivity();
        }else{
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }


    private void getCountry(){
        GetCountryRequest createCommentRequest = new GetCountryRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .execute();
    }



    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), TAG);
    }


    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }
}
