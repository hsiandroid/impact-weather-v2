package com.highlysucceed.impact_weather.view.fragment.registration;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;
import com.server.android.request.auth.LoginRequest;
import com.server.android.request.auth.ResendCodeRequest;
import com.server.android.transformer.user.LoginTransformer;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class MobileConfirmationFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = MobileConfirmationFragment.class.getName().toString();
    private RegistrationActivity registrationActivity;

    private static final String CONTACT_FORMAT = "(+63) [000] [000] [0000]";

    @BindView(R.id.mobileNumberTXT)         TextView mobileNumberTXT;
    @BindView(R.id.numberCode1ET)           EditText numberCode1ET;
    @BindView(R.id.numberCode2ET)           EditText numberCode2ET;
    @BindView(R.id.numberCode3ET)           EditText numberCode3ET;
    @BindView(R.id.numberCode4ET)           EditText numberCode4ET;
    @BindView(R.id.continueBTN)             View continueBTN;
    @BindView(R.id.resendBTN)               View resendBTN;

    @State String contact;

    public static MobileConfirmationFragment newInstance(String contact) {
        MobileConfirmationFragment mobileConfirmationFragment = new MobileConfirmationFragment();
        mobileConfirmationFragment.contact = contact;
        return mobileConfirmationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_number_confirmation;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.setTitle("Activate Account");
        registrationActivity.showTitleBar(true);

        continueBTN.setOnClickListener(this);
        resendBTN.setOnClickListener(this);
        mobileNumberTXT.setText(contact);
        editTextSetup();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    private void attemptLogin() {
        if (numberCode1ET.getText().toString().isEmpty() ||
            numberCode2ET.getText().toString().isEmpty() ||
            numberCode3ET.getText().toString().isEmpty() ||
            numberCode4ET.getText().toString().isEmpty()){
            if (numberCode1ET.getText().toString().equals("")){
                numberCode1ET.setError("Field is required!");
            }
            if (numberCode2ET.getText().toString().equals("")){
                numberCode2ET.setError("Field is required!");
            }
            if (numberCode3ET.getText().toString().equals("")){
                numberCode3ET.setError("Field is required!");
            }
            if (numberCode4ET.getText().toString().equals("")){
                numberCode4ET.setError("Field is required!");
            }
        }

        final Mask mask = new Mask(CONTACT_FORMAT);
        final Mask.Result result = mask.apply(
                new CaretString(
                        contact,
                        contact.length()
                ),
                true
        );
        final String contact1 = "+63" + result.getExtractedValue();
        LoginRequest loginRequest = new LoginRequest(getContext());
        loginRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Validating code...", false, false))
                .addParameters(Variable.server.key.CONTACT, contact1)
                .addParameters(Variable.server.key.CODE, getActivationCode())
                .addParameters(Variable.server.key.CLIENT_ID, Variable.server.client.ID)
                .addParameters(Variable.server.key.CLIENT_SECRET, Variable.server.client.SECRET)
                .execute();
    }

    private void attemptResend(){
        Mask.Result result = new Mask(CONTACT_FORMAT).apply(new CaretString(contact,contact.length()),true);
        ResendCodeRequest resendCodeRequest = new ResendCodeRequest(getContext());
        resendCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Sending...", false, false))
                .addParameters(Variable.server.key.CONTACT, "+63" + result.getExtractedValue())
                .addParameters(Variable.server.key.SEND_SMS, registrationActivity.activeSMS ? "yes" : "no")
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        numberCode1ET.requestFocus();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(LoginRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            UserData.insert(UserData.AUTHORIZATION, userSingleTransformer.token.access_token);
            UserData.insert(UserData.REFRESH_TOKEN, userSingleTransformer.token.refresh_token);
            UserData.insert(UserData.TOKEN_EXPIRED, false);
            UserData.insert(userSingleTransformer.data);
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            registrationActivity.startMainActivity();
        }else{
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(ResendCodeRequest.ServerResponse responseData) {
        LoginTransformer loginTransformer = responseData.getData(LoginTransformer.class);
        if(loginTransformer.status){
            ToastMessage.show(getActivity(), loginTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), loginTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void editTextSetup(){
        numberCode1ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after){

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
                if(numberCode1ET.getText().length() == 1){
                    numberCode1ET.clearFocus();
                    numberCode2ET.requestFocus();
                    numberCode2ET.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        numberCode2ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(numberCode2ET.getText().length() == 1){
                    numberCode2ET.clearFocus();
                    numberCode3ET.requestFocus();
                    numberCode3ET.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(numberCode2ET.getText().length() == 0){
                    numberCode2ET.clearFocus();
                    numberCode1ET.requestFocus();
                    numberCode1ET.setCursorVisible(true);
                    numberCode1ET.setSelection(numberCode1ET.getText().length());
                }
            }
        });

        numberCode3ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(numberCode3ET.getText().length() == 1){
                    numberCode3ET.clearFocus();
                    numberCode4ET.requestFocus();
                    numberCode4ET.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(numberCode3ET.getText().length() == 0){
                    numberCode3ET.clearFocus();
                    numberCode2ET.requestFocus();
                    numberCode2ET.setCursorVisible(true);
                    numberCode2ET.setSelection(numberCode2ET.getText().length());
                }
            }
        });

        numberCode4ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                if(numberCode4ET.getText().length() == 1){
                    numberCode4ET.clearFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(numberCode4ET.getText().length() == 0){
                    numberCode4ET.clearFocus();
                    numberCode3ET.requestFocus();
                    numberCode3ET.setCursorVisible(true);
                    numberCode3ET.setSelection(numberCode3ET.getText().length());
                }else{
                    attemptLogin();
                }
            }
        });

    }

    public String getActivationCode(){
        String code = "";

        if(TextUtils.isEmpty(numberCode1ET.getText().toString())){
            code += "0";
        }else{
            code += numberCode1ET.getText().toString();
        }

        if(TextUtils.isEmpty(numberCode2ET.getText().toString())){
            code += "0";
        }else{
            code += numberCode2ET.getText().toString();
        }

        if(TextUtils.isEmpty(numberCode3ET.getText().toString())){
            code += "0";
        }else{
            code += numberCode3ET.getText().toString();
        }

        if(TextUtils.isEmpty(numberCode4ET.getText().toString())){
            code += "0";
        }else{
            code += numberCode4ET.getText().toString();
        }

        return code;
    }

    public void clearEditText(){
        numberCode1ET.setText("");
        numberCode2ET.setText("");
        numberCode3ET.setText("");
        numberCode4ET.setText("");

        numberCode1ET.requestFocus();
        numberCode1ET.setCursorVisible(true);
        numberCode1ET.setSelection(numberCode1ET.getText().length());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.continueBTN:
                attemptLogin();
                break;
            case R.id.resendBTN:
                attemptResend();
                break;
        }
    }
}
