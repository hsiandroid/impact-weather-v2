package com.highlysucceed.impact_weather.view.dialog;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.server.android.model.UserItem;
import com.server.android.request.auth.LogoutRequest;
import com.server.android.util.BaseTransformer;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.PseudoLoading;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.view.activity.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class LogoutDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = LogoutDialog.class.getName().toString();
	private PseudoLoading pseudoLoading;

	@BindView(R.id.continueTV) TextView continueTV;
	@BindView(R.id.cancelTV) TextView cancelTV;

	public static LogoutDialog newInstance() {
		LogoutDialog logoutDialog = new LogoutDialog();
		return logoutDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_logout;
	}

	@Override
	public void onViewReady() {
		continueTV.setOnClickListener(this);
		cancelTV.setOnClickListener(this);
		pseudoLoading = PseudoLoading.newInstance(getContext()).setTitle("Signing out...").setCallback(new PseudoLoading.Callback() {
			@Override
			public void finish() {
				((MainActivity) getContext()).startRegistrationActivity();
				((MainActivity) getContext()).finish();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		EventBus.getDefault().register(this);
		Window window = getDialog().getWindow();

		window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		window.setBackgroundDrawable(new ColorDrawable(0));
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.continueTV:
				attemptLogout();
				break;
			case R.id.cancelTV:
				dismiss();
				break;
		}
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	private void attemptLogout(){
		LogoutRequest refreshTokenRequest = new LogoutRequest(getContext());
		refreshTokenRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging out...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.execute();
	}

	@Subscribe
	public void onResponse(LogoutRequest.ServerResponse responseData) {
		BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);

		if(baseTransformer.status){
			UserData.insert(UserData.AUTHORIZATION, "");
			UserData.insert(UserData.TOKEN_EXPIRED, true);
			UserData.insert(new UserItem());
			dismiss();
			((RouteActivity) getContext()).startRegistrationActivity();
			((RouteActivity) getContext()).finish();

			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
		}else{
//			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		pseudoLoading.onPause();
	}


}
