package com.highlysucceed.impact_weather.view.fragment.main;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.server.android.model.SettingsItem;
import com.server.android.model.UserItem;
import com.server.android.request.profile.ProfileAvatarRequest;
import com.server.android.request.profile.ProfileEditFieldRequest;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.SettingsAdapter;
import com.highlysucceed.impact_weather.view.dialog.EditProfileDialog;
import com.highlysucceed.impact_weather.view.dialog.ImagePickerDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SettingsFragment extends BaseFragment implements View.OnClickListener, SettingsAdapter.OnEditClickListener,ImagePickerDialog.ImageCallback {

    public static final String TAG = SettingsFragment.class.getName().toString();

    private MainActivity mainActivity;
    private SettingsAdapter settingsAdapter;
    private List<SettingsItem> settingsItems;

    @BindView(R.id.settingsEHLV)        ExpandableHeightListView settingsEHLV;
    @BindView(R.id.avatarCIV)           ImageView avatarCIV;
    @BindView(R.id.uploadBTN)           TextView uploadBTN;
    @BindView(R.id.uploadCON)           View uploadCON;
    @BindView(R.id.aboutBTN)            View aboutBTN;
    @BindView(R.id.logoutBTN)           View logoutBTN;
    @BindView(R.id.versionTXT)          TextView versionTXT;
    @BindView(R.id.changePassBTN)       View changePass;

    public static SettingsFragment newInstance() {
        SettingsFragment settingsFragment = new SettingsFragment();
        return settingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_profile));
        mainActivity.hideFarmBTN();
        settingsItems = new ArrayList<>();
        settingsAdapter = new SettingsAdapter(getContext(), settingsItems);
        settingsEHLV.setAdapter(settingsAdapter);

        displayUserData(UserData.getUserItem());
        aboutBTN.setOnClickListener(this);
        uploadBTN.setOnClickListener(this);
        uploadCON.setOnClickListener(this);
        logoutBTN.setOnClickListener(this);
        changePass.setOnClickListener(this);
    }

    public void displayUserData(UserItem userItem) {
        settingsItems.clear();
        settingsItems.add(new SettingsItem(1, getString(R.string.settings_name),        userItem.name,     Variable.server.key.NAME));
        settingsItems.add(new SettingsItem(4, getString(R.string.settings_email),       userItem.email,    Variable.server.key.EMAIL));
        settingsItems.add(new SettingsItem(3, getString(R.string.sign_up_hint_contact), userItem.contact,   Variable.server.key.CONTACT));
        settingsAdapter.setNewData(settingsItems);
        settingsAdapter.setOnEditClickListener(this);

        Picasso.with(getContext())
                .load(userItem.image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(avatarCIV);

        versionTXT.setText(getAppVersion());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.aboutBTN:
                mainActivity.startSettingsActivity("about");
                break;
            case R.id.editBTN:
                avatarCIV.setEnabled(true);
                uploadBTN.setOnClickListener(this);
                break;
            case R.id.uploadCON:
                ImagePickerDialog.newInstance("Add Photo", false, this).show(getChildFragmentManager(),ImagePickerDialog.TAG);
                break;
            case R.id.logoutBTN:
                mainActivity.openLogoutDialogFragment();
                break;
            case R.id.changePassBTN:
                mainActivity.openChangePass();
                break;
        }
    }

    private void attemptAvatarUpdate(File file){
        ProfileAvatarRequest profileAvatarRequest = new ProfileAvatarRequest(getContext());
        profileAvatarRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating profile picture...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION));
        if(file != null){
            profileAvatarRequest.addPart(Variable.server.key.FILE, file);
        }
        profileAvatarRequest.execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ProfileAvatarRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            displayUserData(userSingleTransformer.data);
            UserData.insert(userSingleTransformer.data);
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(ProfileEditFieldRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            displayUserData(userSingleTransformer.data);
        }
    }

    @Override
    public void onEditClick(int position) {
        EditProfileDialog.newInstance(settingsItems.get(position)).show(getChildFragmentManager(), EditProfileDialog.TAG);
    }

    @Override
    public void result(File file) {
        attemptAvatarUpdate(file);
    }

    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version --";
        }

        return version;
    }

}
