package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.server.android.model.SettingsItem;
import com.highlysucceed.impact_weather.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsAdapter extends BaseAdapter {
	private Context context;
	private List<SettingsItem> data;
    private LayoutInflater layoutInflater;

	public SettingsAdapter(Context context, List<SettingsItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<SettingsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_settings, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.settingsItem = data.get(position);
        holder.titleTV.setText(holder.settingsItem.title);
        holder.contentTV.setText(holder.settingsItem.value);
        holder.editBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onEditClickListener != null){
                    onEditClickListener.onEditClick(position);
                }
            }
        });

		return convertView;
	}

	public class ViewHolder{
        SettingsItem settingsItem;
        View view;
        @BindView(R.id.titleTV) TextView titleTV;
        @BindView(R.id.contentTV) TextView contentTV;
        @BindView(R.id.editBTN) ImageView editBTN;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private OnEditClickListener onEditClickListener;

    public void setOnEditClickListener(OnEditClickListener onEditClickListener) {
        this.onEditClickListener = onEditClickListener;
    }

    public interface OnEditClickListener {
        void onEditClick(int position);
    }
} 
