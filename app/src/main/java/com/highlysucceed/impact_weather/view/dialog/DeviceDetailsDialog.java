package com.highlysucceed.impact_weather.view.dialog;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.util.widget.HorizontalListView;
import com.highlysucceed.impact_weather.view.adapter.HourlyWeatherAdapter;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;
import com.server.android.model.DeviceItem;
import com.server.android.request.station.StationSubscribeRequest;
import com.server.android.request.station.StationUnSubscribeRequest;
import com.server.android.transformer.station.StationSubscriptionSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class DeviceDetailsDialog extends BaseDialog implements View.OnClickListener {
    public static final String TAG = DeviceDetailsDialog.class.getName().toString();

    private HourlyWeatherAdapter hourlyWeatherAdapter;
    private DeviceItem deviceItem;

    @BindView(R.id.weatherIV)           ImageView weatherIV;
    @BindView(R.id.placeTXT)            TextView placeTXT;
    @BindView(R.id.degreesTXT)          TextView degreesTXT;
    @BindView(R.id.highestTempTXT)      TextView highestTempTXT;
    @BindView(R.id.lowestTempTXT)       TextView lowestTempTXT;
    @BindView(R.id.rainAmountTXT)       TextView rainAmountTXT;
    @BindView(R.id.speedTXT)            TextView speedTXT;
    @BindView(R.id.subscribeBTN)        TextView subscribeBTN;
    @BindView(R.id.cancelBTN)           TextView cancelBTN;
    @BindView(R.id.hourlyEHGV)          HorizontalListView hourlyEHGV;

    private Callback callback;

    public static DeviceDetailsDialog newInstance(DeviceItem deviceItem, Callback callback) {
        DeviceDetailsDialog deviceDetailsDialog = new DeviceDetailsDialog();
        deviceDetailsDialog.deviceItem = deviceItem;
        deviceDetailsDialog.callback = callback;
        return deviceDetailsDialog;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.dialog_device_details;
    }

    @Override
    public void onViewReady() {
        setUpHourlyWeatherListView();

        placeTXT.setText(deviceItem.name);
        degreesTXT.setText(deviceItem.current_weather.data.temperature);
        highestTempTXT.setText(deviceItem.current_weather.data.max_temp);
        lowestTempTXT.setText(deviceItem.current_weather.data.min_temp);
        rainAmountTXT.setText(deviceItem.current_weather.data.precipitation);
        speedTXT.setText(deviceItem.current_weather.data.wind_speed);

        subscribeBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);

        hourlyWeatherAdapter.setNewData(deviceItem.daily_weather.data);

        if(deviceItem.is_subscribed){
            subscribeBTN.setText("Unsubscribe");
        }else{
            subscribeBTN.setText("Subscribe");
        }
    }

    private void setUpHourlyWeatherListView() {
        hourlyWeatherAdapter = new HourlyWeatherAdapter(getContext());
        hourlyEHGV.setAdapter(hourlyWeatherAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.subscribeBTN:
                if(deviceItem.is_subscribed){
                   unsubscribe();
                }else{
                    attemptSubscribe();
                }
                break;
            case R.id.cancelBTN:
                dismiss();
                break;
        }
    }

    private void attemptSubscribe(){
        StationSubscribeRequest stationSubscribeRequest = new StationSubscribeRequest(getContext());
        stationSubscribeRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Subscribing to device...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info, station.current_weather, station.location, station.daily_weather")
                .addParameters(Variable.server.key.STATION_ID, deviceItem.id)
                .execute();
    }

    private void unsubscribe(){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog.setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptUnSubscribe();
                    }
                })
                .setDescription("Are you sure you want to Unsubscribe?")
                .setIcon(R.drawable.icon_information)
                .show(getChildFragmentManager(),confirmationDialog.TAG);
    }

    private void attemptUnSubscribe(){
        StationUnSubscribeRequest unSubscribeRequest = new StationUnSubscribeRequest(getContext());
        unSubscribeRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Unsubscribing...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info, station.current_weather, station.location, station.daily_weather")
                .addParameters(Variable.server.key.SUBSCRIPTION_ID, deviceItem.subscription_id)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(StationSubscribeRequest.ServerResponse responseData) {
        StationSubscriptionSingleTransformer stationSingleTransformer = responseData.getData(StationSubscriptionSingleTransformer.class);
        if(stationSingleTransformer.status){
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            if(callback != null){
                callback.subscribed(stationSingleTransformer.data.station.data, this);
            }else{
                Log.e("DeviceDetails", "callback is null");
            }
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(StationUnSubscribeRequest.ServerResponse responseData) {
        StationSubscriptionSingleTransformer stationSingleTransformer = responseData.getData(StationSubscriptionSingleTransformer.class);
        if(stationSingleTransformer.status){
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            if(callback != null){
                callback.unsubscribed(stationSingleTransformer.data.station.data, this);
            }else{
                Log.e("DeviceDetails", "callback is null");
            }
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public interface Callback {
        void subscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog);
        void unsubscribed(DeviceItem deviceItem, DeviceDetailsDialog dialog);
    }
}
