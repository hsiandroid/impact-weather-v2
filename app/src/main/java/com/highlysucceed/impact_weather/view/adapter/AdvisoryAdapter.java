package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.AdvisoryItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdvisoryAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<AdvisoryItem> data;
    private LayoutInflater layoutInflater;

	public AdvisoryAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<AdvisoryItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<AdvisoryItem> data){
        for(AdvisoryItem advisoryItem : data){
            this.data.add(advisoryItem);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_advisory, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);

        holder.advisoryItem = data.get(position);
        holder.placeTXT.setText(holder.advisoryItem.content);
        holder.timeTXT.setText(holder.advisoryItem.created_at.time_passed);

//        holder.criticalTXT.setText(holder.advisoryItem.status.data.critical);
//        holder.moderateTXT.setText(holder.advisoryItem.status.data.moderate);
//        holder.lowTXT.setText(holder.advisoryItem.status.data.low);

		return convertView;
	}

	public class ViewHolder{
        AdvisoryItem advisoryItem;

        @BindView(R.id.placeTXT)               TextView placeTXT;
        @BindView(R.id.timeTXT)               TextView timeTXT;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(viewHolder.advisoryItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(AdvisoryItem advisoryItem);
    }
} 
