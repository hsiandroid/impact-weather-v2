package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.DeviceItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<DeviceItem> data;
    private LayoutInflater layoutInflater;

	public DeviceAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<DeviceItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<DeviceItem> data){
        for(DeviceItem item : data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void addNewData(DeviceItem data){
        this.data.add(0, data);
        notifyDataSetChanged();
    }

    public void updateOrNewData(DeviceItem deviceItem){

        for(DeviceItem item : data){
            if(deviceItem.id == item.id){
                data.set(data.indexOf(item), deviceItem);
                notifyDataSetChanged();
                return;
            }
        }

        this.data.add(0, deviceItem);
        notifyDataSetChanged();
    }

    public void removeData(DeviceItem deviceItem){
        for(DeviceItem item : data){
            if(deviceItem.id == item.id){
                data.remove(data.indexOf(item));
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void updateData(DeviceItem deviceItem){
        for(DeviceItem item : data){
            if(deviceItem.id == item.id){
                data.set(data.indexOf(item), deviceItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_my_devices, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.deviceItem = data.get(position);
        holder.view.setOnClickListener(this);


        holder.placeTXT.setText(holder.deviceItem.name);
        holder.highestTempTXT.setText(holder.deviceItem.current_weather.data.max_temp);
        holder.statusTXT.setText(holder.deviceItem.current_weather.data.status);
        holder.lowestTempTXT.setText(holder.deviceItem.current_weather.data.min_temp);
        holder.rainAmountTXT.setText(holder.deviceItem.current_weather.data.probability_of_precip);
        holder.speedTXT.setText(holder.deviceItem.current_weather.data.wind_speed);
        holder.degreesTXT.setText(holder.deviceItem.current_weather.data.temperature);

        Picasso.with(context)
                .load(holder.deviceItem.current_weather.data.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.weatherIV);

        if(holder.deviceItem.is_subscribed){
            holder.backgroundCON.setAlpha(1);
        }else{
            holder.backgroundCON.setAlpha(0.7f);
        }

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(viewHolder.deviceItem);
        }
    }

    public class ViewHolder{

        DeviceItem deviceItem;

        @BindView(R.id.weatherIV)           ImageView weatherIV;
        @BindView(R.id.placeTXT)            TextView placeTXT;
        @BindView(R.id.degreesTXT)          TextView degreesTXT;
        @BindView(R.id.highestTempTXT)      TextView highestTempTXT;
        @BindView(R.id.lowestTempTXT)       TextView lowestTempTXT;
        @BindView(R.id.rainAmountTXT)       TextView rainAmountTXT;
        @BindView(R.id.speedTXT)            TextView speedTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.windTXT)             TextView windTXT;
        @BindView(R.id.humidityTXT)         TextView humidityTXT;
        @BindView(R.id.backgroundCON)       View backgroundCON;

        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(DeviceItem deviceItem);
    }
} 
