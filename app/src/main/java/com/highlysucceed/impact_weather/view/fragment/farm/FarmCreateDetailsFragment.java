package com.highlysucceed.impact_weather.view.fragment.farm;

import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.adapter.CropsAdapter;
import com.highlysucceed.impact_weather.view.dialog.DatePickerDialog;
import com.highlysucceed.impact_weather.view.dialog.DatePickerFarmDialog;
import com.highlysucceed.impact_weather.view.dialog.FarmMapDialog;
import com.server.android.model.MapItem;
import com.server.android.request.crops.CropsAllRequest;
import com.server.android.request.farm.FarmCreateRequest;
import com.server.android.transformer.crop.CropsCollectionTransformer;
import com.server.android.transformer.farm.FarmSingleTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FarmCreateDetailsFragment extends BaseFragment implements View.OnClickListener, FarmMapDialog.Callback, DatePickerFarmDialog.Callback {

    public static final String TAG = FarmCreateDetailsFragment.class.getName().toString();

    private FarmActivity    farmActivity;
    private List<MapItem> mapItems;
    private CropsAdapter cropsAdapter;

    private CropsAllRequest cropsAllRequest;

    @BindView(R.id.farmPlaceHolderCON)      View farmPlaceHolderCON;
    @BindView(R.id.addBTN)                  TextView addBTN;
    @BindView(R.id.drawFarmMapBTN)          TextView drawFarmMapBTN;
    @BindView(R.id.addCropBTN)              TextView addCropBTN;
    @BindView(R.id.farmMapIV)               ImageView farmMapIV;
    @BindView(R.id.titleET)                 EditText titleET;
    @BindView(R.id.sizeET)                  EditText sizeET;
    @BindView(R.id.dateTXT)                 TextView dateTXT;
    @BindView(R.id.cropsEHLV)               ExpandableHeightListView cropsEHLV;

    @BindView(R.id.editFarmPlaceHolderCON)  View editFarmPlaceHolderCON;

    public static FarmCreateDetailsFragment newInstance() {
        FarmCreateDetailsFragment farmCreateDetailsFragment = new FarmCreateDetailsFragment();
        return farmCreateDetailsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_farm_details;
    }

    @Override
    public void onViewReady() {
        farmActivity = (FarmActivity) getActivity();
        farmActivity.setTitle("Farm Details");
        addBTN.setOnClickListener(this);
        addCropBTN.setOnClickListener(this);
        farmMapIV.setOnClickListener(this);

        drawFarmMapBTN.setOnClickListener(this);
        editFarmPlaceHolderCON.setOnClickListener(this);

        dateTXT.setOnClickListener(this);
        mapItems = new ArrayList<>();
//        FarmMapDialog.newInstance(mapItems, this).show(getChildFragmentManager(), FarmMapDialog.TAG);
        setUpFarmListView();
        attemptCropsAll();
        refreshList();
    }

    private void setUpFarmListView(){
        cropsAdapter =  new CropsAdapter(getContext());
        cropsEHLV.setAdapter(cropsAdapter);
    }

    private void attemptCropsAll(){
        cropsAllRequest = new CropsAllRequest(getContext());
        cropsAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addBTN:
                attemptCreate();
                break;
            case R.id.drawFarmMapBTN:
            case R.id.farmMapIV:
            case R.id.editFarmPlaceHolderCON:
                FarmMapDialog.newInstance(mapItems, this).show(getChildFragmentManager(), FarmMapDialog.TAG);
                break;
            case R.id.addCropBTN:
                cropsAdapter.addCrop();
                break;
            case R.id.dateTXT:
                DatePickerFarmDialog.newInstance("",this).show(getFragmentManager(),DatePickerDialog.TAG);
                break;
        }
    }

    private void refreshList(){
        cropsAllRequest.execute();
    }

    private void attemptCreate(){
        FarmCreateRequest farmCreateRequest = new FarmCreateRequest(getContext());
        farmCreateRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating farm...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.NAME, titleET.getText().toString())
                .addParameters(Variable.server.key.SIZE, sizeET.getText().toString())
                .addParameters(Variable.server.key.START_DATE,dateTXT.getText().toString());

        if(cropsAdapter.getCount() != 0){
            List<String> selected = cropsAdapter.getSelected();
            for(String string : selected){
                farmCreateRequest.addParameters("crops[" + selected.indexOf(string) + "]", string);
            }
        }

        if(mapItems.size() != 0){
            for(MapItem map : mapItems){
                farmCreateRequest.addParameters("map[" + mapItems.indexOf(map) + "]", getMapLatLongString(map));
            }
        }

        farmCreateRequest.execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkImage();
    }

    private void checkImage() {
        if (farmMapIV.getDrawable() == null){
            drawFarmMapBTN.setVisibility(View.INVISIBLE);
        } else {
            drawFarmMapBTN.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(FarmCreateRequest.ServerResponse responseData) {
        FarmSingleTransformer farmSingleTransformer = responseData.getData(FarmSingleTransformer.class);
        if(farmSingleTransformer.status){
            UserData.insert(UserData.DEFAULT_FARM_ID, farmSingleTransformer.defaultFarmId);
            Log.e("EEEEEEEEEEE",  ">>>>>>>>" + farmSingleTransformer);
            Log.e("FFFFFFFF",  ">>>>>>>>" + farmSingleTransformer.defaultFarmId);
            Log.e("FFFF",  ">>>>>>>>" + String.valueOf(UserData.getInt(UserData.DEFAULT_FARM_ID)));
            ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            farmActivity.startMainActivity();
        }else{
            ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.FAILED);
            if(farmSingleTransformer.hasRequirements()){
                ErrorResponseManger.first(titleET, farmSingleTransformer.requires.name);
                ErrorResponseManger.first(sizeET, farmSingleTransformer.requires.size);
            }
        }
    }

    @Subscribe
    public void onResponse(CropsAllRequest.ServerResponse responseData) {
        CropsCollectionTransformer cropsCollectionTransformer = responseData.getData(FarmSingleTransformer.class);
        if(cropsCollectionTransformer.status){
            cropsAdapter.setCropsItems(cropsCollectionTransformer.data);
            cropsAdapter.addCrop();
        }
    }

    @Override
    public void onSelected(List<MapItem> mapItems, String center) {
        this.mapItems = mapItems;
        if (mapItems.size() == 0){
            farmMapIV.setVisibility(View.GONE);
            farmPlaceHolderCON.setVisibility(View.VISIBLE);
        }else {
            Picasso.with(getContext())
                    .load(getMapImage(mapItems))
                    .into(farmMapIV);

            farmMapIV.setVisibility(View.VISIBLE);
            farmPlaceHolderCON.setVisibility(View.GONE);
        }
    }

    private String getMapLatLongString(MapItem mapItem){
        return mapItem.geo_lat + "," + mapItem.geo_long;
    }

    private String getMapImage(List<MapItem> map){

        String mapLat = "";
        for(int i = 0 ; i < map.size(); i++){
            mapLat += getMapLatLongString(map.get(i)) + "|";

            if(i == map.size() - 1){
                mapLat += getMapLatLongString(map.get(0));
            }
        }

        return  "http://maps.googleapis.com/maps/api/staticmap?" +
                "key=AIzaSyCAVLdF3n12h7jZjsjt9Lx2cVj6GpEZ7wE" +
                "&size=600x600" +
                "&maptype=satellite" +
                "&path=color:0xffffff|weight:5|fillcolor:0x44b9af|" + mapLat;
    }

    @Override
    public void onDone(String date) {
        dateTXT.setText(date);
    }
}
