package com.highlysucceed.impact_weather.view.activity;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.device.AllDeviceFragment;
import com.highlysucceed.impact_weather.view.fragment.device.DeviceMapFragment;
import com.highlysucceed.impact_weather.view.fragment.main.DeviceFragment;
import com.highlysucceed.impact_weather.view.fragment.device.SubscribeDeviceFragment;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class DeviceActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = DeviceActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }
    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "list":
                openMyDeviceListFragment();
                break;
            case "subscribe":
                openSubscribeDeviceFragment();
                break;
            case "all":
                openAllDeviceFragment();
                break;
            case "map":
                openMyDeviceMapFragment();
                break;
        }
    }

    public void openMyDeviceListFragment(){
        switchFragment(DeviceFragment.newInstance());
    }

    public void openMyDeviceMapFragment(){
        switchFragment(DeviceMapFragment.newInstance());
    }

    public void openSubscribeDeviceFragment(){
        switchFragment(SubscribeDeviceFragment.newInstance());
    }

    public void openAllDeviceFragment(){
        switchFragment(AllDeviceFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

}
