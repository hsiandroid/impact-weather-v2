package com.highlysucceed.impact_weather.view.activity;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.recommendation.RecommendationDetailFragment;
import com.highlysucceed.impact_weather.view.fragment.recommendation.RecommendationFragment;
import com.server.android.model.RecommendationItem;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RecommendationActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = RecommendationActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)          View mainIconIV;
    @BindView(R.id.mainTitleTXT)        TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
         switch (fragmentName){
             case "view":
                 openRecommendationFragment();
                 break;
             case "detail":
                 openRecommendationDetailFragment((RecommendationItem)getFragmentBundle().getParcelable("recommendation_item"));
                 break;
         }
    }

    public void openRecommendationFragment(){
        switchFragment(RecommendationFragment.newInstance());
    }

    public void openRecommendationDetailFragment(RecommendationItem recommendationItem){
        switchFragment(RecommendationDetailFragment.newInstance(recommendationItem));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }
}
