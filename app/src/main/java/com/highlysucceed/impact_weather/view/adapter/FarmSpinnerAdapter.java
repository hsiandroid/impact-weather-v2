package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.FarmItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FarmSpinnerAdapter extends ArrayAdapter {
	private Context context;
	private List<FarmItem> data;
    private LayoutInflater layoutInflater;

	public FarmSpinnerAdapter(Context context) {
        super(context, R.layout.adapter_crop_spinner);
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<FarmItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<FarmItem> data){
        for(FarmItem farmItem : data){
            this.data.add(farmItem);
        }
        notifyDataSetChanged();
    }

    public FarmItem getItemByPosition(int position){
        return data.get(position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.adapter_crop_spinner, parent, false);

        TextView spinner = (TextView) convertView.findViewById(R.id.spinnerTXT);
        spinner.setText(data.get(position).name + " - " + data.get(position).crops.data.get(0).name);
        spinner.setTextSize(14);
        return spinner;
    }

    @Override
    public int getCount()  {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getFarmId(int position) {
        return data.get(position).id;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_crop_spinner, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.activityItem = data.get(position);
        holder.spinnerTXT.setText(holder.activityItem.name + " - " + holder.activityItem.crops.data.get(0).name);
        holder.spinnerTXT.setTextSize(16);
		return convertView;
	}

	public class ViewHolder{
        FarmItem activityItem;

        @BindView(R.id.spinnerTXT)  TextView spinnerTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
