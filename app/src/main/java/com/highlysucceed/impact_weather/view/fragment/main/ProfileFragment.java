package com.highlysucceed.impact_weather.view.fragment.main;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.server.android.model.RecommendationItem;
import com.server.android.model.UserItem;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.SampleData;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.RecommendationAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ProfileFragment extends BaseFragment implements View.OnClickListener, RecommendationAdapter.ClickListener,SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = ProfileFragment.class.getName().toString();

    private MainActivity mainActivity;

    private List<RecommendationItem> recommendationItems;
    private RecommendationAdapter recommendationAdapter;

    @BindView(R.id.nameTXT)             TextView nameTXT;
    @BindView(R.id.addressTXT)          TextView addressTXT;
    @BindView(R.id.viewAllBTN)          TextView viewAllBTN;
    @BindView(R.id.recommendationEHLV)  ExpandableHeightListView recommendationEHLV;
    @BindView(R.id.profileCON)          View profileCON;
    @BindView(R.id.profileCIV)          ImageView profileCIV;
    @BindView(R.id.profileSRL)          SwipeRefreshLayout profileSRL;

    public static ProfileFragment newInstance() {
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_profile));
        mainActivity.hideFarmBTN();
        viewAllBTN.setOnClickListener(this);
        setUpRecommendationListView();
        profileDetails(UserData.getUserItem());

        profileSRL.setColorSchemeResources(R.color.colorPrimary);
        profileSRL.setOnRefreshListener(this);
        sampleData();
    }

    private void profileDetails(UserItem userItem){
        nameTXT.setText(userItem.name);
        addressTXT.setText(userItem.email);

        Picasso.with(getContext())
                .load(userItem.image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(profileCIV);
    }

    private void setUpRecommendationListView() {

        recommendationItems = new ArrayList<>();
        recommendationAdapter = new RecommendationAdapter(getContext());
        recommendationAdapter.setOnItemClickListener(this);
        recommendationEHLV.setExpanded(true);
        recommendationEHLV.setAdapter(recommendationAdapter);
    }

    private void sampleData(){
        recommendationItems = SampleData.getRecommendationItems(3);
        recommendationAdapter.setNewData(recommendationItems);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.viewAllBTN:
                mainActivity.startRecommendationActivity(0, "view");
                break;
        }
    }

    @Override
    public void onRecommendationClick(RecommendationItem recommendationItem) {
//        mainActivity.startRecommendationActivity(recommendationItem.id, "detail");
    }

    @Override
    public void onRefresh() {

        profileCON.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                profileSRL.setRefreshing(false);
                profileCON.setVisibility(View.VISIBLE);
            }
        }, 2000);
    }
}
