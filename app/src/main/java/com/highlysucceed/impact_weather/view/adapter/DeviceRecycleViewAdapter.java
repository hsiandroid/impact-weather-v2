package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.DeviceItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceRecycleViewAdapter extends RecyclerView.Adapter<DeviceRecycleViewAdapter.ViewHolder>  implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<DeviceItem> data;
    private LayoutInflater layoutInflater;

	public DeviceRecycleViewAdapter(Context context, List<DeviceItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<DeviceItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_my_devices, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.deviceItem = data.get(position);

        holder.view.setTag(position);
        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

//        holder.highestTempTXT.setText(holder.deviceItem.info.data.highest_temp);
//        holder.lowestTempTXT.setText(holder.deviceItem.info.data.lowest_temp);
//        holder.durationTXT.setText(holder.deviceItem.info.data.duration);
//        holder.statusTXT.setText(holder.deviceItem.info.data.status);
//        holder.rainAmountTXT.setText(holder.deviceItem.info.data.rain_amount);
//        holder.speedTXT.setText(holder.deviceItem.info.data.speed);
//        holder.degreesTXT.setText(holder.deviceItem.info.data.temperature);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        DeviceItem deviceItem;

        @BindView(R.id.weatherIV)           ImageView weatherIV;
        @BindView(R.id.placeTXT)            TextView placeTXT;
        @BindView(R.id.degreesTXT)          TextView degreesTXT;
        @BindView(R.id.highestTempTXT)      TextView highestTempTXT;
        @BindView(R.id.lowestTempTXT)       TextView lowestTempTXT;
        @BindView(R.id.durationTXT)         TextView durationTXT;
        @BindView(R.id.statusTXT)           TextView statusTXT;
        @BindView(R.id.rainAmountTXT)       TextView rainAmountTXT;
        @BindView(R.id.speedTXT)            TextView speedTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((int)v.getTag(), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((int)v.getTag(), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
