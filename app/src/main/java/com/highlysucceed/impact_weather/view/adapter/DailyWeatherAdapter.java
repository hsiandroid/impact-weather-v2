package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.util.widget.HorizontalListView;
import com.server.android.model.WeatherItem;
import com.highlysucceed.impact_weather.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyWeatherAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;
    private HorizontalListView.MotionEventListener motionEventListener;

	public DailyWeatherAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setMotionEventListener(HorizontalListView.MotionEventListener motionEventListener) {
        this.motionEventListener = motionEventListener;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_daily_weather, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.weatherItem = data.get(position);

        holder.view.setOnClickListener(this);

        int index = 0;
        int select = 2;

        if(select > holder.weatherItem.hourly_weather.size() - 1){
            index = holder.weatherItem.hourly_weather.size() - 1;
        }else{
            index = select;
        }

        holder.dayTXT.setText(holder.weatherItem.hourly_weather.get(index).day);
        holder.highestTempTXT.setText(holder.weatherItem.hourly_weather.get(index).max_temp);
        holder.lowestTempTXT.setText(holder.weatherItem.hourly_weather.get(index).min_temp);
        holder.statusTXT.setText("(" + holder.weatherItem.hourly_weather.get(index).status + ")");
        holder.windSpeedTXT.setText(holder.weatherItem.hourly_weather.get(index).wind_speed);
        holder.dailyTemperatureTXT.setText(holder.weatherItem.hourly_weather.get(index).temperature);

        Picasso.with(context)
                .load(holder.weatherItem.hourly_weather.get(index).image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.dailyWeatherIV);

        if(holder.weatherItem.isVisible){
            holder.hourlyWeatherAdapter = new HourlyWeatherAdapter(context);
            holder.hourlyWeatherAdapter.setNewData(holder.weatherItem.hourly_weather);
            holder.hourlyEHGV.setAdapter(holder.hourlyWeatherAdapter);
            holder.hourlyEHGV.setVisibility(View.VISIBLE);
            if(motionEventListener != null){
                holder.hourlyEHGV.setMotionEventListener(motionEventListener);
            }
        }else{
            holder.hourlyEHGV.setVisibility(View.GONE);
        }

        holder.weeklyCON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherItem weatherItem = data.get(position);
                weatherItem.isVisible = !weatherItem.isVisible;
                data.set(position, weatherItem);
                notifyDataSetChanged();
            }
        });

        return convertView;
	}

    public class ViewHolder{
        WeatherItem weatherItem;
        HourlyWeatherAdapter hourlyWeatherAdapter;

        @BindView(R.id.dayTXT)                  TextView dayTXT;
        @BindView(R.id.dailyWeatherIV)          ImageView dailyWeatherIV;
        @BindView(R.id.dailyTemperatureTXT)     TextView dailyTemperatureTXT;
        @BindView(R.id.statusTXT)               TextView statusTXT;
        @BindView(R.id.highestTempTXT)          TextView highestTempTXT;
        @BindView(R.id.lowestTempTXT)           TextView lowestTempTXT;
        @BindView(R.id.windSpeedTXT)            TextView windSpeedTXT;
        @BindView(R.id.weeklyCON)               View weeklyCON;
        @BindView(R.id.hourlyEHGV)              HorizontalListView hourlyEHGV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        DailyWeatherAdapter.ViewHolder viewHolder = (DailyWeatherAdapter.ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onDailyItemClick(viewHolder.weatherItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onDailyItemClick(WeatherItem weatherItem);
    }
} 
