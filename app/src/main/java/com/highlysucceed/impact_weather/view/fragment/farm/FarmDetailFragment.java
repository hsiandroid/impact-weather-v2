package com.highlysucceed.impact_weather.view.fragment.farm;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ScrollView;

import com.server.android.model.CropItem;
import com.server.android.model.FarmItem;
import com.server.android.model.JournalItem;
import com.server.android.model.MapItem;
import com.server.android.model.RecommendationItem;
import com.server.android.request.farm.FarmDeleteRequest;
import com.server.android.request.farm.FarmShowRequest;
import com.server.android.transformer.farm.FarmSingleTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.util.widget.MultiSwipeRefreshLayout;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.adapter.JournalAdapter;
import com.highlysucceed.impact_weather.view.adapter.ProductAdapter;
import com.highlysucceed.impact_weather.view.adapter.RecommendationAdapter;
import com.highlysucceed.impact_weather.view.dialog.AddCropDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import icepick.State;

public class FarmDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener,
        JournalAdapter.ClickListener,
        ProductAdapter.ClickListener,
        RecommendationAdapter.ClickListener,
        AddCropDialog.Callback,
        PopupMenu.OnMenuItemClickListener {

    public static final String TAG = FarmDetailFragment.class.getName().toString();

    private FarmActivity farmActivity;

    private FarmShowRequest farmShowRequest;
    private ProductAdapter productAdapter;

    @BindView(R.id.cropsEHLV)           ExpandableHeightListView cropsEHLV;
    @BindView(R.id.addCropsBTN)         View addCropsBTN;
    @BindView(R.id.otherOptionBTN)      View otherOptionBTN;
    @BindView(R.id.farmPlaceHolderCON)  View farmPlaceHolderCON;
    @BindView(R.id.farmMapIV)           ImageView farmMapIV;
    @BindView(R.id.scrollView)          ScrollView scrollView;
    @BindView(R.id.swipeRefreshLayout)  MultiSwipeRefreshLayout swipeRefreshLayout;

    @State int farmId;
    @State FarmItem farmItem;

    public static FarmDetailFragment newInstance(int farmId) {
        FarmDetailFragment farmDetailFragment = new FarmDetailFragment();
        farmDetailFragment.farmId = farmId;
        return farmDetailFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_farm_detail;
    }

    @Override
    public void onViewReady() {
        farmActivity = (FarmActivity) getActivity();
        farmActivity.setTitle("");

        addCropsBTN.setOnClickListener(this);
        otherOptionBTN.setOnClickListener(this);

        setUpProductListView();
        attemptShow();

        scrollView.smoothScrollBy(0, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptShow(){
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setSwipeableChildren(R.id.scrollView);
        swipeRefreshLayout.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        swipeRefreshLayout.setOnRefreshListener(this);

        farmShowRequest = new FarmShowRequest(getContext());
        farmShowRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addParameters(Variable.server.key.INCLUDE, "owner, map, crops")
//                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops")
                .addParameters(Variable.server.key.FARM_ID, farmId);
    }

    private void attemptDelete(){

        new FarmDeleteRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting farm...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops")
                .addParameters(Variable.server.key.FARM_ID, farmId)
                .execute();
    }

    private void refreshList(){
        otherOptionBTN.setVisibility(View.GONE);
        addCropsBTN.setVisibility(View.GONE);
        farmShowRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(FarmShowRequest.ServerResponse responseData) {
        FarmSingleTransformer farmSingleTransformer = responseData.getData(FarmSingleTransformer.class);
        if(farmSingleTransformer.status){

            otherOptionBTN.setVisibility(View.VISIBLE);
            addCropsBTN.setVisibility(View.VISIBLE);
            farmItem = farmSingleTransformer.data;

            farmActivity.setTitle(farmItem.name);
            productAdapter.setNewData(farmItem.crops.data);
            if (farmItem.map.data.size() == 0){
                farmPlaceHolderCON.setVisibility(View.VISIBLE);
                farmMapIV.setVisibility(View.GONE);
            }else {
                Picasso.with(getContext())
                        .load(getMapImage(farmItem.map.data))
                        .into(farmMapIV);
                farmMapIV.setVisibility(View.VISIBLE);
                farmPlaceHolderCON.setVisibility(View.GONE);
            }

        }else{
            farmActivity.onBackPressed();
        }
    }

    private String getMapImage(List<MapItem> map){

        String mapLat = "";
        for(int i = 0 ; i < map.size(); i++){
            mapLat += map.get(i).geo_lat + "," +  map.get(i).geo_long + "|";
            if(i == map.size() - 1){
                mapLat += map.get(0).geo_lat + "," +  map.get(0).geo_long;
            }
        }

        return  "http://maps.googleapis.com/maps/api/staticmap?" +
                "key=AIzaSyCAVLdF3n12h7jZjsjt9Lx2cVj6GpEZ7wE" +
                "&size=600x600" +
                "&maptype=satellite" +
                "&path=color:0xffffff|weight:5|fillcolor:0x44b9af|" + mapLat;
    }

    @Subscribe
    public void onResponse(FarmDeleteRequest.ServerResponse responseData) {
        FarmSingleTransformer farmSingleTransformer = responseData.getData(FarmSingleTransformer.class);
        if(farmSingleTransformer.status){
            ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            farmActivity.startMainActivity();
            UserData.insert(UserData.DEFAULT_FARM_ID, farmSingleTransformer.defaultFarmId);
        }else{
            ToastMessage.show(getActivity(), farmSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void setUpProductListView() {
        productAdapter = new ProductAdapter(getContext());
        productAdapter.setOnItemClickListener(this);
        cropsEHLV.setAdapter(productAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addCropsBTN:
                AddCropDialog.newInstance(farmId, farmItem.crops.data, this).show(getChildFragmentManager(), AddCropDialog.TAG);
                break;
            case R.id.otherOptionBTN:
                otherOption(v);
                break;
        }
    }

    private void otherOption(View v){
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.farm_other_option, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    private void deleteFarm(){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to delete " + farmItem.name + "?")
                .setIcon(R.drawable.icon_successful)
                .setPositiveButtonText("Delete")
                .setNegativeButtonText("Cancel")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptDelete();
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }


    @Override
    public void onCropClickListener(CropItem cropItem) {
//        farmActivity.openFarmJournalFragment(farmId, cropItem.name);
        farmActivity.openJournalCalendarFragment(farmId, cropItem.name);
    }

    @Override
    public void onJournalClickListener(JournalItem journalItem) {

    }

    @Override
    public void onJournalDeleteClickListener(JournalItem journalItem) {
//        EditActivityDialog.newInstance(journalItem).show(getChildFragmentManager(), EditActivityDialog.TAG);
    }

    @Override
    public void onRecommendationClick(RecommendationItem recommendationItem) {
        farmActivity.startRecommendationActivity(recommendationItem.id, "detail");
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.manage_crops:
                AddCropDialog.newInstance(farmId, farmItem.crops.data, this).show(getChildFragmentManager(), AddCropDialog.TAG);
                break;
            case R.id.delete:
                deleteFarm();
                break;
            case R.id.edit:
                farmActivity.openEditFarmDetailsFragment(farmId);
                break;
        }
        return true;
    }

    @Override
    public void onDone() {
        refreshList();
    }
}
