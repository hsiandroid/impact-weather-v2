package com.highlysucceed.impact_weather.view.dialog;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.highlysucceed.impact_weather.view.adapter.ActivitySpinnerAdapter;
import com.server.android.model.ActivityItem;
import com.server.android.request.journals.JournalsCreateRequest;
import com.server.android.transformer.journals.JournalsSingleTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class AddActivityDialog extends BaseDialog implements View.OnClickListener,DatePickerDialog.ClickListener {
	public static final String TAG = AddActivityDialog.class.getName().toString();

	private ActivitySpinnerAdapter activitySpinnerAdapter;

	@BindView(R.id.activityTitleET)		EditText activityTitleET;
	@BindView(R.id.startDateTXT)		TextView startDateTXT;
	@BindView(R.id.endDateET)			TextView endDateET;
	@BindView(R.id.saveBTN)				TextView saveBTN;
	@BindView(R.id.cancelBTN)			TextView cancelBTN;
	@BindView(R.id.activitySPR)			Spinner activitySPR;
	@BindView(R.id.addEndDateBTN)		View addEndDateBTN;

	@State int farmId;
	@State String crop;
	@State String startDate;

	private Callback callback;

	public static AddActivityDialog newInstance(int farmId, String crop, String startDate, Callback callback) {
		AddActivityDialog addActivityDialog = new AddActivityDialog();
		addActivityDialog.farmId = farmId;
		addActivityDialog.crop = crop;
		addActivityDialog.startDate = startDate;
		addActivityDialog.callback = callback;
		return addActivityDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_add_activity;
	}

	@Override
	public void onViewReady() {
		saveBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		addEndDateBTN.setOnClickListener(this);
		endDateET.setOnClickListener(this);
		startDateTXT.setText(startDate);
		endDateET.setText(startDate);

		setUpSPR();
	}

	private void setUpSPR(){
		activitySpinnerAdapter = new ActivitySpinnerAdapter(getContext());
		activitySpinnerAdapter.setNewData(UserData.getActivityItems());
		activitySPR.setAdapter(activitySpinnerAdapter);
		activitySPR.getBackground().setColorFilter(ActivityCompat.getColor(getContext(),R.color.white), PorterDuff.Mode.SRC_ATOP);
	}

//	private List<ActivityItem> getData(){
//		List<ActivityItem> activityItems = new ArrayList<>();
//		ActivityItem activityItem = new ActivityItem();
//		activityItem.id = 1;
//		activityItem.name = "Seeding";
//		activityItems.add(activityItem);
//
//		activityItem = new ActivityItem();
//		activityItem.id = 1;
//		activityItem.name = "Harvesting";
//		activityItems.add(activityItem);
//
//		activityItem = new ActivityItem();
//		activityItem.id = 1;
//		activityItem.name = "Planting";
//		activityItems.add(activityItem);
//
//		return activityItems;
//	}

	private void attemptCreate(){
		JournalsCreateRequest journalsCreateRequest = new JournalsCreateRequest(getContext());
		journalsCreateRequest
				.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating journal...", false, false))
				.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Variable.server.key.FARM_ID, farmId)
				.addParameters(Variable.server.key.CROP, crop)
				.addParameters(Variable.server.key.TITLE, ((ActivityItem)activitySPR.getSelectedItem()).title)
				.addParameters(Variable.server.key.STATUS, "low")
				.addParameters(Variable.server.key.START_DATE, startDateTXT.getText().toString())
				.addParameters(Variable.server.key.END_DATE, endDateET.getText().toString())
				.execute();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe
	public void onResponse(JournalsCreateRequest.ServerResponse responseData) {
        JournalsSingleTransformer journalsSingleTransformer = responseData.getData(JournalsSingleTransformer.class);
		if(journalsSingleTransformer.status){
			ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.SUCCESS);
			if(callback != null){
				callback.onDone();
			}
			dismiss();
		}else{
			ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.FAILED);
			if(journalsSingleTransformer.hasRequirements()){
				ErrorResponseManger.first(activityTitleET, journalsSingleTransformer.requires.title);
				ErrorResponseManger.first(endDateET, journalsSingleTransformer.requires.entry_date);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.saveBTN:
				attemptCreate();
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
			case R.id.endDateET:
			case R.id.addEndDateBTN:
				DatePickerDialog.newInstance(startDate).setClickListener(this).show(getChildFragmentManager(), DatePickerDialog.TAG);
				break;
		}
	}

	@Override
	public void onclick(String date) {
		endDateET.setText(date);
	}

	public interface Callback {
		void onDone();
	}
}
