package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.WeatherItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeeklyWeatherRecycleViewAdapter extends RecyclerView.Adapter<WeeklyWeatherRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener {

	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;

	public WeeklyWeatherRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_weekly_weather, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//	    String lastRecord = "";
//        holder.weatherItem = data.get(position);
//
//	    if(lastRecord.length() == 0 && !lastRecord.equalsIgnoreCase(holder.weatherItem.day)){
//	        lastRecord = holder.weatherItem.day; // save new record
//
//            holder.view.setTag(position);
//            holder.view.setOnClickListener(this);
//
//
//            holder.dayTXT.setText(holder.weatherItem.day);
//            holder.minTempTXT.setText(holder.weatherItem.max_temp);
//            holder.maxTempTXT.setText(holder.weatherItem.min_temp);

            holder.view.setTag(position);
            holder.view.setOnClickListener(this);

            holder.weatherItem = data.get(position);

            holder.dayTXT.setText(holder.weatherItem.day);
            holder.minTempTXT.setText(holder.weatherItem.max_temp);
            holder.maxTempTXT.setText(holder.weatherItem.min_temp);

            Picasso.with(context)
                    .load(holder.weatherItem.image)
                    .placeholder(R.drawable.icon_weather)
                    .error(R.drawable.icon_weather)
                    .into(holder.weatherIV);
       // }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        WeatherItem weatherItem;
        @BindView(R.id.dayTXT)         TextView dayTXT;
        @BindView(R.id.weatherIV)       ImageView weatherIV;
        @BindView(R.id.minTempTXT)  TextView minTempTXT;
        @BindView(R.id.maxTempTXT)   TextView maxTempTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onHourItemClick(data.get((int) v.getTag()));
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onHourItemClick(WeatherItem weatherItem);
    }
} 
