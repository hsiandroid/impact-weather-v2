package com.highlysucceed.impact_weather.view.fragment;

import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;

import butterknife.BindView;

public class TemplateFragment extends BaseFragment {

    public static final String TAG = TemplateFragment.class.getName().toString();

    @BindView(R.id.textviewTxt) TextView textViewTxt;

    public static TemplateFragment newInstance() {
        TemplateFragment fragment = new TemplateFragment();
        return fragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_template;
    }

    @Override
    public void onViewReady() {
        textViewTxt.setText("Hello fragment");
    }
}
