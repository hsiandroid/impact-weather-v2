package com.highlysucceed.impact_weather.view.fragment.farm;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.server.android.model.JournalItem;
import com.server.android.request.farm.FarmCropRequest;
import com.server.android.request.journals.JournalsDeleteRequest;
import com.server.android.transformer.farm.FarmCropTransformer;
import com.server.android.transformer.journals.JournalsSingleTransformer;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.adapter.JournalAdapter;
import com.highlysucceed.impact_weather.view.adapter.RecommendationAdapter;
import com.highlysucceed.impact_weather.view.dialog.AddActivityDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class FarmJournalFragment extends BaseFragment implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        JournalAdapter.ClickListener,
        AddActivityDialog.Callback {

    public static final String TAG = FarmJournalFragment.class.getName().toString();
    private FarmActivity farmActivity;
    private FarmCropRequest farmCropRequest;

    @BindView(R.id.journalEHLV)         ExpandableHeightListView journalEHLV;
    @BindView(R.id.recommendationEHLV)  ExpandableHeightListView recommendationEHLV;
    @BindView(R.id.viewAllBTN)          TextView viewAllBTN;
    @BindView(R.id.addBTN)              View addBTN;
    @BindView(R.id.journalsPB)          View journalsPB;

    @BindView(R.id.recommendationPlaceholderCON)          View recommendationPlaceholderCON;
    @BindView(R.id.journalPlaceholderCON)                 View journalPlaceholderCON;
    @BindView(R.id.swipeRefreshLayout)  SwipeRefreshLayout swipeRefreshLayout;

    @State int farmId;
    @State String crop;

    private JournalAdapter journalAdapter;
    private RecommendationAdapter recommendationAdapter;

    public static FarmJournalFragment newInstance(int farmId, String crop) {
        FarmJournalFragment farmJournalFragment = new FarmJournalFragment();
        farmJournalFragment.farmId = farmId;
        farmJournalFragment.crop = crop;
        return farmJournalFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_farm_journal;
    }

    @Override
    public void onViewReady() {


        Log.e("Farm Id", ">>>" + farmId);
        Log.e("Crop", ">>>" + crop);

        farmActivity = (FarmActivity) getActivity();
        farmActivity.setTitle(crop);

        viewAllBTN.setOnClickListener(this);
        addBTN.setOnClickListener(this);


        setUpJournalListView();
        attemptFarmCrop();

        setUpRecommendationListView();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    private void attemptFarmCrop(){
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);

        farmCropRequest = new FarmCropRequest(getContext());
        farmCropRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .addParameters(Variable.server.key.INCLUDE, "journals, farm, recommendations")
                .addParameters(Variable.server.key.FARM_ID, farmId)
                .addParameters(Variable.server.key.CROP, crop);
    }

    private void attemptDelete(int journalId) {
        new JournalsDeleteRequest(getContext())
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Deleting Journal...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops")
                .addParameters(Variable.server.key.JOURNAL_ID, journalId)
                .execute();
    }

    private void refreshList(){
        farmCropRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(FarmCropRequest.ServerResponse responseData) {
        journalsPB.setVisibility(View.GONE);
        FarmCropTransformer farmCropTransformer = responseData.getData(FarmCropTransformer.class);
        if(farmCropTransformer.status){
            journalAdapter.setNewData(farmCropTransformer.data.journals.data);
            recommendationAdapter.setNewData(farmCropTransformer.data.recommendations.data);
        }

        if (journalAdapter.getCount() == 0){
            journalPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            journalPlaceholderCON.setVisibility(View.GONE);
        }

        if (recommendationAdapter.getCount() == 0){
            recommendationPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            recommendationPlaceholderCON.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(JournalsDeleteRequest.ServerResponse responseData) {
        journalsPB.setVisibility(View.GONE);
        JournalsSingleTransformer journalsSingleTransformer = responseData.getData(JournalsSingleTransformer.class);
        if(journalsSingleTransformer.status){
            journalAdapter.removeItem(journalsSingleTransformer.data);
        }

        if (journalAdapter.getCount() == 0){
            journalPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            journalPlaceholderCON.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void setUpJournalListView() {

        journalAdapter = new JournalAdapter(getContext());
        journalEHLV.setAdapter(journalAdapter);
        journalAdapter.setOnItemClickListener(this);
    }

    private void setUpRecommendationListView() {
        recommendationAdapter = new RecommendationAdapter(getContext());
        recommendationEHLV.setAdapter(recommendationAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewAllBTN:
                farmActivity.startRecommendationActivity(0, "view");
                break;
            case R.id.addBTN:
                AddActivityDialog.newInstance(farmId, crop, "", this).show(getChildFragmentManager(), AddActivityDialog.TAG);
                break;
        }
    }

    private void deleteFarm(final int id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Are you sure you want to delete?")
                .setIcon(R.drawable.icon_successful)
                .setPositiveButtonText("Delete")
                .setNegativeButtonText("Cancel")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptDelete(id);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }

    @Override
    public void onJournalClickListener(JournalItem journalItem) {

    }

    @Override
    public void onJournalDeleteClickListener(JournalItem journalItem) {
        deleteFarm(journalItem.id);
    }

    @Override
    public void onDone() {
        refreshList();
    }
}
