package com.highlysucceed.impact_weather.view.dialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.FarmAdapter;
import com.server.android.model.FarmItem;
import com.server.android.request.farm.FarmAllRequest;
import com.server.android.transformer.farm.FarmCollectionTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class AllFarmDialog extends BaseDialog implements FarmAdapter.ClickListener, View.OnClickListener {
	public static final String TAG = AllFarmDialog.class.getName().toString();

	private MainActivity activity;
	private FarmAllRequest farmAllRequest;
	private FarmAdapter farmAdapter;
	private Callback callback;

	@BindView(R.id.homeEHLV) 			ExpandableHeightListView homeEHLV;
	@BindView(R.id.progressBAR) 		ProgressBar progressBar;
	@BindView(R.id.bgLayout) 			RelativeLayout exitBTN;
	@BindView(R.id.addFarmPHLayout) 	LinearLayout addFarmPHLayout;
	@BindView(R.id.addFarmPH1) 			LinearLayout addFarmPH1;
	@BindView(R.id.addFarmPH2) 			LinearLayout addFarmPH2;
	@BindView(R.id.addFarmPH3) 			LinearLayout addFarmPH3;

	public static AllFarmDialog Builder(Callback callback) {
		AllFarmDialog infoDialog = new AllFarmDialog();
		infoDialog.callback = callback;
		return infoDialog;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_all_farm;
	}

	@Override
	public void onViewReady() {
		attemptFarmAll();
		setUpHomeListView();
		exitBTN.setOnClickListener(this);
		activity = (MainActivity) getContext();
	}

	private void setUpHomeListView() {
		farmAdapter = new FarmAdapter(getContext());
		farmAdapter.setOnItemClickListener(this);
		homeEHLV.setAdapter(farmAdapter);
	}

	private void attemptFarmAll(){
		farmAllRequest = new FarmAllRequest(getContext());
		farmAllRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
				.addParameters(Variable.server.key.INCLUDE, "info, owner, map, crops, status");
		farmAllRequest.first();
	}

	@Subscribe
	public void onResponse(FarmAllRequest.ServerResponse responseData) {
		FarmCollectionTransformer farmCollectionTransformer = responseData.getData(FarmCollectionTransformer.class);
		if(farmCollectionTransformer.status){
			if(responseData.isNext()){
				farmAdapter.addNewData(farmCollectionTransformer.data);

			}else{
				farmAdapter.setNewData(farmCollectionTransformer.data);
			}
			switch (farmAdapter.getCount()){
                case 1:
                    addFarmPHLayout.setVisibility(View.VISIBLE);
                    addFarmPH1.setVisibility(View.GONE);
                    addFarmPH2.setVisibility(View.VISIBLE);
                    addFarmPH3.setVisibility(View.VISIBLE);
                    addFarmPH2.setOnClickListener(this);
                    addFarmPH3.setOnClickListener(this);
                    break;
                case 2:
                    addFarmPHLayout.setVisibility(View.VISIBLE);
                    addFarmPH1.setVisibility(View.GONE);
                    addFarmPH2.setVisibility(View.GONE);
                    addFarmPH3.setVisibility(View.VISIBLE);
                    addFarmPH3.setOnClickListener(this);
                    break;
                case 3:
                    addFarmPHLayout.setVisibility(View.GONE);
                    break;
                    default:
                        addFarmPHLayout.setVisibility(View.VISIBLE);
                        addFarmPH1.setVisibility(View.VISIBLE);
                        addFarmPH2.setVisibility(View.VISIBLE);
                        addFarmPH3.setVisibility(View.VISIBLE);
                        addFarmPH1.setOnClickListener(this);
                        addFarmPH2.setOnClickListener(this);
                        addFarmPH3.setOnClickListener(this);
                        break;
            }
			progressBar.setVisibility(View.GONE);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}


	@Override
	public void onItemClick(FarmItem farmItem) {
		if (callback != null){
			callback.onSuccess(farmItem.id);
			dismiss();
		}
	}

	@Override
	public void onClick(View view) {
		switch(view.getId()){
			case R.id.bgLayout:
				dismiss();
				break;
            case R.id.addFarmPH1:
                activity.startMyFarmActivity(0,"create");
                dismiss();
                break;
            case R.id.addFarmPH2:
                activity.startMyFarmActivity(0,"create");
                dismiss();
                break;
            case R.id.addFarmPH3:
                activity.startMyFarmActivity(0,"create");
                dismiss();
                break;
		}
	}

	public interface  Callback{
		void onSuccess(int id);
		}
}


