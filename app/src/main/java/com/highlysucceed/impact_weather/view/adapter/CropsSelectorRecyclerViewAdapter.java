package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.CropsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CropsSelectorRecyclerViewAdapter extends RecyclerView.Adapter<CropsSelectorRecyclerViewAdapter.ViewHolder>  implements View.OnClickListener{
	private Context context;
	private List<CropsItem> data;
    private LayoutInflater layoutInflater;

	public CropsSelectorRecyclerViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<CropsItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_crops_selector, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.item = data.get(position);

        holder.view.setTag(holder.item);
        holder.view.setOnClickListener(this);

        holder.cropsTXT.setText(holder.item.name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CropsItem item;

        @BindView(R.id.cropsTXT)
        TextView cropsTXT;

        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((CropsItem)v.getTag());
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(CropsItem cropsItem);
    }
} 
