package com.highlysucceed.impact_weather.view.fragment.weather;

import android.widget.ImageView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.WeatherActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MeteogramreviewFragment extends BaseFragment {
    public static final String TAG = MeteogramreviewFragment.class.getName().toString();

    private WeatherActivity weatherActivity;

    @BindView(R.id.imageIV)         ImageView imageIV;

    @State String path;
    @State String code;

    public static MeteogramreviewFragment newInstance(String code, String path) {
        MeteogramreviewFragment fragment = new MeteogramreviewFragment();
        fragment.path = path;
        fragment.code = code;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_photo_preview;
    }

    @Override
    public void onViewReady() {
        weatherActivity = (WeatherActivity) getContext();
        weatherActivity.setTitle(code);

        Picasso.with(getContext())
                .load(path)
                .into(imageIV);
    }
}
