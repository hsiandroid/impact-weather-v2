package com.highlysucceed.impact_weather.view.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.adapter.AdvisoryAdapter;
import com.server.android.model.AdvisoryItem;
import com.server.android.model.FarmItem;
import com.server.android.request.farm.FarmAllRequest;
import com.server.android.transformer.farm.FarmCollectionTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

public class AdvisoryFragment extends BaseFragment implements AdvisoryAdapter.ClickListener,View.OnClickListener{

    public static final String TAG = AdvisoryFragment.class.getName().toString();

    private MainActivity mainActivity;

    private FarmAllRequest farmAllRequest;
    private AdvisoryAdapter advisoryAdapter;
    private List<AdvisoryItem> advisoryItems;


    @BindView(R.id.myFarmLV)            ExpandableHeightListView myFarmLV;

    public static AdvisoryFragment newInstance(List<AdvisoryItem> advisoryItems) {
        AdvisoryFragment farmFragment = new AdvisoryFragment();
        farmFragment.advisoryItems = advisoryItems;
        return farmFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_advisory;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle("Notifications");
        mainActivity.hideFarmBTN();
        setUpFarmListView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void setUpFarmListView() {
        advisoryAdapter = new AdvisoryAdapter(getContext());
        myFarmLV.setAdapter(advisoryAdapter);
        myFarmLV.setExpanded(true);
        advisoryAdapter.setNewData(advisoryItems);
        advisoryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }

    @Override
    public void onItemClick(AdvisoryItem advisoryItem) {

    }
}
