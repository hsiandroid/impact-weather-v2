package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.WidgetItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WidgetAdapter extends BaseAdapter implements View.OnClickListener, View.OnLongClickListener {
	private Context context;
	private List<WidgetItem> data;
    private LayoutInflater layoutInflater;

	public WidgetAdapter(Context context, List<WidgetItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WidgetItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_widget, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.view.setOnClickListener(this);
        holder.view.setOnLongClickListener(this);

        holder.widgetItem = data.get(position);
        holder.widgetCIV.setImageResource(holder.widgetItem.image);
        holder.widgetNameTXT.setText(holder.widgetItem.name);

//        Picasso.with(context)
//                .load(holder.widgetItem.icon)
//                .into(holder.widgetIV);

		return convertView;
	}


	public class ViewHolder{
        WidgetItem widgetItem;

        @BindView(R.id.widgetCIV)                ImageView widgetCIV;
        @BindView(R.id.widgetNameTXT)             TextView widgetNameTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}


    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(data.indexOf(viewHolder.widgetItem), v);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemLongClick(data.indexOf(viewHolder.widgetItem), v);
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
} 
