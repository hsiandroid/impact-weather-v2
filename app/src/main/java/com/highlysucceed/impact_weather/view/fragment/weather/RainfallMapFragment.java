package com.highlysucceed.impact_weather.view.fragment.weather;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.WeatherActivity;

public class RainfallMapFragment extends BaseFragment {

    public static final String TAG = RainfallMapFragment.class.getName().toString();

    private WeatherActivity weatherActivity;

    public static RainfallMapFragment newInstance() {
        RainfallMapFragment rainfallMapFragment = new RainfallMapFragment();
        return rainfallMapFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_rainfall_map;
    }

    @Override
    public void onViewReady() {
        weatherActivity = (WeatherActivity) getActivity();
        weatherActivity.setTitle(getString(R.string.rainfall_area));
    }
}
