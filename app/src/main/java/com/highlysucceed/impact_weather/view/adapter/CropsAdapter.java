package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.BaseActivity;
import com.highlysucceed.impact_weather.view.dialog.CropDialog;
import com.highlysucceed.impact_weather.view.dialog.VarietyDialog;
import com.server.android.model.CropSelectedItem;
import com.server.android.model.CropsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CropsAdapter extends BaseAdapter implements
        View.OnClickListener,
        CropDialog.CropsCallback,
        VarietyDialog.VarietyCallback{

    private Context context;
    private List<CropSelectedItem> data;
    private List<CropsItem> cropsItems;
    private LayoutInflater layoutInflater;

    public CropsAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public List<CropSelectedItem> getData() {
        if(data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    public void setCropsItems(List<CropsItem> cropsItems) {
        this.cropsItems = cropsItems;
        fixCropSelected();
    }

    private void fixCropSelected(){

    }

    public List<CropsItem> getCropsItems() {
        if(cropsItems == null){
            cropsItems = new ArrayList<>();
        }
        return cropsItems;
    }

    private CropsItem getFirstCrop(){
        if(getCropsItems().size() > 0){
            return getCropsItems().get(0);
        }
        return null;
    }

    private CropsItem getCropByPosition(int position){
        if(getData().size() < position){
            return getData().get(position).cropsItem;
        }
        return null;
    }

    private CropsItem getCropByName(String name){
        for(CropsItem cropsItem : getCropsItems()){
            if(cropsItem.name.equalsIgnoreCase(name)){
                return cropsItem;
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return getData().size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    public void addCrop(){
        CropSelectedItem cropSelectedItem = new CropSelectedItem();
        CropsItem cropsItem = getFirstCrop();
        if(cropsItem != null){
            cropSelectedItem.cropsItem = cropsItem;
            cropSelectedItem.name = cropsItem.name;
            cropSelectedItem.variety = cropsItem.getFirstVariety();
        }
        getData().add(cropSelectedItem);
        notifyDataSetChanged();
    }

    public void addCrop(CropSelectedItem cropSelectedItem){
        getData().add(cropSelectedItem);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_crops, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.item = getData().get(position);

        holder.cropBTN.setTag(position);
        holder.varietyBTN.setTag(position);
        holder.removeBTN.setTag(position);

        holder.cropsTXT.setText(holder.item.name);
        holder.varietyTXT.setText(holder.item.variety);

        holder.removeBTN.setVisibility(getData().size() == 1 ? View.GONE : View.VISIBLE);

        return convertView;
    }

    @Override
    public void cropsCallback(CropsItem cropsItem, Object target) {
        CropSelectedItem item = getData().get((int) target);
        item.cropsItem = cropsItem;
        item.name = cropsItem.name;
        item.variety = cropsItem.getFirstVariety();
        notifyDataSetChanged();
    }

    @Override
    public void varietyCallback(String variety, Object target) {
        CropSelectedItem item = getData().get((int) target);
        item.variety = variety;
        notifyDataSetChanged();
    }


    public class ViewHolder{

        private CropSelectedItem item;

        @BindView(R.id.cropsTXT)            TextView cropsTXT;
        @BindView(R.id.varietyTXT)          TextView varietyTXT;
        @BindView(R.id.cropBTN)             View cropBTN;
        @BindView(R.id.varietyBTN)          View varietyBTN;
        @BindView(R.id.removeBTN)           View removeBTN;

        private View view;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
            this.view = view;
            cropBTN.setOnClickListener(CropsAdapter.this);
            varietyBTN.setOnClickListener(CropsAdapter.this);
            removeBTN.setOnClickListener(CropsAdapter.this);
        }

    }

    public List<String> getSelected(){
        List<String> selected = new ArrayList<>();
        for(CropSelectedItem cropsItem : getData()){
            selected.add(cropsItem.name + "|" + cropsItem.variety);
        }
        return selected;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.removeBTN:
                getData().remove((int) v.getTag());
                notifyDataSetChanged();
                break;
            case R.id.cropBTN:
                CropDialog.newInstance(getCropsItems(), v.getTag(), this).show(((BaseActivity)context).getSupportFragmentManager(), CropDialog.TAG);
                break;
            case R.id.varietyBTN:
                variety((int) v.getTag());
                break;
        }
    }

    private void variety(int position){
        CropSelectedItem cropSelectedItem = getData().get(position);
        if(cropSelectedItem.cropsItem == null){
            cropSelectedItem.cropsItem = getCropByName(cropSelectedItem.name);
        }
        if(cropSelectedItem.cropsItem != null){
            VarietyDialog.newInstance(cropSelectedItem.cropsItem.getVariety(), position, this).show(((BaseActivity)context).getSupportFragmentManager(), VarietyDialog.TAG);
        }
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCropClick(int position);
    }
} 
