package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.server.android.model.CropItem;
import com.highlysucceed.impact_weather.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CropAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<CropItem> data;
    private LayoutInflater layoutInflater;

	public CropAdapter(Context context, List<CropItem> data) {
		this.context = context;
		this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

	public CropAdapter(Context context, List<CropItem> data, List<CropItem> selectedItems) {
		this.context = context;
        for(CropItem cropItem : selectedItems){
            for(CropItem item : data){
                if(cropItem.name.equalsIgnoreCase(item.name)){
                    item.isSelected = true;
                    data.set(data.indexOf(item), item);
                }
            }
        }
        this.data = data;
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<CropItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_crop, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.cropItem = data.get(position);
        holder.view.setOnClickListener(this);

        holder.cropCB.setText(holder.cropItem.name);
        holder.cropCB.setChecked(holder.cropItem.isSelected);

        holder.cropCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.cropItem.isSelected = holder.cropCB.isChecked();
                data.set(position, holder.cropItem);
                notifyDataSetChanged();
            }
        });

		return convertView;
	}

	public int getSelectedCount(){
        int count = 0;
        for(CropItem item : data){
            if(item.isSelected){
                count ++;
            }
        }
        return count;
    }

	public String getSelectedItem(){
        String count = "";
        for(CropItem item : data){
            if(item.isSelected){
                count += item.name + ",";
            }
        }
        return count;
    }

	public List<CropItem> getSelectedItems(){
        List<CropItem> cropItems = new ArrayList<>();
        for(CropItem item : data){
            if(item.isSelected){
                cropItems.add(item);
            }
        }
        return cropItems;
    }

	public void setSelectedItems(List<CropItem> selectedItems){
        for(CropItem cropItem : selectedItems){
            for(CropItem item : data){
                if(cropItem.name.equalsIgnoreCase(item.name)){
                    item.isSelected = true;
                    data.set(data.indexOf(item), item);
                }
            }
        }

        notifyDataSetChanged();
    }

    public class ViewHolder{
        CropItem cropItem;
        @BindView(R.id.cropCB) CheckBox cropCB;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onCropClick(viewHolder.cropItem);
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCropClick(CropItem cropItem);
    }
} 
