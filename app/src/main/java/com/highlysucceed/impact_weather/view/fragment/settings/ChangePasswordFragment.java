package com.highlysucceed.impact_weather.view.fragment.settings;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.MainActivity;
import com.highlysucceed.impact_weather.view.activity.SettingsActivity;
import com.server.android.request.ChangePasswordRequest;
import com.server.android.request.auth.LoginRequest;
import com.server.android.transformer.EditProfileTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;
import com.squareup.picasso.Picasso;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ChangePasswordFragment.class.getName().toString();

    private MainActivity settingsActivity;


    @BindView(R.id.oldPasswordET)               EditText oldPasswordET;
    @BindView(R.id.newPasswordET)               EditText newPasswordET;
    @BindView(R.id.newConfirmPasswordET)        EditText newConfirmPasswordET;
    @BindView(R.id.confirmBTN)                  View confirmBTN;
    @BindView(R.id.avatarCIV)                   ImageView avatarCIV;

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
        return changePasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_change_password;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (MainActivity) getContext();
        settingsActivity.setTitle("Change Password");
        confirmBTN.setOnClickListener(this);

        Picasso.with(getContext()).load(UserData.getUserItem().image)
                .placeholder(R.drawable.placeholder_avatar)
                .centerCrop()
                .fit()
                .into(avatarCIV);
    }

    private void attemptChangePassword(){
        if (newPasswordET.getText().toString().equals(newConfirmPasswordET.getText().toString())){
            ChangePasswordRequest loginRequest = new ChangePasswordRequest(getContext());
            loginRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Changing password...", false, false))
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Variable.server.key.CURRENT_PASSWORD, oldPasswordET.getText().toString())
                    .addParameters(Variable.server.key.NEW_PASSWORD, newPasswordET.getText().toString())
                    .execute();
        }else{
            ToastMessage.show(getContext(), "Password does not match", ToastMessage.Status.FAILED);
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(ChangePasswordRequest.ServerResponse responseData) {
        EditProfileTransformer editProfileTransformer = responseData.getData(EditProfileTransformer.class);
        if(editProfileTransformer.status){
            oldPasswordET.setText("");
            newPasswordET.setText("");
            newConfirmPasswordET.setText("");
            ToastMessage.show(getActivity(), editProfileTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getActivity(), editProfileTransformer.msg, ToastMessage.Status.FAILED);
            if(editProfileTransformer.hasRequirements()){
                ErrorResponseManger.first(oldPasswordET, editProfileTransformer.requires.current_password);
                ErrorResponseManger.first(newPasswordET, editProfileTransformer.requires.new_password);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBTN:
                attemptChangePassword();
                break;
        }
    }

//    @Subscribe
//    public void onResponse(Auth.PasswordResponse responseData) {
//        SingleTransformer<UserModel> userSingleTransformer = responseData.getData(SingleTransformer.class);
//        if(userSingleTransformer.status){
//            UserData.insert(userSingleTransformer.data);
//            settingsActivity.startMainActivity("settings");
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
//        }else{
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
//            if(responseData.getData().hasRequirements()){
//                ErrorResponseManger.first(oldPasswordET, userSingleTransformer.errors.current_password);
//                ErrorResponseManger.first(newPasswordET, userSingleTransformer.errors.password);
//            }
//        }
//    }
}
