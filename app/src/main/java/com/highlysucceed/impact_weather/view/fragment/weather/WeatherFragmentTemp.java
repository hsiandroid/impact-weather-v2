package com.highlysucceed.impact_weather.view.fragment.weather;

import android.app.ProgressDialog;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.highlysucceed.impact_weather.util.widget.HorizontalListView;
import com.highlysucceed.impact_weather.view.activity.WeatherActivity;
import com.highlysucceed.impact_weather.view.adapter.DailyWeatherAdapter;
import com.highlysucceed.impact_weather.view.adapter.HourlyWeatherAdapter;
import com.highlysucceed.impact_weather.view.dialog.WeatherDetailDialog;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.ConfirmationDialog;
import com.server.android.model.DeviceItem;
import com.server.android.model.WeatherItem;
import com.server.android.request.station.StationInfoRequest;
import com.server.android.request.station.StationUnSubscribeRequest;
import com.server.android.transformer.station.StationSingleTransformer;
import com.server.android.transformer.station.StationSubscriptionSingleTransformer;
import com.server.android.util.Variable;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class WeatherFragmentTemp extends BaseFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        DailyWeatherAdapter.ClickListener,
        HourlyWeatherAdapter.ClickListener,
        HorizontalListView.MotionEventListener{

    public static final String TAG = WeatherFragmentTemp.class.getName().toString();

    private WeatherActivity weatherActivity;
    private StationInfoRequest stationInfoRequest;

    private HourlyWeatherAdapter hourlyWeatherAdapter;
    private DailyWeatherAdapter dailyWeatherAdapter;

    @BindView(R.id.weatherSRL)                      SwipeRefreshLayout weatherSRL;
    @BindView(R.id.weatherCON)                      View weatherCON;
    @BindView(R.id.unSubscribeBTN)                  View unSubscribeBTN;

    @BindView(R.id.hourlyPlaceholderCON)            View hourlyPlaceholderCON;
    @BindView(R.id.dailyPlaceholderCON)             View dailyPlaceholderCON;

    @BindView(R.id.weatherIV)                       ImageView weatherIV;
    @BindView(R.id.placeTXT)                        TextView placeTXT;
    @BindView(R.id.statusTXT)                       TextView statusTXT;
    @BindView(R.id.degreesTXT)                      TextView degreesTXT;
    @BindView(R.id.highestTempTXT)                  TextView highestTempTXT;
    @BindView(R.id.lowestTempTXT)                   TextView lowestTempTXT;
    @BindView(R.id.rainAmountTXT)                   TextView rainAmountTXT;
    @BindView(R.id.speedTXT)                        TextView speedTXT;

    @BindView(R.id.weatherSV)                       NestedScrollView weatherSV;
    @BindView(R.id.hourlyEHGV)                      HorizontalListView hourlyEHGV;
    @BindView(R.id.dailyEHGV)                       ExpandableHeightListView dailyEHGV;

    @State int deviceId;

    public static WeatherFragmentTemp newInstance(int deviceId) {
        WeatherFragmentTemp weatherFragment = new WeatherFragmentTemp();
        weatherFragment.deviceId = deviceId;
        return weatherFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_weather;
    }

    @Override
    public void onViewReady() {
        weatherActivity = (WeatherActivity) getActivity();

        unSubscribeBTN.setOnClickListener(this);
        setUpHourlyWeatherListView();
        setUpDailyWeatherListView();

        hourlyEHGV.setMotionEventListener(this);
        dailyWeatherAdapter.setMotionEventListener(this);
        attemptShow();

    }

    private void displayData(DeviceItem deviceItem){

        weatherActivity.setTitle(deviceItem.name + " (" + deviceItem.code + ")");

        Picasso.with(getContext())
                .load(deviceItem.current_weather.data.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(weatherIV);

        placeTXT.setText(deviceItem.name);
        statusTXT.setText(deviceItem.current_weather.data.status);
        highestTempTXT.setText(deviceItem.current_weather.data.max_temp);
        lowestTempTXT.setText(deviceItem.current_weather.data.min_temp);
        rainAmountTXT.setText(deviceItem.current_weather.data.probability_of_precip);
        speedTXT.setText(deviceItem.current_weather.data.wind_speed + "\n" + deviceItem.current_weather.data.wind_direction);
        degreesTXT.setText(deviceItem.current_weather.data.temperature);

        unSubscribeBTN.setTag(deviceItem.subscription_id);

        hourlyWeatherAdapter.setNewData(deviceItem.daily_weather.data);
        dailyWeatherAdapter.setNewData(weatherActivity.transformDailyWeather(deviceItem.weekly_weather.data));
    }

    private void setUpHourlyWeatherListView() {
        hourlyWeatherAdapter = new HourlyWeatherAdapter(getContext());
        hourlyEHGV.setAdapter(hourlyWeatherAdapter);
    }

    private void setUpDailyWeatherListView() {
        dailyWeatherAdapter = new DailyWeatherAdapter(getContext());
        dailyWeatherAdapter.setOnItemClickListener(this);
        dailyEHGV.setExpanded(true);
        dailyEHGV.setAdapter(dailyWeatherAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.unSubscribeBTN:
                unsubscribe((int) v.getTag());
                break;
        }
    }

    private void unsubscribe(final int id){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog.setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .setPositiveButtonText("Confirm")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptUnSubscribe(id);
                    }
                })
                .setDescription("Are you sure you want to Unsubscribe this device?")
                .setIcon(R.drawable.icon_information)
                .show(getChildFragmentManager(),confirmationDialog.TAG);
    }

    private void attemptUnSubscribe(int subscriptionId){
        StationUnSubscribeRequest unSubscribeRequest = new StationUnSubscribeRequest(getContext());
        unSubscribeRequest
                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Processing request...", false, false))
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameters(Variable.server.key.INCLUDE, "info,station.current_weather,station.location,station.recommendations")
                .addParameters(Variable.server.key.SUBSCRIPTION_ID, subscriptionId)
                .execute();
    }

    private void attemptShow(){

        weatherSRL.setColorSchemeResources(R.color.colorPrimary);
        weatherSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        weatherSRL.setOnRefreshListener(this);

        stationInfoRequest = new StationInfoRequest(getContext());
        stationInfoRequest.addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(weatherSRL)
                .addParameters(Variable.server.key.INCLUDE, "info,daily_weather,current_weather,weekly_weather,location,recommendations")
                .addParameters(Variable.server.key.STATION_ID, deviceId);
    }

    private void refreshList(){
        weatherCON.setVisibility(View.GONE);
        stationInfoRequest
                .showSwipeRefreshLayout(true)
                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Subscribe
    public void onResponse(StationInfoRequest.ServerResponse responseData) {
        StationSingleTransformer stationSingleTransformer = responseData.getData(StationSingleTransformer.class);
        if(stationSingleTransformer.status){
            weatherCON.setVisibility(View.VISIBLE);
            displayData(stationSingleTransformer.data);
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
            weatherActivity.onBackPressed();
        }
        if (hourlyWeatherAdapter.getCount() == 0){
            hourlyPlaceholderCON.setVisibility(View.VISIBLE);
            hourlyEHGV.setVisibility(View.GONE);
        }else {
            hourlyPlaceholderCON.setVisibility(View.GONE);
            hourlyEHGV.setVisibility(View.VISIBLE);
        }
        if (dailyWeatherAdapter.getCount() == 0){
            dailyPlaceholderCON.setVisibility(View.VISIBLE);
        }else {
            dailyPlaceholderCON.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(StationUnSubscribeRequest.ServerResponse responseData) {
        StationSubscriptionSingleTransformer stationSingleTransformer = responseData.getData(StationSubscriptionSingleTransformer.class);
        if(stationSingleTransformer.status){
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            weatherActivity.onBackPressed();
        }else{
            ToastMessage.show(getActivity(), stationSingleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onHourItemClick(WeatherItem weatherItem) {
        weatherActivity.startRecommendationActivity(0, "view");
    }

    @Override
    public void onDailyItemClick(WeatherItem weatherItem) {
        WeatherDetailDialog.newInstance(weatherItem).show(getChildFragmentManager(), WeatherDetailDialog.TAG);
    }

    @Override
    public void onMotionDown() {
//        weatherSV.setScrollingEnabled(false);
    }

    @Override
    public void onMotionUp() {
//        weatherSV.setScrollingEnabled(true);
    }
}
