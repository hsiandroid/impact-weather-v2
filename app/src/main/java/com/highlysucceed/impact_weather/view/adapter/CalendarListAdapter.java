package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarListAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<CalendarData> data;
    private LayoutInflater layoutInflater;
    private String currentMonthName;
    private int currentMonthNum;
    private int currentYear;

	public CalendarListAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        initCalendar();
	}

	private void initCalendar(){
        Calendar calendar = Calendar.getInstance();
        currentMonthNum = calendar.get(Calendar.MONTH);
        currentYear = calendar.get(Calendar.YEAR);
        currentMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        printDays();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar_daily, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.calendarData = data.get(position);

        holder.dayNumTXT.setText(String.valueOf(holder.calendarData.getDayNum()));
        holder.dayNameTXT.setText(holder.calendarData.getDayName());

		return convertView;
	}

	public void nextMonth(){
        if (currentMonthNum > 11) {
            currentMonthNum = 1;
            currentYear++;
        } else {
            currentMonthNum++;
        }
        printDays();
    }

	public void prevMonth(){
        if (currentMonthNum <= 1) {
            currentMonthNum = 12;
            currentYear--;
        } else {
            currentMonthNum--;
        }
        printDays();
    }

    public String getCurrentMonthName(){
        return currentMonthName;
    }

    public int getCurrentYear(){
        return currentYear;
    }

	private void printDays(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH, currentMonthNum);

        currentMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        data.clear();
        data.add(new CalendarData(calendar));

        int numDays = calendar.getActualMaximum(Calendar.DATE);
        for (int i = 1 ; i < numDays; i ++){
            calendar.add(Calendar.DATE, 1);
            data.add(new CalendarData(calendar));
        }

        notifyDataSetChanged();
    }

    public class CalendarData{
        private int dayNum;
        private String dayName;

        public CalendarData(Calendar calendar){
            dayNum = calendar.get(Calendar.DATE);
            dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
        }

        public String getDayName(){
            return dayName;
        }

        public int getDayNum(){
            return dayNum;
        }
    }

    public class ViewHolder{
        CalendarData calendarData;
        @BindView(R.id.dayNumTXT)           TextView dayNumTXT;
        @BindView(R.id.dayNameTXT)          TextView dayNameTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(onDayClickListener != null){
            onDayClickListener.onItemClick("");
        }
    }

    private OnDayClickListener onDayClickListener;

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }

    public interface OnDayClickListener {
        void onItemClick(String timeStamp);
    }
} 
