package com.highlysucceed.impact_weather.view.fragment.recommendation;

import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RecommendationActivity;
import com.server.android.model.RecommendationItem;

import butterknife.BindView;

public class RecommendationDetailFragment extends BaseFragment {

    public static final String TAG = RecommendationDetailFragment.class.getName().toString();

    private RecommendationActivity recommendationActivity;
    private RecommendationItem recommendationItem;

    @BindView(R.id.imageCIV)                ImageView imageCIV;
    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.recommendationCON)       View recommendationCON;

    @BindView(R.id.step1TXT)                TextView step1TXT;
    @BindView(R.id.step2TXT)                TextView step2TXT;
    @BindView(R.id.step3TXT)                TextView step3TXT;
    @BindView(R.id.step4TXT)                TextView step4TXT;
    @BindView(R.id.step5TXT)                TextView step5TXT;

    public static RecommendationDetailFragment newInstance(RecommendationItem recommendationItem) {
        RecommendationDetailFragment recommendationDetailFragment = new RecommendationDetailFragment();
        recommendationDetailFragment.recommendationItem = recommendationItem;
        return recommendationDetailFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_recommendation_detail;
    }

    @Override
    public void onViewReady() {
        recommendationActivity  = (RecommendationActivity) getActivity();
        recommendationActivity.setTitle(recommendationItem.title);
        descriptionTXT.setText(recommendationItem.content);

        switch (recommendationItem.type){
            case "critical":
                recommendationCON.setBackgroundColor(ActivityCompat.getColor(getContext(), R.color.light_red));
                break;
            case "immediate":
                recommendationCON.setBackgroundColor(ActivityCompat.getColor(getContext(), R.color.light_orange));
                break;
            case "low":
                recommendationCON.setBackgroundColor(ActivityCompat.getColor(getContext(), R.color.light_green));
                break;
        }

        step1TXT.setText("1. " + getString(R.string.lorem));
        step2TXT.setText("2. " + getString(R.string.lorem));
        step3TXT.setText("3. " + getString(R.string.lorem));
        step4TXT.setText("4. " + getString(R.string.lorem));
        step5TXT.setText("5. " + getString(R.string.lorem));

    }
}
