package com.highlysucceed.impact_weather.view.fragment.device;

import android.view.View;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.DeviceActivity;
import com.highlysucceed.impact_weather.view.dialog.defaultdialog.InfoDialog;

import butterknife.BindView;

public class SubscribeDeviceFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = SubscribeDeviceFragment.class.getName().toString();

    private DeviceActivity deviceActivity;

    @BindView(R.id.continueBTN)     View continueBTN;
    @BindView(R.id.cancelBTN)       View cancelBTN;

    public static SubscribeDeviceFragment newInstance() {
        SubscribeDeviceFragment subscribeDeviceFragment = new SubscribeDeviceFragment();
        return subscribeDeviceFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_subscribe_device;
    }

    @Override
    public void onViewReady() {
        deviceActivity = (DeviceActivity) getActivity();
        deviceActivity.setTitle("Subscribe to Device");

        continueBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.continueBTN:
                final InfoDialog infoDialog = InfoDialog.Builder();
                infoDialog
                        .setDescription("You will now be redirected to\n" +
                                "our payment portal for you to \n" +
                                "proceed with your subscription.")
                        .setIcon(R.drawable.icon_subscribe)
                        .setButtonText(getString(R.string.payment_continue))
                        .setButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                InfoDialog infoDialog1 = InfoDialog.Builder();
                                infoDialog1
                                        .setDescription("Payment Successful!")
                                        .setIcon(R.drawable.icon_successful)
                                        .setButtonText(getString(R.string.okay))
                                        .setButtonClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                getActivity().finish();
                                            }
                                        })
                                        .build(getChildFragmentManager());
                            }
                        })
                        .build(getChildFragmentManager());
                break;
            case R.id.cancelBTN:
                getActivity().finish();
                break;
        }
    }
}
