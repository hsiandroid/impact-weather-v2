package com.highlysucceed.impact_weather.view.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import icepick.State;

public class MeteogramDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = MeteogramDialog.class.getName().toString();

	@BindView(R.id.imageIV)         ImageView imageIV;
	@BindView(R.id.mainIconIV)		View mainIconIV;
	@BindView(R.id.mainTitleTXT)	TextView mainTitleTXT;

	@State String path;
	@State String code;

	public static MeteogramDialog newInstance(String code, String path) {
		MeteogramDialog fragment = new MeteogramDialog();
		fragment.path = path;
		fragment.code = code;
		return fragment;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_meteograph;
	}

	@Override
	public void onViewReady() {
		mainTitleTXT.setText(code);
		mainIconIV.setOnClickListener(this);
		Picasso.with(getContext())
				.load(path)
				.into(imageIV);

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.mainIconIV:
				dismiss();
				break;
		}
	}
}
