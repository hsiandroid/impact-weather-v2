package com.highlysucceed.impact_weather.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.WeatherItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HourlyWeatherRecycleViewAdapter extends RecyclerView.Adapter<HourlyWeatherRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener {

	private Context context;
	private List<WeatherItem> data;
    private LayoutInflater layoutInflater;

	public HourlyWeatherRecycleViewAdapter(Context context) {
		this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<WeatherItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_hourly_weather, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.view.setTag(position);
        holder.view.setOnClickListener(this);

        holder.weatherItem = data.get(position);

        holder.timeTXT.setText(holder.weatherItem.read_hr);
        holder.temperatureTXT.setText(holder.weatherItem.temperature + "C");
        holder.rainAmountTXT.setText(holder.weatherItem.probability_of_precip);
        holder.speedTXT.setText(holder.weatherItem.wind_speed);
        holder.statusTXT.setText("(" + holder.weatherItem.status + ")");

        Picasso.with(context)
                .load(holder.weatherItem.image)
                .placeholder(R.drawable.icon_weather)
                .error(R.drawable.icon_weather)
                .into(holder.weatherIV);

        int precip = holder.weatherItem.precip_percentage;

        if(precip <= 49){
                holder.rainAmountTXT.setText("");
                holder.rainAmountTXT.setBackgroundResource(R.color.tranparent);
//            holder.rainAmountTXT.setVisibility(View.GONE);
        }else if(precip >= 50 && precip < 75){
            holder.rainAmountTXT.setBackgroundResource(R.color.deep_orange);
        }else{
            holder.rainAmountTXT.setBackgroundResource(R.color.red);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        WeatherItem weatherItem;
        @BindView(R.id.timeTXT)         TextView timeTXT;
        @BindView(R.id.weatherIV)       ImageView weatherIV;
        @BindView(R.id.temperatureTXT)  TextView temperatureTXT;
        @BindView(R.id.rainAmountTXT)   TextView rainAmountTXT;
        @BindView(R.id.speedTXT)        TextView speedTXT;
        @BindView(R.id.statusTXT)       TextView statusTXT;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onHourItemClick(data.get((int) v.getTag()));
        }
    }

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onHourItemClick(WeatherItem weatherItem);
    }
} 
