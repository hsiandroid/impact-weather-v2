package com.highlysucceed.impact_weather.view.fragment.registration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ForgotPasswordFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;


    @BindView(R.id.supportEmailBTN)         View supportBTN;


    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        return forgotPasswordFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getActivity();
        registrationActivity.setTitle("Forgot Password?");
        registrationActivity.showTitleBar(true);
        supportBTN.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.supportEmailBTN:
//                attemptRequestVerification();
                emailSupport();
                break;
        }
    }


    public void emailSupport(){
        String email = "support@highlysucceed.com";
        String subject = "[REQUEST] Password Reset";
        Spanned body = Html.fromHtml(getString(R.string.body));

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }



}
