package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.ActivityItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySpinnerAdapter extends ArrayAdapter {
	private Context context;
	private List<ActivityItem> data;
    private LayoutInflater layoutInflater;

	public ActivitySpinnerAdapter(Context context) {
        super(context, R.layout.adapter_crop_spinner);
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<ActivityItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<ActivityItem> data){
        for(ActivityItem farmItem : data){
            this.data.add(farmItem);
        }
        notifyDataSetChanged();
    }

    public ActivityItem getItemByPosition(int position){
        return data.get(position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.adapter_crop_spinner, parent, false);

        TextView spinner = (TextView) convertView.findViewById(R.id.spinnerTXT);
        spinner.setText(data.get(position).title);
        spinner.setTextSize(14);
        return spinner;
    }

    @Override
    public int getCount()  {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_crop_spinner, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.activityItem = data.get(position);
        holder.spinnerTXT.setText(holder.activityItem.title);
        holder.spinnerTXT.setTextSize(16);
		return convertView;
	}

	public class ViewHolder{
        ActivityItem activityItem;

        @BindView(R.id.spinnerTXT)  TextView spinnerTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
