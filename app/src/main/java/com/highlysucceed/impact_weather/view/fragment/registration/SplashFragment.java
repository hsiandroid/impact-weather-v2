package com.highlysucceed.impact_weather.view.fragment.registration;

import android.content.pm.PackageManager;
import android.util.Log;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;
import com.highlysucceed.impact_weather.view.dialog.VersionControlDialog;
import com.server.android.request.auth.AppSettingsRequest;
import com.server.android.request.auth.RefreshTokenRequest;
import com.server.android.transformer.AppSettingsTransformer;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SplashFragment extends BaseFragment implements VersionControlDialog.ClickListener{

    public static final String TAG = SplashFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    public static SplashFragment newInstance() {
        SplashFragment splashFragment = new SplashFragment();
        return splashFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.showTitleBar(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        attemptGetSettings();
    }


    private void attemptRefreshToken() {
        if(UserData.getBoolean(UserData.TOKEN_EXPIRED)){
            RefreshTokenRequest refreshTokenRequest = new RefreshTokenRequest(getContext());
            refreshTokenRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Variable.server.key.INCLUDE, "info,statistics,social")
                    .addParameters(Variable.server.key.REFRESH_TOKEN, UserData.REFRESH_TOKEN)
                    .addParameters(Variable.server.key.CLIENT_ID, Variable.server.client.ID)
                    .addParameters(Variable.server.key.CLIENT_SECRET, Variable.server.client.SECRET)
                    .showNoInternetConnection(false)
                    .execute();
        }else if(!UserData.getString(UserData.AUTHORIZATION).equals("")){
            authenticated();
        }else{
            showLogin();
        }
    }

    private void attemptGetSettings() {
        new AppSettingsRequest(getContext())
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RefreshTokenRequest.ServerResponse responseData) {
        UserSingleTransformer baseTransformer = responseData.getData(UserSingleTransformer.class);
        if (baseTransformer.status) {
            UserData.insert(UserData.AUTHORIZATION, baseTransformer.token.access_token);
            UserData.insert(UserData.REFRESH_TOKEN, baseTransformer.token.access_token);
            UserData.insert(baseTransformer.data);
            authenticated();
        } else {
            showLogin();
        }
    }

    private void authenticated(){
        registrationActivity.startMainActivity();
        UserData.insert(UserData.TOKEN_EXPIRED, false);
        ToastMessage.show(getActivity(), "Welcome Back " + UserData.getUserItem().name, ToastMessage.Status.SUCCESS);
    }

    private void showLogin(){
        registrationActivity.openLoginFragment();
    }

    @Subscribe
    public void onResponse(AppSettingsRequest.ServerResponse responseData) {

        AppSettingsTransformer appSettingsTransformer = responseData.getData(AppSettingsTransformer.class);
        if (appSettingsTransformer.status) {
            UserData.insert(appSettingsTransformer.data);
            UserData.insert(appSettingsTransformer.ads);
            int appVersion = 0;
            try {
                appVersion = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (appVersion < appSettingsTransformer.data.latest_version.major_version) {
//                Log.e("version", "required");
                VersionControlDialog.newInstance(true, this).show(getChildFragmentManager(), VersionControlDialog.TAG);
            } else if (appVersion < appSettingsTransformer.data.latest_version.minor_version) {
//                Log.e("version", "not required");
                VersionControlDialog.newInstance(false, this).show(getChildFragmentManager(), VersionControlDialog.TAG);
            } else {
                attemptRefreshToken();
                Log.e("Refresh", ">>>true");
            }
        } else {
            registrationActivity.openLoginFragment();
        }
    }

    @Override
    public void later(VersionControlDialog dialogFragment) {
        attemptRefreshToken();
        dialogFragment.dismiss();
    }

    @Override
    public void update(VersionControlDialog dialogFragment) {
        dialogFragment.openGooglePlayStore();
        dialogFragment.dismiss();
    }
}
