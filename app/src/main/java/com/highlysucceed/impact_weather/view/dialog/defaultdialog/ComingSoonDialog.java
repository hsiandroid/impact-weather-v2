package com.highlysucceed.impact_weather.view.dialog.defaultdialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.dialog.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class ComingSoonDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = ComingSoonDialog.class.getName().toString();

    @BindView(R.id.iconIV)                  ImageView iconIV;
    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.positiveBTN)             TextView positiveBTN;

	public static ComingSoonDialog Builder() {
		ComingSoonDialog infoDialog = new ComingSoonDialog();
		return infoDialog;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_coming_soon;
	}

	@Override
	public void onViewReady() {
      positiveBTN.setOnClickListener(this);
      iconIV.setImageResource(R.drawable.icon_maintenance);
      descriptionTXT.setText("Coming Soon");
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogCustomSize(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.positiveBTN:
                dismiss();
                break;
        }
    }
}


