package com.highlysucceed.impact_weather.view.fragment.registration;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.highlysucceed.impact_weather.data.CountryData;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.view.dialog.CountryDialog;
import com.server.android.request.GetCountryRequest;
import com.server.android.request.auth.RegistrationRequest;
import com.server.android.transformer.GetCountryTransformer;
import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;
import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.RegistrationActivity;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import icepick.State;

public class SignUpFragment extends BaseFragment implements View.OnClickListener, CountryDialog.CountryPickerListener{

    public static final String TAG = SignUpFragment.class.getName().toString();
    public RegistrationActivity registrationActivity;

    @BindView(R.id.nameET)              EditText nameET;
    @BindView(R.id.contactET)           EditText contactET;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.signUpTV)            TextView signUpTV;
    @BindView(R.id.countryFlagIV)       ImageView countryFlagIV;
    @BindView(R.id.contactCodeTXT)      TextView contactCodeTXT;
    @BindView(R.id.contactCON)          View contactCON;

    @State String country;

    private CountryData.Country countryData;

    @State  String contact;

    public static SignUpFragment newInstance(){
        SignUpFragment signUpFragment = new SignUpFragment();
        return signUpFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        registrationActivity.setTitle("REGISTRATION FORM");
        registrationActivity.showTitleBar(true);
        signUpTV.setOnClickListener(this);
        contactCON.setOnClickListener(this);
        onMaskedTextChangedListener(contactET);
        getCountry();
    }

    public void onMaskedTextChangedListener(EditText editText){
        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "[000] [000] [0000]",
                true,
                editText,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {

                    }
                });
        editText.addTextChangedListener(listener);
        editText.setOnFocusChangeListener(listener);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpTV:
                attemptSignUp();
                break;
            case R.id.contactCON:
                showCountryPicker();
                break;
        }
    }

    private void attemptSignUp(){
        contact = contactET.getText().toString();
        final Mask mask = new Mask("[000] [000] [0000]");
        final Mask.Result result = mask.apply(
                new CaretString(
                        contact,
                        contact.length()
                ),
                true
        );

        final String contact1 = contactCodeTXT.getText().toString() + result.getExtractedValue();
        RegistrationRequest registrationRequest = new RegistrationRequest(getContext());
        registrationRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Signing up...", false, false))
                .addParameters(Variable.server.key.NAME, nameET.getText().toString())
                .addParameters(Variable.server.key.CONTACT, contact1)
                .addParameters(Variable.server.key.PASSWORD, passwordET.getText().toString())
                .addParameters(Variable.server.key.EMAIL, emailET.getText().toString())
                .addParameters(Variable.server.key.CLIENT_ID, Variable.server.client.ID)
                .addParameters(Variable.server.key.CLIENT_SECRET, Variable.server.client.SECRET)
                .execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(RegistrationRequest.ServerResponse responseData) {
        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
        if(userSingleTransformer.status){
            UserData.insert(userSingleTransformer.data);
            registrationActivity.startMainActivity();
        }
        else{
            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
            if(userSingleTransformer.hasRequirements()){
                ErrorResponseManger.first(nameET, userSingleTransformer.requires.name);
                ErrorResponseManger.first(emailET, userSingleTransformer.requires.email);
                ErrorResponseManger.first(contactET, userSingleTransformer.requires.contact);
            }
        }
    }

    @Subscribe
    public void onResponse(GetCountryRequest.ServerResponse responseData) {
        GetCountryTransformer getCountryTransformer = responseData.getData(GetCountryTransformer.class);
        if(getCountryTransformer.status){
            countryData = CountryData.getCountryDataByCode1(getCountryTransformer.data.country);
            contactCodeTXT.setText(countryData.code3);
            countryCode(countryData);
        }else{
            ToastMessage.show(getContext(), getCountryTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

    private void getCountry(){
        GetCountryRequest createCommentRequest = new GetCountryRequest(getContext());
        createCommentRequest
                .showNoInternetConnection(false)
                .execute();
    }

    private void showCountryPicker(){
        CountryDialog.newInstance(this).show(getChildFragmentManager(), TAG);
    }


    private void countryCode(CountryData.Country country){
        contactCodeTXT.setText(country.code3);
        Glide.with(getContext())
                .load(country.getFlag())
                .into(countryFlagIV);
    }

    @Override
    public void onSelectCountry(CountryData.Country country, int requestCode) {
        countryData = country;
        countryCode(countryData);
    }
}
