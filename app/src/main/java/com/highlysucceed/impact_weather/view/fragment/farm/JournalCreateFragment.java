package com.highlysucceed.impact_weather.view.fragment.farm;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.data.UserData;
import com.highlysucceed.impact_weather.util.lib.DateFormatter;
import com.highlysucceed.impact_weather.util.lib.ToastMessage;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.FarmActivity;
import com.highlysucceed.impact_weather.view.adapter.ActivitySpinnerAdapter;
import com.highlysucceed.impact_weather.view.dialog.TimeDialog;
import com.server.android.model.ActivityItem;
import com.server.android.model.JournalItem;
import com.server.android.request.journals.JournalsCreateRequest;
import com.server.android.request.journals.JournalsEditRequest;
import com.server.android.transformer.journals.JournalsSingleTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.ErrorResponseManger;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class JournalCreateFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = JournalCreateFragment.class.getName().toString();
    public static final String TIME_FORMAT = "hh:mm a";
    public static final int MINIMUM_TIME = 2;

    private FarmActivity farmActivity;
    private ActivitySpinnerAdapter activitySpinnerAdapter;
    private JournalItem journalItem;

    @BindView(R.id.startDateTXT)        TextView startDateTXT;
    @BindView(R.id.startTimeTXT)        TextView startTimeTXT;
    @BindView(R.id.endTimeTXT)          TextView endTimeTXT;
    @BindView(R.id.quantityET)          TextView quantityET;
    @BindView(R.id.brandET)             TextView brandET;
    @BindView(R.id.activitySPR)         Spinner activitySPR;
    @BindView(R.id.allDaySW)            Switch allDaySW;
    @BindView(R.id.startTimeCON)        View startTimeCON;
    @BindView(R.id.endTimeCON)          View endTimeCON;
    @BindView(R.id.saveBTN)             View saveBTN;

    @State int farmId;
    @State String crop;
    @State String startDate;

    public static JournalCreateFragment newInstance(int farmId, String crop, String startDate) {
        JournalCreateFragment fragment = new JournalCreateFragment();
        fragment.farmId = farmId;
        fragment.crop = crop;
        fragment.startDate = startDate;
        fragment.journalItem = new JournalItem();
        return fragment;
    }

    public static JournalCreateFragment newInstance(JournalItem journalItem) {
        JournalCreateFragment fragment = new JournalCreateFragment();
        fragment.journalItem = journalItem;
        fragment.crop = journalItem.crop;
        fragment.startDate = journalItem.entry_date;
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_journal_create;
    }

    @Override
    public void onViewReady() {
        farmActivity = (FarmActivity) getActivity();
        startDateTXT.setText(startDate);
        allDaySW.setOnClickListener(this);
        saveBTN.setOnClickListener(this);
        startTimeTXT.setOnClickListener(this);
        endTimeTXT.setOnClickListener(this);
        setUpSPR();
        allDay();
        setUpTime();
        displayData();

        Log.e("token", UserData.getString(UserData.AUTHORIZATION));
    }

    private void displayData(){
        if(journalItem.id > 0){
            startDateTXT.setText(DateFormatter.format(journalItem.entry_date_db, "yyyy-MM-dd", "MMMM dd, yyyy"));
            quantityET.setText(String.valueOf(journalItem.qty));
            brandET.setText(journalItem.brand);

            if(journalItem.start_time.equals("12:00 AM") && journalItem.end_time.equals("12:00 AM")){
                allDaySW.setChecked(true);
            }else{
                allDaySW.setChecked(false);
                startTimeTXT.setText(journalItem.start_time);
                endTimeTXT.setText(journalItem.end_time);
            }
            allDay();

            List<ActivityItem> items = UserData.getActivityItems();
            for(ActivityItem item : items){
                if(item.title.equals(journalItem.title)){
                    activitySPR.setSelection(items.indexOf(item));
                    break;
                }
            }
        }
    }

    private void setUpTime(){
        Date date = new Date();
        startTimeTXT.setText(getTimeFormat().format(date.getTime()));
        endTimeTXT.setText(getTimeFormat().format(addTime(date, MINIMUM_TIME).getTime()));
    }

    private void openStartTimePicker(){
        TimeDialog.newInstance(
                new TimeDialog.TimePickerListener() {
                    @Override
                    public void forTimeDisplay(String time) {
                        startTimeTXT.setText(time);
                        fixEndTime();
                    }
                },
                startTimeTXT.getText().toString()
        ).show(getChildFragmentManager(), TimeDialog.TAG);
    }

    private void openEndTimePicker(){
        TimeDialog.newInstance(
                new TimeDialog.TimePickerListener() {
                    @Override
                    public void forTimeDisplay(String time) {
                        endTimeTXT.setText(time);
                        fixStartTime();
                    }
                },
                endTimeTXT.getText().toString()
        ).show(getChildFragmentManager(), TimeDialog.TAG);
    }

    private void fixEndTime(){
        Date startDate = getDate(startTimeTXT.getText().toString());
        Date endDate = getDate(endTimeTXT.getText().toString());

        if(startDate.getTime() > endDate.getTime()){
            endTimeTXT.setText(getTimeFormat().format(addTime(startDate, MINIMUM_TIME).getTime()));
        }
    }

    private void fixStartTime(){
        Date startDate = getDate(startTimeTXT.getText().toString());
        Date endDate = getDate(endTimeTXT.getText().toString());

        if(startDate.getTime() > endDate.getTime()){
            startTimeTXT.setText(getTimeFormat().format(addTime(endDate, -MINIMUM_TIME).getTime()));
        }
    }

    private Date addTime(Date date, int additional){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, additional);
        return calendar.getTime();
    }

    private Date getDate(String date){
        try {
            return getTimeFormat().parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    private SimpleDateFormat getTimeFormat(){
        return new SimpleDateFormat(TIME_FORMAT);
    }

    private void setUpSPR(){
        activitySpinnerAdapter = new ActivitySpinnerAdapter(getContext());
        activitySpinnerAdapter.setNewData(UserData.getActivityItems());
        activitySPR.setAdapter(activitySpinnerAdapter);
        activitySPR.getBackground().setColorFilter(ActivityCompat.getColor(getContext(),R.color.white), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.allDaySW:
                allDay();
                break;
            case R.id.saveBTN:
                attemptCreate();
                break;
            case R.id.startTimeTXT:
                openStartTimePicker();
                break;
            case R.id.endTimeTXT:
                openEndTimePicker();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(JournalsCreateRequest.ServerResponse responseData) {
        JournalsSingleTransformer journalsSingleTransformer = responseData.getData(JournalsSingleTransformer.class);
        if(journalsSingleTransformer.status){
            ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            farmActivity.onBackPressed();
        }else{
            ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.FAILED);
            if(journalsSingleTransformer.hasRequirements()){
                ErrorResponseManger.first(startDateTXT, journalsSingleTransformer.requires.entry_date);
                ErrorResponseManger.first(quantityET, journalsSingleTransformer.requires.qty);
                ErrorResponseManger.first(brandET, journalsSingleTransformer.requires.brand);
            }
        }
    }

    @Subscribe
    public void onResponse(JournalsEditRequest.ServerResponse responseData) {
        JournalsSingleTransformer journalsSingleTransformer = responseData.getData(JournalsSingleTransformer.class);
        if(journalsSingleTransformer.status){
            ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.SUCCESS);
            farmActivity.onBackPressed();
        }else{
            ToastMessage.show(getActivity(), journalsSingleTransformer.msg, ToastMessage.Status.FAILED);
            if(journalsSingleTransformer.hasRequirements()){
                ErrorResponseManger.first(startDateTXT, journalsSingleTransformer.requires.entry_date);
                ErrorResponseManger.first(quantityET, journalsSingleTransformer.requires.qty);
                ErrorResponseManger.first(brandET, journalsSingleTransformer.requires.brand);
            }
        }
    }

    private void attemptCreate(){

        String start = "12:00 AM";
        String end = "12:00 AM";

        if(!allDaySW.isChecked()){
            start = startTimeTXT.getText().toString();
            end = endTimeTXT.getText().toString();
        }
        BaseRequest baseRequest = null;

//        if(brandET.getText().toString().equalsIgnoreCase("") || quantityET.getText().toString().equalsIgnoreCase("")){
//            if (brandET.getText().toString().equalsIgnoreCase("")){
//                brandET.setError("Field is required!");
//            }else if (quantityET.getText().toString().equalsIgnoreCase("")){
//                quantityET.setError("Field is required");
//            }
//        }else {
            if(journalItem.id > 0){
                baseRequest = new JournalsEditRequest(getContext())
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating journal...", false, false))
                        .addParameters(Variable.server.key.JOURNAL_ID, journalItem.id);
            }else{
                baseRequest = new JournalsCreateRequest(getContext())
                        .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Creating journal...", false, false))
                        .addParameters(Variable.server.key.FARM_ID, farmId);
            }

            if (!brandET.getText().toString().equalsIgnoreCase("")){
              baseRequest
                    .addParameters(Variable.server.key.BRAND, brandET.getText().toString());
            }

            if (!quantityET.getText().toString().equalsIgnoreCase("")){
              baseRequest
                    .addParameters(Variable.server.key.QTY, quantityET.getText().toString());
            }else {
                baseRequest
                        .addParameters(Variable.server.key.QTY, "1");
            }

            Log.e("JournalFarmID",">>>" + farmId);
            Log.e("JournalCrop",">>>" + crop);
            Log.e("JournalTitle",">>>" + ((ActivityItem) activitySPR.getSelectedItem()).title);
            Log.e("JournalStatus",">>>" + "low");
            Log.e("JournalEntryDate",">>>" + startDateTXT.getText().toString());
            Log.e("JournalStartTime",">>>" + DateFormatter.format(start, "hh:mm a", "HH:mm"));
            Log.e("JournalEndTime",">>>" + DateFormatter.format(end, "hh:mm a", "HH:mm"));
            Log.e("JournalQuantity",">>>" + quantityET.getText().toString());
            Log.e("JournalBrand",">>>" + brandET.getText().toString());

            baseRequest
                    .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                    .addParameters(Variable.server.key.CROP, crop)
                    .addParameters(Variable.server.key.TITLE, ((ActivityItem) activitySPR.getSelectedItem()).title)
                    .addParameters(Variable.server.key.STATUS, "low")
                    .addParameters(Variable.server.key.ENTRY_DATE, startDateTXT.getText().toString())
                    .addParameters(Variable.server.key.START_TIME, DateFormatter.format(start, "hh:mm a", "HH:mm"))
                    .addParameters(Variable.server.key.END_TIME, DateFormatter.format(end, "hh:mm a", "HH:mm"))
                    .execute();
//        }

    }

    private void allDay(){
        if(allDaySW.isChecked()){
            startTimeCON.setVisibility(View.GONE);
            endTimeCON.setVisibility(View.GONE);
        }else{
            startTimeCON.setVisibility(View.VISIBLE);
            endTimeCON.setVisibility(View.VISIBLE);
        }
    }
}
