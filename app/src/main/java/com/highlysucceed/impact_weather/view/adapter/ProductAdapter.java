package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.server.android.model.CropItem;
import com.highlysucceed.impact_weather.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<CropItem> data;
    private LayoutInflater layoutInflater;

	public ProductAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<CropItem> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_product, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.cropItem = data.get(position);
        holder.view.setOnClickListener(this);

        holder.productTXT.setText(holder.cropItem.name);
        if(holder.cropItem.variety != null && holder.cropItem.variety.length() > 0){
            holder.varietyTXT.setText("(" + holder.cropItem.variety + ")");
        }else{
            holder.varietyTXT.setText("");
        }

		return convertView;
	}

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onCropClickListener(viewHolder.cropItem);
        }
    }

    public class ViewHolder{
        CropItem cropItem;

        @BindView(R.id.productTXT)      TextView productTXT;
        @BindView(R.id.varietyTXT)      TextView varietyTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    private ClickListener clickListener;

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onCropClickListener(CropItem cropItem);
    }
} 
