package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.JournalItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JournalCalendarMonthlyAdapter extends BaseAdapter implements View.OnClickListener {
    private Context context;
    private List<CalendarData> data;
    private LayoutInflater layoutInflater;
    private String currentMonthName;
    private int currentMonthNum;
    private int currentYear;

    private int actualDay;
    private int actualMonth;
    private String actualMonthName;
    private int actualYear;
    private boolean isActualMonth;

    public JournalCalendarMonthlyAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        initCalendar();
    }

    private void initCalendar(){
        Calendar calendar = Calendar.getInstance();
        actualMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        actualMonth = calendar.get(Calendar.MONTH);
        actualYear = calendar.get(Calendar.YEAR);
        actualDay = calendar.get(Calendar.DATE);

        currentMonthNum = actualMonth;
        currentYear = actualYear;
        currentMonthName = actualMonthName;

        isActualMonth = currentMonthNum == actualMonth;
        printDays();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar_monthly, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.calendarData = data.get(position);
        holder.calendarData.position = position;
        holder.dayTXT.setText(String.valueOf(holder.calendarData.getDayNum()));

        if(holder.calendarData.isOffsetFirst || holder.calendarData.isOffsetLast ){
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white_dark_gray));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }else if(isActualMonth() && isActualYear() && holder.calendarData.getDayNum() == getActualDay()){
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }else{
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }



        if (holder.calendarData.selected) {
            holder.dayTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.selector_calendar_selected));
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
        }

        holder.activityCON.setVisibility(holder.calendarData.getJournalAdapter() != null && holder.calendarData.getJournalAdapter().getCount() > 0 ? View.VISIBLE : View.GONE);

        holder.dayTXT.setTag(holder.calendarData);
        holder.dayTXT.setOnClickListener(this);

        return convertView;
    }

    public void nextMonth(){
        // 0 = January
        // 11 = December
        if(currentMonthNum == 11){
            currentMonthNum = 0;
            currentYear++;
        }else{
            currentMonthNum++;
        }
        printDays();
    }

    public void prevMonth(){
        if(currentMonthNum == 0){
            currentMonthNum = 11;
            currentYear--;
        }else{
            currentMonthNum--;
        }
        printDays();
    }

    public boolean isActualYear(){
        return currentYear == actualYear;
    }

    public void setCurrentMonthNum(int currentMonthNum) {
        this.currentMonthNum = currentMonthNum;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public void refresh(){
        printDays();
    }

    public String getCurrentMonthName(){
        return currentMonthName;
    }

    public int getCurrentMonthNum() {
        return currentMonthNum;
    }

    public int getCurrentYear(){
        return currentYear;
    }

    public boolean isActualMonth(){
        return isActualMonth;
    }

    public int getActualDay(){
        return actualDay;
    }

    public CalendarData getCurrentDayData(){
        for(CalendarData calendarData : data){
            if(calendarData.dayNum == getActualDay()){
                return calendarData;
            }
        }
        return null;
    }

    public CalendarData getSelectedData(){
        for(CalendarData calendarData : data){
            if(calendarData.selected){
                return calendarData;
            }
        }
        return null;
    }

    public int getActualDayPosition(){
        return actualDay - 1;
    }

    private void printDays(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH, currentMonthNum);

        currentMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        data.clear();

        int numDays = calendar.getActualMaximum(Calendar.DATE);
        int currentWeekDay = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.set(Calendar.DATE, - currentWeekDay);


        for (int i = 1 ; i <= currentWeekDay; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = true;
            calendarData.isOffsetLast = false;
            calendarData.isDays = false;
            calendarData.selected = false;
            data.add(calendarData);
        }

        for (int i = 1 ; i <= numDays; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = false;
            calendarData.isOffsetLast = false;
            calendarData.isDays = true;
            calendarData.selected = actualDay == i;
            data.add(calendarData);
        }

        currentWeekDay = 7 - calendar.get(Calendar.DAY_OF_WEEK);

        for (int i = 1 ; i <= currentWeekDay; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = false;
            calendarData.isOffsetLast = true;
            calendarData.isDays = false;
            calendarData.selected = false;
            data.add(calendarData);
        }

        notifyDataSetChanged();
    }

    public void addJournalsInDay(String day, List<JournalItem> journalItems){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(day)){
                calendarData.setJournalItems(journalItems);
                break;
            }
        }
    }

    public void addJournalInDay(String day, JournalItem journalItem){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(day)){
                calendarData.setJournalItem(journalItem);
                break;
            }
        }
    }

    public void removeJournalItem(JournalItem journalItem){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(journalItem.getChecker())){
                calendarData.getJournalAdapter().removeItem(journalItem);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateCalendarData(){
        notifyDataSetChanged();
    }

    public class CalendarData{
        private int dayNum;
        private String dayName;
        private int monthNum;
        private String monthName;
        private int year;
        private String dateChecker;
        private String formattedDate;
        public boolean isOffsetFirst;
        public boolean isOffsetLast;
        public boolean isDays;
        public boolean selected;
        int position;

        //        private List<JournalItem> journalItems;
        private JournalAdapter journalAdapter;

        public CalendarData(Calendar calendar){
            dayNum = calendar.get(Calendar.DATE);
            dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            monthNum = calendar.get(Calendar.MONTH);
            monthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
            year = calendar.get(Calendar.YEAR);
            dateChecker = String.format("%04d-%02d-%02d", year, monthNum + 1, dayNum);
            formattedDate = monthName + " " + dayNum + ", " + year;
        }

        public String getDayName(){
            return dayName;
        }

        public int getDayNum(){
            return dayNum;
        }

        public void setJournalItems(List<JournalItem> journalItems){
            if(journalAdapter == null){
                this.journalAdapter = new JournalAdapter(context);
            }
            this.journalAdapter.addNewData(journalItems);
        }

        public void setJournalItem(JournalItem journalItem){
            if(journalAdapter == null){
                this.journalAdapter = new JournalAdapter(context);
            }
            this.journalAdapter.addItem(journalItem);
        }

        public JournalAdapter getJournalAdapter(){
            return journalAdapter;
        }

        public JournalAdapter getJournalAdapter(JournalAdapter.ClickListener clickListener){
            journalAdapter.setOnItemClickListener(clickListener);
            return journalAdapter;
        }

        public String getDateChecker(){
            return dateChecker;
        }

        public String getFormattedDate(){
            return formattedDate;
        }
    }

    public class ViewHolder{
        CalendarData calendarData;
        @BindView(R.id.dayTXT)           TextView dayTXT;
        @BindView(R.id.activityCON)      View activityCON;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dayTXT:
                CalendarData calendarData = (CalendarData) v.getTag();
                setSelected(calendarData.position);
                if(onDayClickListener != null){
                    onDayClickListener.onItemClick(calendarData);
                }
                break;
        }
    }

    public void setSelected(int position){
        for (CalendarData calendarData : data) {
            calendarData.selected = data.indexOf(calendarData) == position && calendarData.isDays;
        }
        notifyDataSetChanged();
    }

    public void setSelectedDay(CalendarData calendarData){
        for (CalendarData item : data) {
            item.selected = item.dayNum == calendarData.dayNum && item.isDays;
        }
        notifyDataSetChanged();
    }

    private OnDayClickListener onDayClickListener;

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }

    public interface OnDayClickListener {
        void onItemClick(CalendarData calendarData);
    }
} 
