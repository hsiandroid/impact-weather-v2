package com.highlysucceed.impact_weather.view.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.widget.ExpandableHeightListView;
import com.server.android.model.JournalItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JournalCalendarDailyAdapter extends BaseAdapter implements View.OnClickListener, JournalAdapter.ClickListener {
	private Context context;
	private List<CalendarData> data;
    private LayoutInflater layoutInflater;
    private String currentMonthName = "";
    private int currentMonthNum;
    private int currentYear;

    private int actualDay;
    private int actualMonth;
    private String actualMonthName;
    private int actualYear;
    private boolean isActualMonth;

	public JournalCalendarDailyAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        initCalendar();
	}

	private void initCalendar(){
        Calendar calendar = Calendar.getInstance();
        actualMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        actualMonth = calendar.get(Calendar.MONTH);
        actualYear = calendar.get(Calendar.YEAR);
        actualDay = calendar.get(Calendar.DATE);

        currentMonthNum = actualMonth;
        currentYear = actualYear;
        currentMonthName = actualMonthName;

        isActualMonth = currentMonthNum == actualMonth;
        printDays();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar_daily, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.calendarData = data.get(position);
        holder.messageTXT.setTag(holder.calendarData);
        holder.messageTXT.setOnClickListener(this);
        holder.addBTN.setTag(holder.calendarData);
        holder.addBTN.setOnClickListener(this);

        holder.dayNumTXT.setText(String.valueOf(holder.calendarData.getDayNum()));
        holder.dayNameTXT.setText(holder.calendarData.getDayName());

        if(isActualMonth() && holder.calendarData.getDayNum() == getActualDay()){
            holder.dayNumTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            holder.dayNameTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
        }else{
            holder.dayNumTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
            holder.dayNameTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
        }

        if(holder.calendarData.getJournalAdapter() != null && holder.calendarData.getJournalAdapter().getCount() > 0){
            holder.journalEHLV.setVisibility(View.VISIBLE);
            holder.journalEHLV.setExpanded(true);
            holder.journalEHLV.setAdapter(holder.calendarData.getJournalAdapter(this));
            holder.messageTXT.setVisibility(View.INVISIBLE);
            holder.messageTXT.setOnClickListener(null);
            holder.addBTN.setOnClickListener(this);
        }else{
            holder.journalEHLV.setVisibility(View.GONE);
            holder.messageTXT.setVisibility(View.VISIBLE);
            holder.messageTXT.setOnClickListener(this);
            holder.addBTN.setOnClickListener(this);
        }
		return convertView;
	}

	public void nextMonth(){
        // 0 = January
        // 11 = December
        if(currentMonthNum == 11){
            currentMonthNum = 0;
            currentYear++;
        }else{
            currentMonthNum++;
        }
        printDays();
    }

	public void prevMonth(){
        if(currentMonthNum == 0){
            currentMonthNum = 11;
            currentYear--;
        }else{
            currentMonthNum--;
        }
        printDays();
    }

    public void setCurrentMonthNum(int currentMonthNum) {
        this.currentMonthNum = currentMonthNum;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public void refresh(){
        printDays();
    }

    public String getCurrentMonthName(){
	    if (!currentMonthName.equalsIgnoreCase("")){
            return currentMonthName;
        }
        return "";
    }

    public int getCurrentMonthNum() {
        return currentMonthNum;
    }

    public int getCurrentYear(){
        return currentYear;
    }

    public boolean isActualMonth(){
        return isActualMonth;
    }

    public int getActualDay(){
        return actualDay;
    }

    public CalendarData getCurrentDayData(){
        for(CalendarData calendarData : data){
            if(calendarData.dayNum == getActualDay()){
                return calendarData;
            }
        }
        return null;
    }

    public int getActualDayPosition(){
        return actualDay - 1;
    }

	private void printDays(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH, currentMonthNum);

        currentMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        isActualMonth = currentMonthNum == actualMonth;

        data.clear();
        data.add(new CalendarData(calendar));

        int numDays = calendar.getActualMaximum(Calendar.DATE);
        for (int i = 1 ; i < numDays; i ++){
            calendar.add(Calendar.DATE, 1);
            data.add(new CalendarData(calendar));
        }

        notifyDataSetChanged();
    }

    public void addJournalsInDay(String day, List<JournalItem> journalItems){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(day)){
                calendarData.setJournalItems(journalItems);
                break;
            }
        }
    }

    public void addJournalInDay(String day, JournalItem journalItem){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(day)){
                calendarData.setJournalItem(journalItem);
                break;
            }
        }
    }

    public void removeJournalItem(JournalItem journalItem){
        for(CalendarData calendarData : data){
            if(calendarData.getDateChecker().equals(journalItem.getChecker())){
                calendarData.getJournalAdapter().removeItem(journalItem);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateCalendarData(){
        notifyDataSetChanged();
    }

    @Override
    public void onJournalClickListener(JournalItem journalItem) {

    }

    @Override
    public void onJournalDeleteClickListener(JournalItem journalItem) {
        if(onDayClickListener != null){
            onDayClickListener.onJournalDeleteClickListener(journalItem);
        }
    }

    public class CalendarData{
        private int dayNum;
        private String dayName;
        private int monthNum;
        private String monthName;
        private int year;
        private String dateChecker;
        private String formattedDate;

//        private List<JournalItem> journalItems;
        private JournalAdapter journalAdapter;

        public CalendarData(Calendar calendar){
            dayNum = calendar.get(Calendar.DATE);
            dayName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            monthNum = calendar.get(Calendar.MONTH);
            monthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
            year = calendar.get(Calendar.YEAR);
            dateChecker = String.format("%04d-%02d-%02d", year, monthNum + 1, dayNum);
            formattedDate = monthName + " " + dayNum + ", " + year;
        }

        public String getDayName(){
            return dayName;
        }

        public int getDayNum(){
            return dayNum;
        }

        public void setJournalItems(List<JournalItem> journalItems){
            if(journalAdapter == null){
                this.journalAdapter = new JournalAdapter(context);
            }
            this.journalAdapter.addNewData(journalItems);
        }

        public void setJournalItem(JournalItem journalItem){
            if(journalAdapter == null){
                this.journalAdapter = new JournalAdapter(context);
            }
            this.journalAdapter.addItem(journalItem);
        }

        public JournalAdapter getJournalAdapter(){
            return journalAdapter;
        }

        public JournalAdapter getJournalAdapter(JournalAdapter.ClickListener clickListener){
            journalAdapter.setOnItemClickListener(clickListener);
            return journalAdapter;
        }

        public String getDateChecker(){
            return dateChecker;
        }


        public String getFormattedDate(){
            return formattedDate;
        }
    }

    public class ViewHolder{
        CalendarData calendarData;
        @BindView(R.id.dayNumTXT)           TextView dayNumTXT;
        @BindView(R.id.dayNameTXT)          TextView dayNameTXT;
        @BindView(R.id.messageTXT)          TextView messageTXT;
        @BindView(R.id.addBTN)              View addBTN;
        @BindView(R.id.journalEHLV)         ExpandableHeightListView journalEHLV;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(onDayClickListener != null){
                    onDayClickListener.onItemClick(((JournalCalendarDailyAdapter.ViewHolder) v.getTag()).calendarData);
                }
                break;
            case R.id.messageTXT:
            case R.id.addBTN:
                if(onDayClickListener != null){
                    onDayClickListener.onAddClick((CalendarData) v.getTag());
                }
                break;
        }

    }

    private OnDayClickListener onDayClickListener;

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }

    public interface OnDayClickListener {
        void onItemClick(CalendarData calendarData);
        void onAddClick(CalendarData calendarData);
        void onJournalDeleteClickListener(JournalItem journalItem);
    }
} 
