package com.highlysucceed.impact_weather.view.fragment.device;

import android.view.View;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.fragment.BaseFragment;
import com.highlysucceed.impact_weather.view.activity.DeviceActivity;

import butterknife.BindView;

public class DeviceMapFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = DeviceMapFragment.class.getName().toString();
    private DeviceActivity deviceActivity;

    @BindView(R.id.listViewBTN)              View listViewBTN;

    public static DeviceMapFragment newInstance() {
        DeviceMapFragment deviceMapFragment = new DeviceMapFragment();
        return deviceMapFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_device_map;
    }

    @Override
    public void onViewReady() {
        deviceActivity = (DeviceActivity) getActivity();
        deviceActivity.setTitle("My Devices Map");

        listViewBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.listViewBTN:
                deviceActivity.openMyDeviceListFragment();
                break;
        }
    }
}
