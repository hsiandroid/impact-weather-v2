package com.highlysucceed.impact_weather.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.server.android.model.FarmItem;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<FarmItem> {

    LayoutInflater flater;

    public CustomAdapter(Activity context, int resouceId, int textviewId, List<FarmItem> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FarmItem farmItem = getItem(position);

        View rowview = flater.inflate(R.layout.adapter_spinner,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
        txtTitle.setText(farmItem.name);

        return rowview;
    }
}
