package com.highlysucceed.impact_weather.view.activity;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.farm.FarmCreateDetailsFragment;
import com.highlysucceed.impact_weather.view.fragment.farm.FarmDetailFragment;
import com.highlysucceed.impact_weather.view.fragment.farm.FarmEditDetailsFragment;
import com.highlysucceed.impact_weather.view.fragment.farm.FarmJournalFragment;
import com.highlysucceed.impact_weather.view.fragment.farm.JournalCalendarFragment;
import com.highlysucceed.impact_weather.view.fragment.farm.JournalCreateFragment;
import com.highlysucceed.impact_weather.view.fragment.main.AdvisoryFragment;
import com.server.android.model.AdvisoryItem;
import com.server.android.model.JournalItem;

import java.util.List;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class FarmActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = FarmActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)       View mainIconIV;
    @BindView(R.id.mainTitleTXT)     TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
       switch(fragmentName){
           case "create":
               openCreateFragment();
               break;
           case "details":
               openMyFarmDetailFragment(getFragmentBundle().getInt("farm_id"));
               break;
           case "edit":
               openEditFarmDetailsFragment(getFragmentBundle().getInt("farm_id"));
               break;
           case "journal":
               openJournalCalendarFragment(getFragmentBundle().getInt("farm_id"), getFragmentBundle().getString("crop"));
               break;

       }
    }

    public void openCreateFragment(){
        switchFragment(FarmCreateDetailsFragment.newInstance());
    }

    public void openEditFarmDetailsFragment(int farmId){
        switchFragment(FarmEditDetailsFragment.newInstance(farmId));
    }

    public void openMyFarmDetailFragment(int farmID){
        switchFragment(FarmDetailFragment.newInstance(farmID));
    }

    public void openFarmJournalFragment(int farmId, String crop){
        switchFragment(FarmJournalFragment.newInstance(farmId, crop));
    }

    public void openJournalCalendarFragment(int farmId, String crop){
        switchFragment(JournalCalendarFragment.newInstance(farmId, crop));
    }

    public void openJournalCreateFragment(int farmId, String crop, String startDate){
        switchFragment(JournalCreateFragment.newInstance(farmId, crop, startDate));
    }

    public void openJournalCreateFragment(JournalItem journalItem){
        switchFragment(JournalCreateFragment.newInstance(journalItem));
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
                break;
        }
    }
}
