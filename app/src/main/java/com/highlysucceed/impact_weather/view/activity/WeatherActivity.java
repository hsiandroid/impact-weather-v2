package com.highlysucceed.impact_weather.view.activity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.impact_weather.R;
import com.highlysucceed.impact_weather.util.view.activity.RouteActivity;
import com.highlysucceed.impact_weather.view.fragment.weather.MeteogramreviewFragment;
import com.highlysucceed.impact_weather.view.fragment.weather.RainfallMapFragment;
import com.highlysucceed.impact_weather.view.fragment.weather.WeatherFragment;
import com.server.android.model.WeatherItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class WeatherActivity extends RouteActivity implements View.OnClickListener {
    public static final String TAG = WeatherActivity.class.getName().toString();

    @BindView(R.id.mainIconIV)      View mainIconIV;
    @BindView(R.id.mainTitleTXT)    TextView mainTitleTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }
    @Override
    public void onViewReady() {
        mainIconIV.setOnClickListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "weather":
                openWeatherFragment(getFragmentBundle().getInt("device_id"));
                break;
        }
    }

    public void openWeatherFragment(int deviceID){
        switchFragment(WeatherFragment.newInstance(deviceID));
    }

    public void openMeteogramreviewFragment(String code, String path){
        switchFragment(MeteogramreviewFragment.newInstance(code, path));
    }

    public void openRainfallMapFragment(){
        switchFragment(RainfallMapFragment.newInstance());
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                onBackPressed();
        }
    }

    public List<WeatherItem> transformDailyWeather(List<WeatherItem> raw){
        if(raw != null){
            List<WeatherItem> dailyWeather = new ArrayList<>();
            for(int i = 0 ; i < raw.size(); i++){
                WeatherItem weatherItem = raw.get(i);
                weatherItem.hourly_weather = new ArrayList<>();
                for(int j = i ; j < raw.size(); j++){
                    WeatherItem child = raw.get(j);
                    if(weatherItem.day.equalsIgnoreCase(child.day)){
                        weatherItem.hourly_weather.add(child);
                        i ++;
                        Log.e ("test", "equal");
                    }else{

                        Log.e ("test", "not equal");
                        i--;
                        break;
                    }
                }
                dailyWeather.add(weatherItem);
//                if(i == raw.size() - 2){
//                    break;
//                }
            }
            return dailyWeather;
        }else{
            return new ArrayList<>();
        }
    }
}
