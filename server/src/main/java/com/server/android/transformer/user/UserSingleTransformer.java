package com.server.android.transformer.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.util.BaseTransformer;
import com.server.android.model.UserItem;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class UserSingleTransformer extends BaseTransformer {

    @SerializedName("data")
    public UserItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {
        @SerializedName("name")
        public List<String> name = null;

        @SerializedName("email")
        public List<String> email = null;

        @SerializedName("contact")
        public List<String> contact = null;

        @SerializedName("key")
        public List<String> key = null;

        @SerializedName("value")
        public List<String> value = null;

    }
}
