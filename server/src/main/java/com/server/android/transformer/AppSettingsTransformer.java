package com.server.android.transformer;

import com.google.gson.annotations.SerializedName;
import com.server.android.model.ActivityItem;
import com.server.android.model.AdvertisementItem;
import com.server.android.model.AppSettingsItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class AppSettingsTransformer extends BaseTransformer {

    @SerializedName("data")
    public Data data;

    public class Data{
        @SerializedName("latest_version")
        public AppSettingsItem latest_version;

        @SerializedName("farm_activity")
        public List<ActivityItem> farm_activity;
    }

    @SerializedName("ads")
    public AdvertisementItem ads;

}
