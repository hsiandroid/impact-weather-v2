package com.server.android.transformer.journals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.util.BaseTransformer;
import com.server.android.model.JournalItem;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class JournalsSingleTransformer extends BaseTransformer {

    @SerializedName("data")
    public JournalItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {
        @SerializedName("title")
        public List<String> title;
//        public List<String> status;

        @SerializedName("qty")
        public List<String> qty;

        @SerializedName("brand")
        public List<String> brand;

        @SerializedName("entry_date")
        public List<String> entry_date;
    }
}
