package com.server.android.transformer.farm;

import com.google.gson.annotations.SerializedName;
import com.server.android.model.FarmItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class FarmCollectionTransformer extends BaseTransformer {
    @SerializedName("data")
    public List<FarmItem> data;
}
