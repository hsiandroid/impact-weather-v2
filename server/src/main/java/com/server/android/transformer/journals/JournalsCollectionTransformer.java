package com.server.android.transformer.journals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.model.JournalItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class JournalsCollectionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<JournalItem> data;

    @SerializedName("errors")
    @Expose
    public JournalsSingleTransformer.Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {

        public List<String> title;
        //        public List<String> status;
        public List<String> entry_date;
    }
}
