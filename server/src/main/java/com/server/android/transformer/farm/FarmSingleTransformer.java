package com.server.android.transformer.farm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.model.FarmItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class FarmSingleTransformer extends BaseTransformer {
    @SerializedName("data")
    public FarmItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {
        @SerializedName("name")
        public List<String> name;

        @SerializedName("size")
        public List<String> size;

        @SerializedName("map")
        public List<String> map;

        @SerializedName("crops")
        public List<String> crops;
    }
}
