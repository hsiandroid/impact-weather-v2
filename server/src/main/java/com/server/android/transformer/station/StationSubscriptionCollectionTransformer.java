package com.server.android.transformer.station;

import com.google.gson.annotations.SerializedName;
import com.server.android.model.SubscriptionItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class StationSubscriptionCollectionTransformer extends BaseTransformer {
    @SerializedName("data")
    public List<SubscriptionItem> data;
}
