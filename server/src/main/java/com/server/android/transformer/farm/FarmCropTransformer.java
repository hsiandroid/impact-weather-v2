package com.server.android.transformer.farm;

import com.google.gson.annotations.SerializedName;
import com.server.android.util.BaseTransformer;
import com.server.android.model.FarmItem;
import com.server.android.model.JournalItem;
import com.server.android.model.RecommendationItem;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class FarmCropTransformer extends BaseTransformer {

    @SerializedName("data")
    public Data data;

    public static class Data{

        @SerializedName("farm")
        public Farm farm;

        @SerializedName("journals")
        public Journals journals;

        @SerializedName("recommendations")
        public Recommendations recommendations;

        public static class Farm{
            @SerializedName("data")
            public FarmItem data;
        }

        public static class Journals{
            @SerializedName("data")
            public List<JournalItem> data;
        }

        public static class Recommendations{
            @SerializedName("data")
            public List<RecommendationItem> data;
        }
    }


}
