package com.server.android.transformer.crop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.model.CropsItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class CropsSingleTransformer extends BaseTransformer {
    @SerializedName("data")
    public CropsItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {
        public List<String> name;
        public List<String> code;
    }
}
