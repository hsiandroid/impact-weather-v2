package com.server.android.transformer.crop;

import com.google.gson.annotations.SerializedName;
import com.server.android.model.CropsItem;
import com.server.android.util.BaseTransformer;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class CropsCollectionTransformer extends BaseTransformer {
    @SerializedName("data")
    public List<CropsItem> data;
}
