package com.server.android.transformer.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.server.android.util.BaseTransformer;
import com.server.android.model.SubscriptionItem;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class StationSubscriptionSingleTransformer extends BaseTransformer {

    @SerializedName("data")
    public SubscriptionItem data;

    @SerializedName("errors")
    @Expose
    public Requirements requires;

    @Override
    public boolean hasRequirements() {
        return checkEmpty(requires);
    }

    public class Requirements {
        @SerializedName("name")
        public List<String> name;
    }
}
