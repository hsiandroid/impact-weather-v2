package com.server.android.transformer.station;

import com.google.gson.annotations.SerializedName;
import com.server.android.util.BaseTransformer;
import com.server.android.model.DeviceItem;

import java.util.List;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class StationCollectionTransformer extends BaseTransformer {

    @SerializedName("data")
    public List<DeviceItem> data;
}
