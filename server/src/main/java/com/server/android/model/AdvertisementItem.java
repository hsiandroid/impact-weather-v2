package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class AdvertisementItem {

    @SerializedName("id")
    public int id;

    @SerializedName("web_link")
    public String web_link;

    @SerializedName("image")
    private String image;

    public String getImage() {
        if(image == null){
            return "/";
        }
        if(image.equals("")){
            return "/";
        }
        return image;
    }
}
