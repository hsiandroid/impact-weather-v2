package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class JournalItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("crop")
    public String crop;

    @SerializedName("entry_date")
    public String entry_date;

    @SerializedName("entry_date_db")
    public String entry_date_db;

    @SerializedName("start_time")
    public String start_time;

    @SerializedName("end_time")
    public String end_time;

    @SerializedName("qty")
    public int qty;

    @SerializedName("brand")
    public String brand;

    @SerializedName("status")
    public String status;

    @SerializedName("recommendationItems")
    public List<RecommendationItem> recommendationItems;

    public String getChecker(){
        return entry_date_db;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.crop);
        dest.writeString(this.entry_date);
        dest.writeString(this.status);
        dest.writeTypedList(this.recommendationItems);
    }

    public JournalItem() {
    }

    protected JournalItem(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.crop = in.readString();
        this.entry_date = in.readString();
        this.status = in.readString();
        this.recommendationItems = in.createTypedArrayList(RecommendationItem.CREATOR);
    }

    public static final Parcelable.Creator<JournalItem> CREATOR = new Parcelable.Creator<JournalItem>() {
        @Override
        public JournalItem createFromParcel(Parcel source) {
            return new JournalItem(source);
        }

        @Override
        public JournalItem[] newArray(int size) {
            return new JournalItem[size];
        }
    };
}
