package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ActivityItem implements Parcelable{

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("code")
    public String code;

    public ActivityItem() {

    }

    protected ActivityItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        code = in.readString();
    }

    public static final Creator<ActivityItem> CREATOR = new Creator<ActivityItem>() {
        @Override
        public ActivityItem createFromParcel(Parcel in) {
            return new ActivityItem(in);
        }

        @Override
        public ActivityItem[] newArray(int size) {
            return new ActivityItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(code);
    }
}
