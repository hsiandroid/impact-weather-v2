package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CropItem implements Parcelable {

    @SerializedName("name")
    public String name;

    @SerializedName("variety")
    public String variety;

    @SerializedName("isSelected")
    public boolean isSelected;

    @SerializedName("journal")
    public Journal journal;

    @SerializedName("activity")
    public Activity activity;

    @SerializedName("recommendation")
    public Recommendation recommendation;

    public static class Journal implements Parcelable {

        @SerializedName("data")
        public List<JournalItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Journal() {
        }

        protected Journal(Parcel in) {
            this.data = in.createTypedArrayList(JournalItem.CREATOR);
        }

        public static final Parcelable.Creator<Journal> CREATOR = new Parcelable.Creator<Journal>() {
            @Override
            public Journal createFromParcel(Parcel source) {
                return new Journal(source);
            }

            @Override
            public Journal[] newArray(int size) {
                return new Journal[size];
            }
        };
    }

    public static class Activity implements Parcelable {

        @SerializedName("data")
        public List<ActivityItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Activity() {
        }

        protected Activity(Parcel in) {
            this.data = in.createTypedArrayList(ActivityItem.CREATOR);
        }

        public static final Parcelable.Creator<Activity> CREATOR = new Parcelable.Creator<Activity>() {
            @Override
            public Activity createFromParcel(Parcel source) {
                return new Activity(source);
            }

            @Override
            public Activity[] newArray(int size) {
                return new Activity[size];
            }
        };
    }

    public static class Recommendation implements Parcelable {

        @SerializedName("data")
        public List<RecommendationItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Recommendation() {
        }

        protected Recommendation(Parcel in) {
            this.data = in.createTypedArrayList(RecommendationItem.CREATOR);
        }

        public static final Parcelable.Creator<Recommendation> CREATOR = new Parcelable.Creator<Recommendation>() {
            @Override
            public Recommendation createFromParcel(Parcel source) {
                return new Recommendation(source);
            }

            @Override
            public Recommendation[] newArray(int size) {
                return new Recommendation[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.variety);
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.journal, flags);
        dest.writeParcelable(this.recommendation, flags);
    }

    public CropItem() {
    }

    public CropItem(String name) {
        this.name = name;
        this.journal = new Journal();
        this.journal.data = new ArrayList<>();
        this.recommendation = new Recommendation();
        this.recommendation.data = new ArrayList<>();
    }

    protected CropItem(Parcel in) {
        this.name = in.readString();
        this.variety = in.readString();
        this.isSelected = in.readByte() != 0;
        this.journal = in.readParcelable(Journal.class.getClassLoader());
        this.recommendation = in.readParcelable(Recommendation.class.getClassLoader());
    }

    public static final Parcelable.Creator<CropItem> CREATOR = new Parcelable.Creator<CropItem>() {
        @Override
        public CropItem createFromParcel(Parcel source) {
            return new CropItem(source);
        }

        @Override
        public CropItem[] newArray(int size) {
            return new CropItem[size];
        }
    };
}
