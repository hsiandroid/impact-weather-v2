
package com.server.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class WeatherItem  {

    @SerializedName("id")
    public int id;

    @SerializedName("image")
    public String image = "http://demo.waveone.highlysucceed.com/weather/heavy_rain_showers.png";

    @SerializedName("schedule")
    public String schedule;


    @SerializedName("date")
    public String date;

    @SerializedName("feeled_temperature")
    public String feeled_temperature;

    @SerializedName("day")
    public String day;

    @SerializedName("hour")
    public String hour;

    @SerializedName("read_hr")
    public String read_hr;

    @SerializedName("temperature")
    public String temperature;

    @SerializedName("min_temp")
    public String min_temp;

    @SerializedName("max_temp")
    public String max_temp;

    @SerializedName("wind_speed")
    public String wind_speed;

    @SerializedName("precipitation")
    public String precipitation;

    @SerializedName("probability_of_precip")
    public String probability_of_precip;

    @SerializedName("probability_of_precip_percentage")
    public int precip_percentage;

    @SerializedName("relative_humidity")
    public String relative_humidity;

    @SerializedName("wind_direction")
    public String wind_direction;

    @SerializedName("status")
    public String status = "Sunny";

    @SerializedName("isVisible")
    public boolean isVisible;

    @SerializedName("hourly_weather")
    public List<WeatherItem> hourly_weather;

    public WeatherItem(){

    }
}
