package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class AppSettingsItem {

    @SerializedName("id")
    public int id;

    @SerializedName("version_name")
    public String version_name;

    @SerializedName("major_version")
    public int major_version;

    @SerializedName("minor_version")
    public int minor_version;

    @SerializedName("changelogs")
    public String changelogs;
}
