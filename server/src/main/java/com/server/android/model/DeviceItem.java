package com.server.android.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class DeviceItem {

    @SerializedName("id")
    public int id;

    @SerializedName("subscription_id")
    public int subscription_id;

    @SerializedName("name")
    public String name;

    @SerializedName("code")
    public String code;

    @SerializedName("meteogram_image")
    private String meteogram_image;

    @SerializedName("is_subscribed")
    public boolean is_subscribed;

    @SerializedName("location")
    public Location location;

    @SerializedName("info")
    public Info info;

    @SerializedName("current_weather")
    public CurrentWeather current_weather;

    @SerializedName("daily_weather")
    public DailyWeather daily_weather;

    @SerializedName("weekly_weather")
    public WeekLyWeather weekly_weather;

    @SerializedName("recommendations")
    public Recommendations recommendations;

    public DeviceItem() {
        location = new Location();
        info = new Info();
    }

    public String getMeteogram() {
        if(meteogram_image == null){
            return "/";
        }
        if(meteogram_image.equals("")){
            return "/";
        }
        return meteogram_image;
    }

    public static class Location{

        public Location (){
            data = new Data();
        }

        @SerializedName("data")
        public Data data;

        public static class Data{

            @SerializedName("distance")
            public String distance;

            @SerializedName("geo_lat")
            public double geo_lat;

            @SerializedName("geo_long")
            public double geo_long;

            @SerializedName("street_address")
            public String street_address;

            @SerializedName("city")
            public String city;

            @SerializedName("state")
            public String state;

            @SerializedName("country")
            public String country;

            public LatLng getLatLng(){
                return new LatLng(geo_lat, geo_long);
            }

        }
    }

    public static class Info{

        public Info (){
            data = new Data();
        }

        @SerializedName("data")
        public Data data;

        public static class Data{

        }
    }

    public static class CurrentWeather{

        @SerializedName("data")
        public WeatherItem data;
    }

    public static class DailyWeather{

        @SerializedName("data")
        public List<WeatherItem> data;
    }

    public static class WeekLyWeather{

        @SerializedName("data")
        public List<WeatherItem> data;
    }

    public static class Recommendations{

        @SerializedName("data")
        public List<RecommendationItem> data;
    }
}
