package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class SubscriptionItem {

    @SerializedName("id")
    public int id;

    @SerializedName("type")
    public String type;

    @SerializedName("status")
    public String status;

    @SerializedName("station")
    public Station station;


    public static class Station{

        @SerializedName("data")
        public DeviceItem data;
    }
}
