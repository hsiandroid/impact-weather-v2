package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class FarmItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("start_date_display")
    public String start_date;

    @SerializedName("start_date")
    public String start_date_display;

    @SerializedName("size")
    public double size;

    @SerializedName("owner")
    public UserItem owner;

    @SerializedName("map")
    public Map map;

    @SerializedName("crops")
    public Crops crops;

    @SerializedName("status")
    public Status status;

    @SerializedName("station")
    public Station station;

    @SerializedName("daily_forecast")
    public Daily_forecast daily_forecast;

    @SerializedName("weekly_forecast")
    public Weekly_forecast weekly_forecast;

    @SerializedName("current_forecast")
    public Current_forecast current_forecast;

    public static class Daily_forecast{
        @SerializedName("data")
        public List<WeatherItem> data;
    }

    public static class Weekly_forecast{
        @SerializedName("data")
        public List<WeatherItem> data;
    }

    public static class Current_forecast{
        @SerializedName("data")
        public WeatherItem data;
    }

    public static class Station{

        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("id")
            public int id;

            @SerializedName("subscription_id")
            public int subscriptionId;

            @SerializedName("name")
            public String name;

            @SerializedName("code")
            public String code;

            @SerializedName("is_subscribed")
            public boolean isSubscribed;

            @SerializedName("is_nearby")
            public boolean isNearby;

            @SerializedName("meteogram_image")
            public String meteogramImage;
        }
    }


    public static class Crops implements Parcelable {

        @SerializedName("data")
        public List<CropItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Crops() {
        }

        protected Crops(Parcel in) {
            this.data = in.createTypedArrayList(CropItem.CREATOR);
        }

        public static final Parcelable.Creator<Crops> CREATOR = new Parcelable.Creator<Crops>() {
            @Override
            public Crops createFromParcel(Parcel source) {
                return new Crops(source);
            }

            @Override
            public Crops[] newArray(int size) {
                return new Crops[size];
            }
        };
    }

    public static class Status implements Parcelable {

        @SerializedName("data")
        public Data data;

        public static class Data implements Parcelable {

            @SerializedName("critical")
            public String critical;

            @SerializedName("moderate")
            public String moderate;

            @SerializedName("low")
            public String low;

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.critical);
                dest.writeString(this.moderate);
                dest.writeString(this.low);
            }

            public Data() {
            }

            protected Data(Parcel in) {
                this.critical = in.readString();
                this.moderate = in.readString();
                this.low = in.readString();
            }

            public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
                @Override
                public Data createFromParcel(Parcel source) {
                    return new Data(source);
                }

                @Override
                public Data[] newArray(int size) {
                    return new Data[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.data, flags);
        }

        public Status() {
        }

        protected Status(Parcel in) {
            this.data = in.readParcelable(Data.class.getClassLoader());
        }

        public static final Parcelable.Creator<Status> CREATOR = new Parcelable.Creator<Status>() {
            @Override
            public Status createFromParcel(Parcel source) {
                return new Status(source);
            }

            @Override
            public Status[] newArray(int size) {
                return new Status[size];
            }
        };
    }

    public static class Map implements Parcelable {

        @SerializedName("data")
        public List<MapItem> data;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.data);
        }

        public Map() {
        }

        protected Map(Parcel in) {
            this.data = in.createTypedArrayList(MapItem.CREATOR);
        }

        public static final Parcelable.Creator<Map> CREATOR = new Parcelable.Creator<Map>() {
            @Override
            public Map createFromParcel(Parcel source) {
                return new Map(source);
            }

            @Override
            public Map[] newArray(int size) {
                return new Map[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.owner, flags);
        dest.writeParcelable(this.map, flags);
        dest.writeParcelable(this.crops, flags);
        dest.writeParcelable(this.status, flags);
    }

    public FarmItem() {
    }

    protected FarmItem(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.owner = in.readParcelable(UserItem.class.getClassLoader());
        this.map = in.readParcelable(Map.class.getClassLoader());
        this.crops = in.readParcelable(Crops.class.getClassLoader());
        this.status = in.readParcelable(Status.class.getClassLoader());
    }

    public static final Creator<FarmItem> CREATOR = new Creator<FarmItem>() {
        @Override
        public FarmItem createFromParcel(Parcel source) {
            return new FarmItem(source);
        }

        @Override
        public FarmItem[] newArray(int size) {
            return new FarmItem[size];
        }
    };
}
