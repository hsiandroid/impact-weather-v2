package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class ProductItem {

    @SerializedName("id")
    public int id;

    @SerializedName("userID")
    public String userID;

    @SerializedName("product")
    public String product;

    @SerializedName("quantity")
    public String quantity;

    public ProductItem(){

    }
}
