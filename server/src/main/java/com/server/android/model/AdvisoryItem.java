package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class AdvisoryItem {
    @SerializedName("id")
    public int id;

    @SerializedName("content")
    public String content;


    public CreatedAtBean created_at;

    public static class CreatedAtBean {
        public String date_db;
        public String month_year;
        public String time_passed;
        public Timestamp timestamp;

        public static class Timestamp {
            public String date;
            public int timezone_type;
            public String timezone;
        }
    }
}
