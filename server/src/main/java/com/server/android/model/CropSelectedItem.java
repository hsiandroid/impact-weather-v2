package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CropSelectedItem {

    @SerializedName("name")
    public String name;

    @SerializedName("variety")
    public String variety;

    @SerializedName("cropsItem")
    public CropsItem cropsItem;
}
