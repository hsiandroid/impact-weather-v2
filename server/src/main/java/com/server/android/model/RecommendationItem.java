package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class RecommendationItem implements Parcelable {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("type")
    public String type;

    @SerializedName("image")
    public String image;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.content);
        dest.writeString(this.type);
        dest.writeString(this.image);
    }

    public RecommendationItem() {
    }

    protected RecommendationItem(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.content = in.readString();
        this.type = in.readString();
        this.image = in.readString();
    }

    public static final Creator<RecommendationItem> CREATOR = new Creator<RecommendationItem>() {
        @Override
        public RecommendationItem createFromParcel(Parcel source) {
            return new RecommendationItem(source);
        }

        @Override
        public RecommendationItem[] newArray(int size) {
            return new RecommendationItem[size];
        }
    };
}
