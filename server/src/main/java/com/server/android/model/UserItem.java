package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class UserItem implements Parcelable {

    @SerializedName("id")
    public Integer id = 0;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("contact")
    public String contact;

    @SerializedName("image")
    public String image;

    @SerializedName("allow_weather_station")
    public String allow_weather_station = "yes";

    @SerializedName("default_farm_id")
    public int defaultFarmId;

    @SerializedName("num_farm")
    public int numFarm;

    @SerializedName("username")
    public String username;

    @SerializedName("fb_id")
    public String fbId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.contact);
        dest.writeString(this.image);
    }

    public UserItem() {
    }

    protected UserItem(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.email = in.readString();
        this.contact = in.readString();
        this.image = in.readString();
    }

    public static final Creator<UserItem> CREATOR = new Creator<UserItem>() {
        @Override
        public UserItem createFromParcel(Parcel source) {
            return new UserItem(source);
        }

        @Override
        public UserItem[] newArray(int size) {
            return new UserItem[size];
        }
    };
}
