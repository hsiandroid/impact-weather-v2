package com.server.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BCTI 3 on 3/6/2017.
 */

public class CropsItem {

    @SerializedName("name")
    public String name;

    @SerializedName("code")
    public String code;

    @SerializedName("variety")
    public List<String> variety;

    public List<String> getVariety() {
        if (variety == null) {
            variety = new ArrayList<>();
        }
        return variety;
    }

    public String getFirstVariety() {
        if (getVariety().size() > 0) {
            return getVariety().get(0);
        }
        return "";
    }
}
