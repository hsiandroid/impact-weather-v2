package com.server.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 5/3/2017.
 */

public class MapItem implements Parcelable {

    @SerializedName("geo_lat")
    public double geo_lat;

    @SerializedName("geo_long")
    public double geo_long;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.geo_lat);
        dest.writeDouble(this.geo_long);
    }

    public MapItem() {
    }

    protected MapItem(Parcel in) {
        this.geo_lat = in.readDouble();
        this.geo_long = in.readDouble();
    }

    public static final Parcelable.Creator<MapItem> CREATOR = new Parcelable.Creator<MapItem>() {
        @Override
        public MapItem createFromParcel(Parcel source) {
            return new MapItem(source);
        }

        @Override
        public MapItem[] newArray(int size) {
            return new MapItem[size];
        }
    };
}
