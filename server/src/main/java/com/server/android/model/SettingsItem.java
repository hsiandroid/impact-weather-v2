package com.server.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class SettingsItem {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("value")
    public String value;

    @SerializedName("key")
    public String key;

    public SettingsItem() {

    }

    public SettingsItem(int id, String title, String value, String key){
        this.id = id;
        this.title = title;
        this.value = value;
        this.key = key;
    }
}
