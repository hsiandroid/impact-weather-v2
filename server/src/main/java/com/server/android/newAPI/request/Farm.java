package com.server.android.newAPI.request;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;

import com.server.android.model.AdvisoryItem;
import com.server.android.model.FarmItem;
import com.server.android.newAPI.config.Keys;
import com.server.android.newAPI.config.Url;
import com.server.android.newAPI.server.request.APIRequest;
import com.server.android.newAPI.server.request.APIResponse;
import com.server.android.newAPI.server.transformer.CollectionTransformer;
import com.server.android.newAPI.server.transformer.SingleTransformer;


import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;


/**
 * Created by Labyalo on 8/3/2017.
 */

public class Farm {

    public static Farm getDefault(){ return new Farm(); }

    public APIRequest show(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<FarmItem>>(context) {
            @Override
            public Call<SingleTransformer<FarmItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleModel(Url.getFarmInfo(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditResponse(this));
            }
        };

        apiRequest
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Loading")
                .addParameter(Keys.INCLUDE, "info, owner, map, crops,daily_forecast,current_forecast,station,weekly_forecast")
                .addParameter(Keys.FARM_ID, id);
        return apiRequest;
    }

    public APIRequest advisory(Context context) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<AdvisoryItem>>(context) {
            @Override
            public Call<CollectionTransformer<AdvisoryItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAdvisoryModel(Url.getAdvisory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AdvisoryResponse(this));
            }
        };

//        apiRequest
////                        .execute();        .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
////                .addParameter(Keys.INCLUDE, "info, owner, map, crops")
//

        return apiRequest;
    }





    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<FarmItem>> requestCollectionModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<AdvisoryItem>> requestAdvisoryModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<SingleTransformer<FarmItem>> requestSingleModel(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class CollectionResponse extends APIResponse<CollectionTransformer<FarmItem>> {
        public CollectionResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AdvisoryResponse extends APIResponse<CollectionTransformer<AdvisoryItem>> {
        public AdvisoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


    public class EditResponse extends APIResponse<SingleTransformer<FarmItem>> {
        public EditResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


}

