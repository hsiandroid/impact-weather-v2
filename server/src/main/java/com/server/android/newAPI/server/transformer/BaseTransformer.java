package com.server.android.newAPI.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.server.android.model.ErrorModel;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Internal Server Error";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("num_advisory")
    public String num_advisory = "";

    @SerializedName("advisory_display")
    public String advisory_display = "";

    @SerializedName("has_morepages")
    public Boolean has_morepages = false;

    @SerializedName("hasRequirements")
    public boolean hasRequirements(){
        return checkEmpty(errors);
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }

    @SerializedName("errors")
    public ErrorModel errors;
}
