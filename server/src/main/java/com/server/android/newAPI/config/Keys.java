package com.server.android.newAPI.config;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static String DEVICE_ID = "device_id";
    public static String DEVICE_NAME = "device_name";
    public static String DEVICE_MODEL = "device_model";
    public static String DEVICE_IMEI = "device_imei";
    public static String OS_VERSION= "os_version";
    public static String DEVICE_REG_ID = "device_reg_id";
    public static String PAGE = "page";
    public static String PER_PAGE = "per_page";
    public static String USER_ID = "user_id";
    public static String USERNAME = "username";
    public static String NAME = "name";
    public static String PASSWORD = "password";
    public static String OLD_PASSWORD = "old_password";
    public static String INCLUDE = "include";
    public static String AUTH_ID = "auth_id";
    public static String AUTHORIZATION = "authorization";
    public static String EMAIL = "email";
    public static String CONTACT_NUMBER = "contact_number";
    public static String CONTACT_PHONE = "contact_phone";
    public static String CONTACT_MOBILE = "contact_mobile";
    public static String CONTACT_EMAIL = "contact_email";
    public static String CURRENT_PASSWORD = "current_password";
    public static String PASSWORD_CONFIRMATION = "password_confirmation";
    public static String COUNTRY_ISO = "country_iso";
    public static String COUNTRY_CODE = "country_code";
    public static String FILE = "file";
    public static String FILES = "file[]";
    public static String DESCRIPTION = "description";
    public static String FB_ID = "fb_id";
    public static String GOOGLE_ID = "google_id";
    public static String CATEGORY_ID = "category_id";
    public static String SUB_CATEGORY_ID = "subcategory_id";
    public static String PROMOTION_ID = "promotion_id";
    public static String STAFF_ID = "staff_id";
    public static String COUNTRY = "country";
    public static String LOCATION = "location";
    public static String KEYWORD = "keyword";
    public static String ACCESS_TOKEN = "access_token";
    public static String VALIDATION_TOKEN = "validation_token";
    public static String BUSINESS_NAME = "business_name";
    public static String BUSINESS_EMAIL = "business_email";
    public static String BUSINESS_ADDRESS = "business_address";
    public static String BUSINESS_REG_NUMBER = "business_reg_number";
    public static String BUSINESS_DESCRIPTION = "business_description";
    public static String TERMS = "terms";
    public static String DISCOUNT_TYPE = "discount_type";
    public static String TYPE = "type";
    public static String DISCOUNT_VALUE = "discount_value";
    public static String ORIGINAL_PRICE = "original_price";
    public static String START_DATE = "start_date";
    public static String END_DATE = "end_date";
    public static String SORT_BY = "sort_by";
    public static String SORT_ORDER = "sort_order";
    public static String PROMO_CODE = "promo_code";



    //Farm
    public static String FARM_ID = "farm_id";
}
