package com.server.android.newAPI.config;


import com.server.android.newAPI.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
//    public static final String PRODUCTION_URL = decrypt("https://reward.commonground.work");
    public static final String PRODUCTION_URL = decrypt("http://impact.highlysucceed.com");
    public static final String DEBUG_URL = decrypt("http://impact.highlysucceed.com");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    //    Farm

    public static final String getFarmInfo(){return "api/farms/show.json";}

    public static final String getAllFarmInfo(){return "/api/farms/all.json";}

    public static final String getAdvisory(){return "/api/advisory.json";}

}
