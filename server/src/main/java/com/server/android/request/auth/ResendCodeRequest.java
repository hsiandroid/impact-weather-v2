package com.server.android.request.auth;

import android.content.Context;

import com.server.android.util.BaseRequest;
import com.server.android.transformer.user.LoginTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class ResendCodeRequest extends BaseRequest<LoginTransformer> {

    public ResendCodeRequest(Context context){
        super(context);
    }

    @Override
    public Call<LoginTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getParameters());
    }

    @Override
    public void responseData(Response<LoginTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.auth.RESEND_CODE)
        Call<LoginTransformer> requestParam(@Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<LoginTransformer> response) {
            super(response);
        }
    }
}
