package com.server.android.request.farm;

import android.content.Context;

import com.server.android.transformer.farm.FarmCropTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class FarmCropRequest extends BaseRequest<FarmCropTransformer> {

    public FarmCropRequest(Context context){
        super(context);
    }

    @Override
    public Call<FarmCropTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<FarmCropTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.farm_crops.SHOW)
        Call<FarmCropTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<FarmCropTransformer> response) {
            super(response);
        }
    }
}
