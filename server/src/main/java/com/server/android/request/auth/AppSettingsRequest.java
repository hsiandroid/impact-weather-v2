package com.server.android.request.auth;

import android.content.Context;


import com.server.android.transformer.AppSettingsTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class AppSettingsRequest extends BaseRequest<AppSettingsTransformer> {

    public AppSettingsRequest(Context context){
        super(context);
        setCheckToken(false);
    }

    @Override
    public Call<AppSettingsTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam();
    }

    @Override
    public void responseData(Response<AppSettingsTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.APP_SETTINGS)
        Call<AppSettingsTransformer> requestParam();
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<AppSettingsTransformer> response) {
            super(response);
        }
    }
}
