package com.server.android.request.inquiry;

import android.content.Context;

import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class InquiryAdvisoryRequest extends BaseRequest<UserSingleTransformer> {

    public InquiryAdvisoryRequest(Context context){
        super(context);
    }

    private MultipartBody.Part image;

    @Override
    public Call<UserSingleTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getRequestBodyParameters(), image);
    }

    public void addPart(String key, File file){
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        image = MultipartBody.Part.createFormData(key, file.getName(), reqFile);
    }

    @Override
    public void responseData(Response<UserSingleTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @Multipart
        @POST(Variable.server.route.inquiry.ADVISORY)
        Call<UserSingleTransformer> requestParam(@Header("Authorization") String authorization, @PartMap Map<String, RequestBody> params, @Part MultipartBody.Part file);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<UserSingleTransformer> response) {
            super(response);
        }
    }
}
