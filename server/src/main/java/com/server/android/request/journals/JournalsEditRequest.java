package com.server.android.request.journals;

import android.content.Context;

import com.server.android.transformer.journals.JournalsSingleTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class JournalsEditRequest extends BaseRequest<JournalsSingleTransformer> {

    public JournalsEditRequest(Context context){
        super(context);
    }

    @Override
    public Call<JournalsSingleTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<JournalsSingleTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.journals.EDIT)
        Call<JournalsSingleTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<JournalsSingleTransformer> response) {
            super(response);
        }
    }
}
