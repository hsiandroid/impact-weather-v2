package com.server.android.request.farm;

import android.content.Context;

import com.server.android.util.BaseRequest;
import com.server.android.transformer.farm.FarmSingleTransformer;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class FarmCropsEditRequest extends BaseRequest<FarmSingleTransformer> {

    public FarmCropsEditRequest(Context context){
        super(context);
    }

    @Override
    public Call<FarmSingleTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<FarmSingleTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @FormUrlEncoded
        @POST(Variable.server.route.farms.EDIT_CROPS)
        Call<FarmSingleTransformer> requestParam(@Header("Authorization") String authorization, @FieldMap Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<FarmSingleTransformer> response) {
            super(response);
        }
    }
}
