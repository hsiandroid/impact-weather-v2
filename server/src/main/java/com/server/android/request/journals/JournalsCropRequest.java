package com.server.android.request.journals;

import android.content.Context;

import com.server.android.transformer.journals.JournalsCollectionTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class JournalsCropRequest extends BaseRequest<JournalsCollectionTransformer> {

    public JournalsCropRequest(Context context){
        super(context);
    }

    @Override
    public Call<JournalsCollectionTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getAuthorization(), getParameters());
    }

    @Override
    public void responseData(Response<JournalsCollectionTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.journals.CROP)
        Call<JournalsCollectionTransformer> requestParam(@Header("Authorization") String authorization, @Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData{
        public ServerResponse(Response<JournalsCollectionTransformer> response) {
            super(response);
        }
    }
}
