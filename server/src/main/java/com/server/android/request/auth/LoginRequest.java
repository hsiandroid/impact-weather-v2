package com.server.android.request.auth;

import android.content.Context;

import com.server.android.transformer.user.UserSingleTransformer;
import com.server.android.util.BaseRequest;
import com.server.android.util.Variable;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class LoginRequest extends BaseRequest<UserSingleTransformer> {

    public LoginRequest(Context context){
        super(context);
    }

    @Override
    public Call<UserSingleTransformer> onCreateCall() {
        return getRetrofit().create(RequestService.class).requestParam(getParameters());
    }

    @Override
    public void responseData(Response<UserSingleTransformer> response) {
        super.responseData(response);
        EventBus.getDefault().post(new ServerResponse(response));
    }

    public interface RequestService{
        @POST(Variable.server.route.auth.LOGIN)
        Call<UserSingleTransformer> requestParam(@Body Map<String, String> params);
    }

    public class ServerResponse extends ResponseData {
        public ServerResponse(Response<UserSingleTransformer> response) {
            super(response);
        }
    }
}
