package com.server.android.util;

import com.google.gson.annotations.SerializedName;

/**
 * Created by BCTI 3 on 12/9/2016.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "";

    @SerializedName("has_morepages")
    public boolean has_morepages = false;

    @SerializedName("token")
    public Token token;

    @SerializedName("default_farm_id")
    public int defaultFarmId;

    public boolean hasRequirements(){
        return false;
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }

    public class Token{
        @SerializedName("expires_in")
        public String expires_in;

        @SerializedName("access_token")
        public String access_token;

        @SerializedName("refresh_token")
        public String refresh_token;
    }

}