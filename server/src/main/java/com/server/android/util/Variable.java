package com.server.android.util;

/**
 * Created by BCTI 3 on 12/8/2016.
 */

public class Variable {
    public static final String API = "\\api";
    public static final String FORMAT = ".json";

    public static final String APP_SETTINGS = "\\app-settings";
    public static final String AUTH = "\\auth";
    public static final String FARMS = "\\farms";
    public static final String JOURNALS = "\\journals";
    public static final String PROFILE = "\\profile";
    public static final String CROPS   = "\\crops";
    public static final String STATIONS = "\\stations";
    public static final String FARM_CROPS = "\\farm-crops";
    public static final String INQUIRY = "\\inquiry";
    public static final String V2 = "\\v2";

    // inquiry
    public static final String ADVISORY = "\\advisory";

    // auth
    public static final String LOGIN = "\\login";
    public static final String REGISTER = "\\register";
    public static final String RESEND_CODE = "\\resend-code";
    public static final String LOGOUT = "\\logout";
    public static final String REFRESH_TOKEN = "\\refresh-token";

    // profile
    public static final String UPLOAD_AVATAR = "\\upload-avatar";
    public static final String EDIT_FIELD = "\\edit-field";
    public static final String CHANGE_PASSWORD = "\\change-password";

    // farms
    public static final String EDIT_CROPS = "\\edit-crops";

    // journal
    public static final String CROP = "\\crop";

    // stations
    public static final String NEARBY = "\\nearby";
    public static final String SUBSCRIPTIONS = "\\subscriptions";
    public static final String SUBSCRIBE = "\\subscribe";
    public static final String UNSUBSCRIBE = "\\unsubscribe";
    public static final String OWNED = "\\owned";


    //basic
    public static final String CREATE = "\\create";
    public static final String EDIT = "\\edit";
    public static final String SHOW = "\\show";
    public static final String DELETE = "\\delete";
    public static final String ALL = "\\all";
    public static final String INFO = "\\info";

    public static final String GET_COUNTRY = "\\get-country";

    public static class server{

        public static class client{
            public static final String ID = "2";
            public static final String SECRET = "ZUv9DNZbqsEg2EofuZtpM0rLtpEeeIIJvRIKyIe9";
        }

        public static class key{
            public static final String API_TOKEN = "api_token";
            public static final String DEVICE_ID = "device_id";
            public static final String DEVICE_NAME = "device_name";
            public static final String DEVICE_REG_ID = "device_reg_id";
            public static final String PAGE = "page";
            public static final String PER_PAGE = "per_page";
            public static final String USERNAME = "username";
            public static final String PASSWORD = "password";
            public static final String FILE = "file";
            public static final String EMAIL = "email";
            public static final String NAME = "name";
            public static final String SIZE = "size";
            public static final String CONTACT = "contact";
            public static final String CODE = "code";
            public static final String CLIENT_ID = "client_id";
            public static final String CLIENT_SECRET = "client_secret";
            public static final String REFRESH_TOKEN = "refresh_token";
            public static final String CROPS_ARRAY = "crops[]";
            public static final String CROP = "crop";
            public static final String MONTH_YEAR = "month_year";
            public static final String CROPS = "crops[0]";
            public static final String VARIETY = "variety";
            public static final String MAP = "map";
            public static final String INCLUDE = "include";
            public static final String FARM_ID = "farm_id";
            public static final String JOURNAL_ID = "journal_id";
            public static final String TITLE = "title";
            public static final String STATUS = "status";
            public static final String ENTRY_DATE = "entry_date";
            public static final String START_DATE = "start_date";
            public static final String END_DATE = "end_date";
            public static final String FIELD = "field";
            public static final String KEY = "key";
            public static final String VALUE = "value";
            public static final String STATION_ID = "station_id";
            public static final String USER_LAT = "user_lat";
            public static final String USER_LONG = "user_long";
            public static final String SUBSCRIPTION_ID = "subscription_id";
            public static final String SEND_SMS = "send_sms";
            public static final String START_TIME = "start_time";
            public static final String END_TIME = "end_time";
            public static final String QTY = "qty";
            public static final String BRAND = "brand";
            public static final String CONTENT = "content";
            public static final String NEW_PASSWORD = "new_password";
            public static final String CURRENT_PASSWORD = "current_password";
        }

        public static class route{

            public static final String APP_SETTINGS = API + Variable.APP_SETTINGS + FORMAT;

            public static class auth{

                public static final String LOGIN = API + AUTH + V2 + Variable.LOGIN + FORMAT;
                public static final String REGISTER = API + AUTH + V2 + Variable.REGISTER + FORMAT;
                public static final String RESEND_CODE = API + AUTH + Variable.RESEND_CODE + FORMAT;
                public static final String LOGOUT = API + AUTH + Variable.LOGOUT + FORMAT;
                public static final String REFRESH_TOKEN = API + AUTH + Variable.REFRESH_TOKEN + FORMAT;
                public static final String GET_COUNTRY = API + Variable.GET_COUNTRY + FORMAT;
            }

            public static class farms{
                public static final String CREATE = API + FARMS + Variable.CREATE + FORMAT;
                public static final String EDIT = API + FARMS + Variable.EDIT + FORMAT;
                public static final String DELETE = API + FARMS + Variable.DELETE + FORMAT;
                public static final String ALL = API + FARMS + Variable.ALL + FORMAT;
                public static final String SHOW = API + FARMS + Variable.SHOW + FORMAT;

                public static final String EDIT_CROPS = API + FARMS + Variable.EDIT_CROPS + FORMAT;
            }

            public static class farm_crops{
                public static final String SHOW = API + FARM_CROPS + Variable.SHOW + FORMAT;
            }

            public static class journals{
                public static final String CREATE = API + JOURNALS + Variable.CREATE + FORMAT;
                public static final String EDIT = API + JOURNALS + Variable.EDIT + FORMAT;
                public static final String DELETE = API + JOURNALS + Variable.DELETE + FORMAT;
                public static final String ALL = API + JOURNALS + Variable.ALL + FORMAT;
                public static final String SHOW = API + JOURNALS + Variable.SHOW + FORMAT;

                public static final String CROP = API + JOURNALS + Variable.CROP + FORMAT;
            }

            public static class profile{
                public static final String INFO = API + PROFILE + Variable.INFO + FORMAT;
                public static final String EDIT_FIELD = API + PROFILE + Variable.EDIT_FIELD + FORMAT;
                public static final String EDIT = API + PROFILE + Variable.EDIT + FORMAT;
                public static final String UPLOAD_AVATAR = API + PROFILE + Variable.UPLOAD_AVATAR + FORMAT;
                public static final String CHANGE_PASSWORD = API + PROFILE + Variable.CHANGE_PASSWORD + FORMAT;
            }

            public static class crops{
                public static final String ALL = API + CROPS + Variable.ALL + FORMAT;
            }

            public static class inquiry{
                public static final String ADVISORY = API + INQUIRY + Variable.ADVISORY + FORMAT;
            }

            public static class stations{
                public static final String ALL = API + STATIONS + Variable.ALL + FORMAT;
                public static final String NEARBY = API + STATIONS + Variable.NEARBY + FORMAT;
                public static final String SHOW = API + STATIONS + Variable.SHOW + FORMAT;
                public static final String SUBSCRIPTIONS = API + STATIONS + Variable.SUBSCRIPTIONS + FORMAT;
                public static final String SUBSCRIBE = API + STATIONS + Variable.SUBSCRIBE + FORMAT;
                public static final String UNSUBSCRIBE = API + STATIONS + Variable.UNSUBSCRIBE + FORMAT;
                public static final String OWNED = API + STATIONS + Variable.OWNED + FORMAT;
            }

        }
    }
}
