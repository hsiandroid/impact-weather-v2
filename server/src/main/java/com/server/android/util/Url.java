package com.server.android.util;


/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url {
    public static final String PRODUCTION_URL = "http://impact.highlysucceed.com";
    public static final String DEBUG_URL = "http://impact.highlysucceed.com";

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    public static final String getLogin(){
        return "api/auth/login";
    }

    public static final String getRefreshToken(){
        return "api/auth/refresh";
    }

    public static final String getMemberFeed(){
        return "api/posts/feed";
    }

    public static final String getAddFeed(){
        return "api/posts/feed/add";
    }

    public static final String getDeleteFeed(){
        return "api/posts/delete";
    }

    public static final String getLikeFeed(){
        return "api/activities/like/post";
    }

    public static final String getUnlikeFeed(){
        return "api/activities/unlike/post";
    }

    public static final String getComment(){
        return "api/posts/comment";
    }

    public static final String getPostComment(){
        return "api/posts/comment";
    }

    public static final String getMyGroup(){
        return "/api/groups/my/group";
    }

    public static final String getDiscoverGroup(){
        return "api/groups/discover/group";
    }

    public static final String getSuggestGroup(){
        return "/api/groups/add";
    }

    public static final String getUpdateGroup(){
        return "/api/groups/edit";
    }

    public static final String getDeleteGroup(){
        return "/api/groups/delete";
    }

    public static final String getInviteGroup(){
        return "/api/activities/invite/group";
    }

    public static final String getJoinGroup(){
        return "/api/activities/join/group";
    }

    public static final String getLeaveGroup(){
        return "/api/activities/leave/group";
    }

    public static final String getAllGroup(){
        return "/api/groups";
    }

    public static final String getGroupFeed(){
        return "/api/posts/group";
    }

    public static final String getGroupFeedAddPost(){
        return "/api/posts/group/add";
    }

    public static final String getGroupFeedDeletePost(){
        return "/api/posts/delete";
    }

    public static final String getGroupFeedLikePost(){
        return "/api/activities/like/post";
    }

    public static final String getGroupFeedUnlikePost(){
        return "/api/activities/unlike/post";
    }

    public static final String getGroupFeedComment(){
        return "/api/posts/comment";
    }

    public static final String getGroupPostComment(){
        return "/api/posts/comment";
    }

    public static final String getGroupDeleteComment(){
        return "/api/posts/comment";
    }

    public static final String getGroupMember(){
        return "/api/activities/joining/group/member";
    }

    public static final String getAllEvent(){
        return "api/events";
    }

    public static final String getGoingEvent(){
        return "api/activities/going/event";
    }

    public static final String getLeaveEvent(){
        return "api/activities/leave/event";
    }
}
