package com.server.android.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Jomar Olaybal on 13/12/2016.
 */

public class BaseRequest<T> implements Callback<T> {

    private static Context context;
    private Retrofit retrofit;

    private String BASE_URL = "http://google.com";
    private String TOKEN = "123456789";
    private final String DEVICE_NAME = "android";
    private int page = 1;
    private int perPage = 5;
    private boolean hasMorePage = false;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    private HashMap<String, String> params;
    private HashMap<String, RequestBody> requestBodyParams;
    private DATA_RESPONSE_TYPE dataResponseType = DATA_RESPONSE_TYPE.DEFAULT;

    private String authorization = "xxxxxx";
    private boolean checkToken = true;

    private boolean showNoInternetConnection = true;

    public BaseRequest(Context context) {
        this.context = context;
        Bundle data = null;
        try {
            data = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TOKEN = data.getString(context.getPackageName() + ".TOKEN", "");
        BASE_URL = data.getString(context.getPackageName() + ".BASEURL", "");


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public String getToken() {
        return TOKEN;
    }

    public String getDeviceID() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getDeviceName() {
        return DEVICE_NAME;
    }

    public void execute() {
        setDataResponseType(DATA_RESPONSE_TYPE.DEFAULT);
        run();
    }

    public BaseRequest<T> addAuthorization(String authorization) {
        this.authorization = "Bearer " + authorization;
        return this;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void run() {
        onCreateCall().enqueue(this);
    }

    public void executeString() {
        onCreateStringCall().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.e("String Response", response.body().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Converter<ResponseBody, BaseTransformer> converter = getRetrofit()
                            .responseBodyConverter(BaseTransformer.class, new Annotation[0]);
                    try {

                        Log.e("String Response", converter.convert(response.errorBody()).toString());
                    } catch (IOException e) {
                        Log.e("String Response", "parse error");
                    }
                }

                if (getProgressDialog() != null) {
                    getProgressDialog().dismiss();
                }
                if (getSwipeRefreshLayout() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (getProgressDialog() != null) {
                    getProgressDialog().dismiss();
                }
                if (getSwipeRefreshLayout() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public Call<T> onCreateCall() {
        return null;
    }

    public Call<ResponseBody> onCreateStringCall() {
        return null;
    }

    // for pagination

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public BaseRequest<T> setPerPage(int perPage) {
        this.perPage = perPage;
        return this;
    }

    public BaseRequest<T> nextPage() {
        setDataResponseType(DATA_RESPONSE_TYPE.NEXT);
        if (hasMorePage) {
            setPage(getPage() + 1);
        }
        run();
        return this;
    }

    public BaseRequest<T> previousPage() {
        setDataResponseType(DATA_RESPONSE_TYPE.PREV);
        if (getPage() > 1) {
            setPage(getPage() - 1);
        }
        run();
        return this;
    }

    public BaseRequest<T> first() {
        setDataResponseType(DATA_RESPONSE_TYPE.FIRST);
        setPage(1);
        run();
        return this;
    }


    // for parameter

    public HashMap<String, String> getParameters() {
        if (params == null) {
            params = new HashMap<>();
            defaultParam();
        }

        HashMap<String, String> page = params;
        page.put(Variable.server.key.PAGE, getPage() + "");
        page.put(Variable.server.key.PER_PAGE, getPerPage() + "");
        return page;
    }

    public BaseRequest<T> addParameters(String key, Object object) {

        if (params == null) {
            params = new HashMap<>();
            defaultParam();
        }

        params.put(key, String.valueOf(object));
        return this;
    }

    public BaseRequest<T> clearParameters() {
        if (params != null) {
            params.clear();
            params = new HashMap<>();
            defaultParam();
        }
        return this;
    }

    public void defaultParam() {
        params.put(Variable.server.key.API_TOKEN, getToken());
        params.put(Variable.server.key.API_TOKEN, getToken());
//        params.put(Variable.server.key.DEVICE_ID, getDeviceID());
        params.put(Variable.server.key.DEVICE_NAME, getDeviceName());
//        params.put(Variable.server.key.DEVICE_REG_ID, getDeviceRegId());
        addDefaultParam();
    }

    public void addDefaultParam() {

    }

    // for request body parameter

    public HashMap<String, RequestBody> getRequestBodyParameters() {
        if (requestBodyParams == null) {
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }

        HashMap<String, RequestBody> page = requestBodyParams;
        page.put(Variable.server.key.PAGE, getStringRequestBody(String.valueOf(getPage())));
        page.put(Variable.server.key.PER_PAGE, getStringRequestBody(String.valueOf(getPerPage())));
        return page;
    }

    public RequestBody getStringRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
    }

    public RequestBody getStringRequestBody(int value) {
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
    }


    public BaseRequest<T> addRequestBodyParameters(String key, Object object) {

        if (requestBodyParams == null) {
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }
        requestBodyParams.put(key, RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(object)));
        return this;
    }

    public enum RequestType {
        FILE_REQUEST,
        STRING_REQUEST
    }

    public BaseRequest<T> clearRequestBodyParameters() {
        if (requestBodyParams != null) {
            requestBodyParams.clear();
            requestBodyParams = new HashMap<>();
            defaultRequestBodyParam();
        }
        return this;
    }

    public void defaultRequestBodyParam() {
        requestBodyParams.put(Variable.server.key.API_TOKEN, getStringRequestBody(String.valueOf(getToken())));
        requestBodyParams.put(Variable.server.key.DEVICE_ID, getStringRequestBody(String.valueOf(getDeviceID())));
        requestBodyParams.put(Variable.server.key.DEVICE_NAME, getStringRequestBody(String.valueOf(getDeviceName())));
        requestBodyParams.put(Variable.server.key.DEVICE_REG_ID, getStringRequestBody(String.valueOf(getDeviceRegId())));
        addDefaultRequestBodyParam();
    }

    public void addDefaultRequestBodyParam() {

    }

    public MultipartBody.Builder requestBodyBuilder;

    public BaseRequest<T> addStringMultipartBody(String key, Object object) {

        if (requestBodyBuilder == null) {
            requestBodyBuilder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM);

//            defaultRequestBodyParam();
        }
        requestBodyBuilder.addFormDataPart(key, null, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(object)));
        return this;
    }

    public BaseRequest<T> addFileMultipartBody(String key, File object) {
        if (requestBodyBuilder == null) {
            requestBodyBuilder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM);
//            defaultRequestBodyParam();
        }
        requestBodyBuilder.addFormDataPart(key, object.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), object));
        return this;
    }

    public MultipartBody getMultipartBody() {
        return requestBodyBuilder.build();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("TAG", ">>>" + response.code());
//        Log.e("TAG" , ">>>" + new Gson().toJson(response));
//        generateNoteOnSD(context, "JSON", new Gson().toJson(response));
        responseData(response);
        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }
        if (getSwipeRefreshLayout() != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Logs");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (getProgressDialog() != null) {
            getProgressDialog().dismiss();
        }
        if (getSwipeRefreshLayout() != null) {
            swipeRefreshLayout.setRefreshing(false);
        }
        Log.e("TAG", ">>>FAILED");
        if (showNoInternetConnection) {
            Toast.makeText(context, "Check your internet connection.", Toast.LENGTH_LONG).show();
        }

//        responseData(null);
    }


    public void responseData(Response<T> response) {

    }

    public BaseRequest<T> showNoInternetConnection(boolean showNoInternetConnection) {
        this.showNoInternetConnection = showNoInternetConnection;
        return this;
    }

    public BaseRequest<T> setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
        return this;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public BaseRequest<T> setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        return this;
    }

    public BaseRequest<T> showSwipeRefreshLayout(boolean b) {
        if (getSwipeRefreshLayout() != null) {
            getSwipeRefreshLayout().setRefreshing(b);
        }
        return this;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }


    private enum DATA_RESPONSE_TYPE {
        DEFAULT,
        NEXT,
        PREV,
        FIRST
    }

    private void setDataResponseType(DATA_RESPONSE_TYPE dataResponseType) {
        this.dataResponseType = dataResponseType;
    }

    private DATA_RESPONSE_TYPE getDataResponseType() {
        return dataResponseType;
    }

    private String deviceRegId = "";

    public BaseRequest<T> setDeviceRegID(String regID) {
        deviceRegId = regID;
        return this;
    }

    public void setCheckToken(boolean checkToken) {
        this.checkToken = checkToken;
    }

    public String getDeviceRegId() {
        return deviceRegId;
    }

    public class ResponseData {
        private Response<T> response;

        private boolean isNext = false;
        private boolean isPrev = false;
        private boolean isFirst = false;

        public boolean isNext() {
            return isNext;
        }

        public boolean isPrev() {
            return isPrev;
        }

        public boolean isFirst() {
            return isFirst;
        }

        public ResponseData(Response<T> response) {
            switch (getDataResponseType()) {
                case DEFAULT:
                    break;
                case NEXT:
                    isNext = true;
                    Log.e("ResponseData", "next");
                    break;
                case PREV:
                    isPrev = true;
                    Log.e("ResponseData", "prev");
                    break;
                case FIRST:
                    isFirst = true;
                    Log.e("ResponseData", "first");
                    break;
            }

            this.response = response;
        }

        public int getCode() {
            return response.code();
        }

        public String getMessage() {
            return response.message();
        }

        public T getData(Class K) {
            return processResponse(K);
        }

        public T getData() {
            return processResponse(BaseTransformer.class);
        }

        public T processResponse(Class K) {
            if (response.code() >= 400 && response.code() < 599) {
                Converter<ResponseBody, T> converter = getRetrofit().responseBodyConverter(K, new Annotation[0]);
                try {
                    T temp = converter.convert(response.errorBody());
                    if (checkToken) {
                        switch (((BaseTransformer) temp).status_code) {
                            case "TOKEN_EXPIRED":
                            case "TOKEN_INVALID":
                            case "INVALID_TOKEN":
                            case "USER_NOT_FOUND ":
                            case "TOKEN_NOT_PROVIDED ":
                            case "INVALID_AUTH_USER ":
                                EventBus.getDefault().post(InvalidToken.newInstance());
                                break;
                        }
                    }
                    return temp;
                } catch (IOException e) {
                    Log.e("ResponseData Error", ">>> " + e.getMessage());
                    return (T) new BaseTransformer().getClass();
                }
            } else {
                T temp = response.body();
                hasMorePage = ((BaseTransformer) temp).has_morepages;
                return temp;
            }
        }
    }
}
